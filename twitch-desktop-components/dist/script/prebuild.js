"use strict";
exports.__esModule = true;
var tslib_1 = require("tslib");
var cp = require("child_process");
var path = require("path");
function run(cmd) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    cp.exec(cmd, function (err, stdout, stderr) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(stdout || stderr);
                        }
                    });
                })];
        });
    });
}
var internalRegistry = 'https://npm.internal.justin.tv/';
(function () { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
    var registry, pkg, version, name, platform, supportedArchs, _i, supportedArchs_1, arch, submodulePath, buildPath, ex_1, ex_2, ex_3, ex_4;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, run('npm config get registry')];
            case 1:
                registry = (_a.sent()).trim();
                if (registry !== internalRegistry) {
                    console.error('Registry is not set to the twitch internal. You can do it by calling `npm config set registry https://npm.internal.justin.tv/ && yarn config set registry https://npm.internal.justin.tv/`');
                    process.exit(1);
                }
                pkg = require(path.resolve('package.json'));
                version = pkg.version;
                name = pkg.name;
                platform = process.platform;
                supportedArchs = pkg.binaries[platform] || [];
                _i = 0, supportedArchs_1 = supportedArchs;
                _a.label = 2;
            case 2:
                if (!(_i < supportedArchs_1.length)) return [3 /*break*/, 17];
                arch = supportedArchs_1[_i];
                submodulePath = path.resolve('prebuilt_modules', platform + "-" + arch);
                buildPath = path.join(submodulePath, 'build');
                _a.label = 3;
            case 3:
                _a.trys.push([3, 5, , 6]);
                return [4 /*yield*/, run("npm run compile -- --arch " + arch + " --out " + buildPath)];
            case 4:
                _a.sent();
                return [3 /*break*/, 6];
            case 5:
                ex_1 = _a.sent();
                console.error('Failed to compile binaries.', ex_1);
                process.exit(1);
                return [3 /*break*/, 6];
            case 6:
                if (!process.argv.includes('publish')) return [3 /*break*/, 16];
                _a.label = 7;
            case 7:
                _a.trys.push([7, 9, , 10]);
                return [4 /*yield*/, run("cd " + submodulePath + " && npm version " + version + " --no-git-tag-version --allow-same-version")];
            case 8:
                _a.sent();
                return [3 /*break*/, 10];
            case 9:
                ex_2 = _a.sent();
                console.warn("Failed to set versuon for " + arch, ex_2);
                return [3 /*break*/, 10];
            case 10:
                _a.trys.push([10, 12, , 13]);
                return [4 /*yield*/, run("cd " + submodulePath + " && npm publish")];
            case 11:
                _a.sent();
                return [3 /*break*/, 13];
            case 12:
                ex_3 = _a.sent();
                console.warn("Failed to publish for " + arch, ex_3);
                return [3 /*break*/, 13];
            case 13:
                _a.trys.push([13, 15, , 16]);
                return [4 /*yield*/, run("npm install --no-save " + name + "-" + platform + "-" + arch + "@" + version)];
            case 14:
                _a.sent();
                return [3 /*break*/, 16];
            case 15:
                ex_4 = _a.sent();
                console.error("Failed to install for " + arch + ".", ex_4);
                process.exit(1);
                return [3 /*break*/, 16];
            case 16:
                _i++;
                return [3 /*break*/, 2];
            case 17: return [2 /*return*/];
        }
    });
}); })();
//# sourceMappingURL=prebuild.js.map