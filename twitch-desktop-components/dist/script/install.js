"use strict";
exports.__esModule = true;
var tslib_1 = require("tslib");
var cp = require("child_process");
var path = require("path");
var fs = require("fs");
function run(cmd) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    cp.exec(cmd, function (err, stdout, stderr) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(stdout || stderr);
                        }
                    });
                })];
        });
    });
}
function copySync(src, dest) {
    var exists = fs.existsSync(src);
    if (!exists) {
        return;
    }
    if (fs.statSync(src).isDirectory()) {
        fs.mkdirSync(dest);
        fs.readdirSync(src).forEach(function (child) {
            copySync(path.join(src, child), path.join(dest, child));
        });
    }
    else {
        fs.copyFileSync(src, dest);
    }
}
;
function unlinkSync(src) {
    var exists = fs.existsSync(src);
    if (!exists) {
        return;
    }
    if (fs.statSync(src).isDirectory()) {
        fs.readdirSync(src).forEach(function (child) { return unlinkSync(path.join(src, child)); });
        fs.rmdirSync(src);
    }
    else {
        fs.unlinkSync(src);
    }
}
var arch = (process.argv.find(function (arg) { return arg.startsWith('--arch='); }) || "--arch=" + process.arch).substr('--arch='.length);
var platform = process.platform;
var pkg = require(path.resolve('package.json'));
var version = pkg.version;
var name = pkg.name;
var submoduleName = name + "-" + platform + "-" + arch;
(function () { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
    var modulePath, srcBuildPath, dstBuildPath, err_1;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, run("npm install --registry https://npm.internal.justin.tv/ --no-save " + submoduleName + "@" + version)];
            case 1:
                _a.sent();
                modulePath = require.resolve(submoduleName);
                srcBuildPath = path.resolve(path.dirname(modulePath), 'build');
                dstBuildPath = path.join(path.dirname(path.resolve('package.json')), 'build');
                unlinkSync(dstBuildPath);
                copySync(srcBuildPath, dstBuildPath);
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                console.error("Failed to install binary dependency " + submoduleName, err_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); })();
//# sourceMappingURL=install.js.map