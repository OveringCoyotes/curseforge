"use strict";
exports.__esModule = true;
var tslib_1 = require("tslib");
var cp = require("child_process");
var path = require("path");
function run(cmd) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    cp.exec(cmd, function (err, stdout, stderr) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(stdout || stderr);
                        }
                    });
                })];
        });
    });
}
var pkg = require(path.resolve('package.json'));
var version = pkg.version;
var name = pkg.name;
var binaries = pkg.binaries;
(function () { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
    var _a, _b, _i, platform, archs, _c, archs_1, arch, submoduleName, versions, _d, _e, err_1;
    return tslib_1.__generator(this, function (_f) {
        switch (_f.label) {
            case 0:
                _a = [];
                for (_b in binaries)
                    _a.push(_b);
                _i = 0;
                _f.label = 1;
            case 1:
                if (!(_i < _a.length)) return [3 /*break*/, 8];
                platform = _a[_i];
                archs = binaries[platform];
                _c = 0, archs_1 = archs;
                _f.label = 2;
            case 2:
                if (!(_c < archs_1.length)) return [3 /*break*/, 7];
                arch = archs_1[_c];
                _f.label = 3;
            case 3:
                _f.trys.push([3, 5, , 6]);
                submoduleName = name + "-" + platform + "-" + arch;
                _e = (_d = JSON).parse;
                return [4 /*yield*/, run("npm show " + submoduleName + " versions --json")];
            case 4:
                versions = _e.apply(_d, [_f.sent()]);
                if (!versions.includes(version)) {
                    throw new Error("Missing version " + version + " for submodule " + submoduleName);
                }
                return [3 /*break*/, 6];
            case 5:
                err_1 = _f.sent();
                console.error('Binary submodules validation error', err_1);
                process.exit(1);
                return [3 /*break*/, 6];
            case 6:
                _c++;
                return [3 /*break*/, 2];
            case 7:
                _i++;
                return [3 /*break*/, 1];
            case 8: return [2 /*return*/];
        }
    });
}); })();
//# sourceMappingURL=prepublish.js.map