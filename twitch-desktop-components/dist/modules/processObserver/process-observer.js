"use strict";
exports.__esModule = true;
exports.ProcessObserver = exports.EventType = void 0;
var module_1 = require("../module");
var EventType;
(function (EventType) {
    EventType[EventType["Started"] = 0] = "Started";
    EventType[EventType["Terminated"] = 1] = "Terminated";
    EventType[EventType["Activated"] = 2] = "Activated";
})(EventType = exports.EventType || (exports.EventType = {}));
var ProcessObserver = /** @class */ (function () {
    function ProcessObserver() {
        this.processObserver = new module_1.nativeModule.ProcessObserver();
    }
    ProcessObserver.prototype.on = function (event, handler) {
        var _this = this;
        var identifier = this.processObserver.on(event, handler);
        return function () { return _this.processObserver.removeListener(identifier); };
    };
    ProcessObserver.prototype.getRunningProcesses = function () {
        return this.processObserver.getRunningProcesses();
    };
    ProcessObserver.prototype.getForegroundProcess = function () {
        return this.processObserver.getForegroundProcess();
    };
    return ProcessObserver;
}());
exports.ProcessObserver = ProcessObserver;
//# sourceMappingURL=process-observer.js.map