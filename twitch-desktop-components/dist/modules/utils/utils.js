"use strict";
exports.__esModule = true;
exports.getFileVersions = exports.getVideoDevices = exports.saveScreenshot = exports.getIdleTime = exports.orderWindowBack = exports.setLaunchAtStartup = exports.isLaunchOnStartEnabled = exports.getRegistryValue = exports.setMachineKey = exports.getMachineKey = exports.decryptChromeCookie = exports.decryptDPAPI = void 0;
var tslib_1 = require("tslib");
var module_1 = require("../module");
var utils = new module_1.nativeModule.Utils();
var cachedMachineKeys = new Map();
/**
 * Decodes the provided curse token into a usable token string.
 * @param value Buffer containing contents of encoded curse token.
 */
function decryptDPAPI(value) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.decryptDPAPI(value)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.decryptDPAPI = decryptDPAPI;
/**
 * Decrypts a cookie from the Chrome db using their special
 * decryption scheme.
 * @param value A Buffer containing the encrypted_value directly
 * from the database table.
 */
function decryptChromeCookie(value) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.decryptChromeCookie(value)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.decryptChromeCookie = decryptChromeCookie;
/**
 * Gets the machine key from the registry.
 * If the key does not exist, this generates and sets a new key
 * and returns that value.
 */
function getMachineKey(name) {
    if (name === void 0) { name = ''; }
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = cachedMachineKeys.get(name);
                    if (_a) return [3 /*break*/, 2];
                    return [4 /*yield*/, utils.getMachineKey(name)];
                case 1:
                    _a = (_b.sent());
                    _b.label = 2;
                case 2: return [2 /*return*/, _a];
            }
        });
    });
}
exports.getMachineKey = getMachineKey;
/**
 * Writes provided key to the registry. Resolves to true if write succeeds, false otherwise.
 * @param key Key to set as the machine key
 */
function setMachineKey(key, name) {
    if (name === void 0) { name = ''; }
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.setMachineKey(key, name)];
                case 1:
                    _a.sent();
                    cachedMachineKeys.set(name, key);
                    return [2 /*return*/];
            }
        });
    });
}
exports.setMachineKey = setMachineKey;
/**
 * Gets a string from the registry.
 */
function getRegistryValue(hive, location, key) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.getRegistryValue(hive, location, key)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.getRegistryValue = getRegistryValue;
/**
 * Gets wether launch on startup is enabled for the specified file path
 * @param launcherFileName path to the file to be launched at system startup
 */
function isLaunchOnStartEnabled(launcherFileName) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.isLaunchOnStartEnabled(launcherFileName)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.isLaunchOnStartEnabled = isLaunchOnStartEnabled;
/**
 * Enables or disables launch on system startup for the specified file path
 * @param enabled enables and disables launch at startup
 * @param launcherFileName file path to the binary to be started
 * @param args arguments passed to the started binary
 * @param userModelId user model id
 */
function setLaunchAtStartup(enabled, launcherFileName, args, userModelId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, utils.setLaunchAtStartup(enabled, launcherFileName, args.join(' '), userModelId)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.setLaunchAtStartup = setLaunchAtStartup;
/**
 * Sends window below every other window on screen.
 * @param handle A buffer containing native window handle pointer. NSView* on mac and HWND* on Windows.
 * The handle can be aquired from BrowserWindow's getNativeWindowHandle() function.
 */
function orderWindowBack(handle) {
    utils.orderWindowBack(handle);
}
exports.orderWindowBack = orderWindowBack;
/**
 * Get the idle time from the operating system
 */
function getIdleTime() {
    return utils.getIdleTime();
}
exports.getIdleTime = getIdleTime;
/**
 * Save a PNG of the current window to the path
 */
function saveScreenshot(path) {
    return utils.saveScreenshot(path);
}
exports.saveScreenshot = saveScreenshot;
/**
 * Returns capabilities of all video devices (Webcams/Capture Cards)
 */
function getVideoDevices() {
    return JSON.parse(utils.getVideoDevices());
}
exports.getVideoDevices = getVideoDevices;
/**
 * Returns fileVersion and productVersion of input exe/dll file
 */
function getFileVersions(path) {
    return JSON.parse(utils.getFileVersions(path));
}
exports.getFileVersions = getFileVersions;
//# sourceMappingURL=utils.js.map