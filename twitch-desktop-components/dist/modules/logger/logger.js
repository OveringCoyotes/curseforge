"use strict";
exports.__esModule = true;
exports.Logger = exports.LogLevel = void 0;
var tslib_1 = require("tslib");
var ErrorStackParser = require("error-stack-parser");
var module_1 = require("../module");
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["Trace"] = 0] = "Trace";
    LogLevel[LogLevel["Debug"] = 1] = "Debug";
    LogLevel[LogLevel["Info"] = 2] = "Info";
    LogLevel[LogLevel["Warn"] = 3] = "Warn";
    LogLevel[LogLevel["Error"] = 4] = "Error";
    LogLevel[LogLevel["Fatal"] = 5] = "Fatal";
    LogLevel[LogLevel["Off"] = 6] = "Off";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
var Logger = /** @class */ (function () {
    function Logger(logger, errorHandlers, breadcrumbs, category, breadcrumbsCount) {
        this.parseError = function (error) {
            var stack = ErrorStackParser.parse(error);
            var err = {
                message: error.message,
                stack: stack.map(function (e) { return e.fileName + ":" + e.lineNumber + ":" + e.columnNumber; })
            };
            return err;
        };
        this.logger = logger;
        this.errorHandlers = errorHandlers;
        this.breadcrumbs = breadcrumbs;
        this.category = category;
        this.breadcrumbsCount = breadcrumbsCount || 20;
    }
    Logger.createWithConfig = function (config) {
        var errorHandlers = new Set();
        var breadcrumbs = new Array();
        var logger = new module_1.nativeModule.Logger();
        return new Logger(logger, errorHandlers, breadcrumbs, config.category, config.breadcrumbsCount);
    };
    Logger.prototype.start = function (rootPath, logName) {
        try {
            this.logger.start(rootPath, logName);
        }
        catch (error) {
            console.log('Failed to start logger.', error);
        }
    };
    Logger.prototype.setMinLogLevel = function (logLevel) {
        this.logger.minLogLevel = logLevel;
    };
    Logger.prototype.getMinLogLevel = function () {
        return this.logger.minLogLevel;
    };
    Logger.prototype.trace = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Trace, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'trace' });
    };
    Logger.prototype.debug = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Debug, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'debug' });
    };
    Logger.prototype.info = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Info, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'info' });
    };
    Logger.prototype.warn = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Warn, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'warn' });
    };
    Logger.prototype.error = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Error, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'error' });
    };
    Logger.prototype.fatal = function (message, dataOrError, error) {
        this.log({ level: LogLevel.Fatal, message: message, dataOrError: dataOrError, error: error, consoleFunc: 'error' });
    };
    Logger.prototype.flush = function (timeout) {
        if (timeout === void 0) { timeout = 0; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var flushPromise, rejectPromise;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                flushPromise = new Promise(function (resolve, _reject) {
                    _this.logger.flush(function () { return resolve(); });
                });
                rejectPromise = new Promise(function (_resolve, reject) {
                    if (timeout > 0) {
                        setTimeout(function () { return reject('Timed out'); }, timeout);
                    }
                });
                return [2 /*return*/, Promise.race([flushPromise, rejectPromise])];
            });
        });
    };
    Logger.prototype.onError = function (errorHandler) {
        var _this = this;
        var handler = function (errorLogEntry, breadcrumbs) { return errorHandler(errorLogEntry, breadcrumbs); };
        this.errorHandlers.add(handler);
        return function () {
            _this.errorHandlers["delete"](handler);
        };
    };
    Logger.prototype.withCategory = function (category) {
        return category ? new Logger(this.logger, this.errorHandlers, this.breadcrumbs, this.category ? this.category + "." + category : category) : this;
    };
    Logger.prototype.log = function (logData) {
        var _this = this;
        try {
            var category = this.category || '';
            var validated = this.validate(logData);
            var timestamp = Date.now();
            var message = validated.message;
            var error = validated.error;
            var level = validated.level;
            if (level < this.logger.minLogLevel) {
                return;
            }
            var parsedError = error ? this.parseError(error) : null;
            var data = validated.dataOrError;
            if ((data !== undefined && typeof data !== 'object' && !(data instanceof Error)) || data instanceof Array) {
                data = { data: data };
            }
            if (level > LogLevel.Debug) {
                var entry_1 = {
                    timestamp: timestamp,
                    category: category,
                    level: level,
                    message: message,
                    error: error,
                    data: data
                };
                if (error) {
                    this.errorHandlers.forEach(function (handler) { return handler(entry_1, Object.assign([], _this.breadcrumbs)); });
                }
                this.breadcrumbs.push(entry_1);
                while (this.breadcrumbs.length > this.breadcrumbsCount) {
                    this.breadcrumbs.shift();
                }
            }
            var dataOrUndefined = data ? JSON.stringify(data) : undefined;
            var errorOrUndefined = parsedError ? JSON.stringify(parsedError) : undefined;
            this.consoleLog(validated.consoleFunc, level, validated.message, category, data, parsedError);
            this.logger.log(level, category, message, dataOrUndefined, errorOrUndefined);
        }
        catch (error) {
            console.log('Logger log failed.', error);
        }
    };
    Logger.prototype.validate = function (logData) {
        // Handle case where no message is passed (just an error)
        if (logData.message instanceof Error) {
            logData.error = logData.message;
            logData.message = logData.error.message;
        }
        // Handle case where data is an Error object
        if (logData.dataOrError instanceof Error) {
            if (logData.error instanceof Error) {
                // two error objects -> wrap one as the data param
                logData.dataOrError = this.parseError(logData.dataOrError);
            }
            else if (logData.error) {
                // data is an error but err is not -> swap
                var tmpData = { error: logData.error };
                logData.error = logData.dataOrError;
                logData.dataOrError = tmpData;
            }
            else {
                // data is the error
                logData.error = logData.dataOrError;
                logData.dataOrError = undefined;
            }
        }
        // Make sure err is an Error or null (the error stack parser will throw an exception if err isn't an Error object)
        if (logData.error) {
            if (!(logData.error instanceof Error)) {
                // non-Error error -> do the best we can with it
                if (typeof logData.error === 'object') {
                    logData.error = new Error(JSON.stringify(logData.error));
                }
                else {
                    logData.error = new Error('' + logData.error);
                }
            }
        }
        else if (logData.level >= LogLevel.Error) {
            // make sure logger.error/fatal always has a stacktrace
            logData.error = new Error(logData.message || '');
        }
        return logData;
    };
    Logger.prototype.consoleLog = function (consoleFunc, level, message, category, data, error) {
        if (!console || !console[consoleFunc]) {
            return;
        }
        try {
            message = new Date().toLocaleTimeString() + " [" + LogLevel[level] + "] [" + category + "] " + message;
        }
        catch (_a) {
            console.warn('Failed to format message');
        }
        // Tedium because console funcs aren't real funcs
        if (data && error) {
            console[consoleFunc](message, data, error);
        }
        else if (data) {
            console[consoleFunc](message, data);
        }
        else if (error) {
            console[consoleFunc](message, error);
        }
        else {
            console[consoleFunc](message);
        }
    };
    Logger.prototype.getLogLevelName = function (logLevel) {
        switch (logLevel) {
            case LogLevel.Trace: return "Trace";
            case LogLevel.Debug: return "Debug";
            case LogLevel.Info: return "Info";
            case LogLevel.Warn: return "Warn";
            case LogLevel.Error: return "Error";
            case LogLevel.Fatal: return "Fatal";
            case LogLevel.Off: return "Off";
        }
    };
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map