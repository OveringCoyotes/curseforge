"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ItemAction = exports.ItemInteraction = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ItemInteraction {
  constructor(timestamp, item, action) {
    _defineProperty(this, "timestamp", void 0);

    _defineProperty(this, "consumable", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "itemID", void 0);

    _defineProperty(this, "action", void 0);

    this.timestamp = timestamp;
    this.consumable = item.consumable || false;
    this.itemID = item.itemID || -1;
    this.name = item.displayName || '';
    this.action = action;
  }

}

exports.ItemInteraction = ItemInteraction;
let ItemAction;
exports.ItemAction = ItemAction;

(function (ItemAction) {
  ItemAction["Removed"] = "removed";
  ItemAction["Added"] = "added";
})(ItemAction || (exports.ItemAction = ItemAction = {}));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvaXRlbS1oaXN0b3J5LnRzIl0sIm5hbWVzIjpbIkl0ZW1JbnRlcmFjdGlvbiIsImNvbnN0cnVjdG9yIiwidGltZXN0YW1wIiwiaXRlbSIsImFjdGlvbiIsImNvbnN1bWFibGUiLCJpdGVtSUQiLCJuYW1lIiwiZGlzcGxheU5hbWUiLCJJdGVtQWN0aW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFFTyxNQUFNQSxlQUFOLENBQXNCO0FBTzNCQyxFQUFBQSxXQUFXLENBQUNDLFNBQUQsRUFBb0JDLElBQXBCLEVBQWdDQyxNQUFoQyxFQUFvRDtBQUFBOztBQUFBOztBQUFBOztBQUFBOztBQUFBOztBQUM3RCxTQUFLRixTQUFMLEdBQWlCQSxTQUFqQjtBQUNBLFNBQUtHLFVBQUwsR0FBa0JGLElBQUksQ0FBQ0UsVUFBTCxJQUFtQixLQUFyQztBQUNBLFNBQUtDLE1BQUwsR0FBY0gsSUFBSSxDQUFDRyxNQUFMLElBQWUsQ0FBQyxDQUE5QjtBQUNBLFNBQUtDLElBQUwsR0FBWUosSUFBSSxDQUFDSyxXQUFMLElBQW9CLEVBQWhDO0FBQ0EsU0FBS0osTUFBTCxHQUFjQSxNQUFkO0FBQ0Q7O0FBYjBCOzs7SUFnQmpCSyxVOzs7V0FBQUEsVTtBQUFBQSxFQUFBQSxVO0FBQUFBLEVBQUFBLFU7R0FBQUEsVSwwQkFBQUEsVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEl0ZW0gfSBmcm9tICcuL2xlYWd1ZS1hcGkvYWxsLWdhbWUtZGF0YSc7XHJcblxyXG5leHBvcnQgY2xhc3MgSXRlbUludGVyYWN0aW9uIHtcclxuICBwdWJsaWMgdGltZXN0YW1wOiBudW1iZXI7XHJcbiAgcHVibGljIGNvbnN1bWFibGU6IGJvb2xlYW47XHJcbiAgcHVibGljIG5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgaXRlbUlEOiBudW1iZXI7XHJcbiAgcHVibGljIGFjdGlvbjogSXRlbUFjdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IodGltZXN0YW1wOiBudW1iZXIsIGl0ZW06IEl0ZW0sIGFjdGlvbjogSXRlbUFjdGlvbikge1xyXG4gICAgdGhpcy50aW1lc3RhbXAgPSB0aW1lc3RhbXA7XHJcbiAgICB0aGlzLmNvbnN1bWFibGUgPSBpdGVtLmNvbnN1bWFibGUgfHwgZmFsc2U7XHJcbiAgICB0aGlzLml0ZW1JRCA9IGl0ZW0uaXRlbUlEIHx8IC0xO1xyXG4gICAgdGhpcy5uYW1lID0gaXRlbS5kaXNwbGF5TmFtZSB8fCAnJztcclxuICAgIHRoaXMuYWN0aW9uID0gYWN0aW9uO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGVudW0gSXRlbUFjdGlvbiB7XHJcbiAgUmVtb3ZlZCA9ICdyZW1vdmVkJyxcclxuICBBZGRlZCA9ICdhZGRlZCdcclxufVxyXG4iXX0=