"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SidecarClient = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class SidecarClient {
  constructor(port, host) {
    _defineProperty(this, "port", void 0);

    _defineProperty(this, "host", void 0);

    _defineProperty(this, "isRunning", void 0);

    this.port = port;
    this.host = host;
    this.isRunning = false;
  }

}

exports.SidecarClient = SidecarClient;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9iYXNlL2NsaWVudC50cyJdLCJuYW1lcyI6WyJTaWRlY2FyQ2xpZW50IiwiY29uc3RydWN0b3IiLCJwb3J0IiwiaG9zdCIsImlzUnVubmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQU8sTUFBTUEsYUFBTixDQUFvQjtBQUt6QkMsRUFBQUEsV0FBVyxDQUFDQyxJQUFELEVBQWVDLElBQWYsRUFBNkI7QUFBQTs7QUFBQTs7QUFBQTs7QUFDdEMsU0FBS0QsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsU0FBS0MsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixLQUFqQjtBQUNEOztBQVR3QiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBTaWRlY2FyQ2xpZW50IHtcclxuICBwdWJsaWMgcG9ydDogbnVtYmVyO1xyXG4gIHB1YmxpYyBob3N0OiBzdHJpbmc7XHJcbiAgcHVibGljIGlzUnVubmluZzogYm9vbGVhbjtcclxuXHJcbiAgY29uc3RydWN0b3IocG9ydDogbnVtYmVyLCBob3N0OiBzdHJpbmcpIHtcclxuICAgIHRoaXMucG9ydCA9IHBvcnQ7XHJcbiAgICB0aGlzLmhvc3QgPSBob3N0O1xyXG4gICAgdGhpcy5pc1J1bm5pbmcgPSBmYWxzZTtcclxuICB9XHJcbn1cclxuIl19