"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LeagueOfLegendsSidecarClient = void 0;

var mdaas = _interopRequireWildcard(require("enhanced-experiences-sdk"));

var _events = require("events");

var _https = require("https");

var _lcuConnector = _interopRequireDefault(require("lcu-connector"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var _base = require("../base");

var _constants = require("./constants");

var _abilityHistory = require("./transforms/ability-history");

var _itemHistory = require("./transforms/item-history");

var _payload = require("./transforms/payload");

var _playerMap = require("./transforms/player-map");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const tokenExpiredStub = _fn => {
  console.error('unexpected token refresh call-back');
  return false;
};

const DEFAULT_OPTIONS = {
  pollingInterval: 1000,
  broadcasterID: '',
  appTokenID: ''
};

class LeagueOfLegendsSidecarClient extends _base.SidecarClient {
  // public ActivePlayerName: string | undefined;
  constructor(options = DEFAULT_OPTIONS) {
    super(_constants.PORT, _constants.HOSTNAME);

    _defineProperty(this, "MDaaSConnection", mdaas.default());

    _defineProperty(this, "dataFlowRunning", false);

    _defineProperty(this, "lcuConnector", void 0);

    _defineProperty(this, "lcuRunning", false);

    _defineProperty(this, "lcuMetadata", void 0);

    _defineProperty(this, "lcuReady", false);

    _defineProperty(this, "config", void 0);

    _defineProperty(this, "configOptions", void 0);

    _defineProperty(this, "activeSessionId", '');

    _defineProperty(this, "lcuEventEmitter", void 0);

    _defineProperty(this, "isDataCacheValid", false);

    _defineProperty(this, "dataCache", void 0);

    _defineProperty(this, "connectionLock", false);

    _defineProperty(this, "leagueApiPollTimer", void 0);

    this.configOptions = Object.assign({}, DEFAULT_OPTIONS, options);
    this.lcuConnector = new _lcuConnector.default('');
    this.lcuEventEmitter = new _events.EventEmitter();
    this.dataCache = this.buildCacheObject();
    this.initializeLCUConnector();
    this.startLCUConnector();
    this.leagueApiPollTimer = undefined;
  }

  initializeLCUConnector() {
    this.lcuConnector.on('connect', data => {
      this.lcuMetadata = data;
      this.lcuRunning = true;
      const lcuReadyCheckInterval = setInterval(async () => {
        try {
          const isReady = await this.lcuReadyCheck();

          if (isReady) {
            this.lcuEventEmitter.emit('ready');
            clearInterval(lcuReadyCheckInterval);
          }
        } catch (e) {
          console.log(e);
        }
      }, 200);
      this.lcuReadyCheck();
    });
    this.lcuConnector.on('disconnect', () => {
      this.lcuRunning = false;
      this.lcuMetadata = undefined;
      this.dataCache.region = undefined;
    });
    this.lcuEventEmitter.on('ready', () => {
      this.lcuGetData();
    });
  }

  startLCUConnector() {
    this.lcuConnector.start();
  }

  stopLCUConnector() {
    this.lcuConnector.stop();
  }

  async lcuReadyCheck() {
    const agent = this.setupAgent();

    if (this.lcuRunning && this.lcuMetadata) {
      const lcuBaseUrl = `${this.lcuMetadata.protocol}://${this.lcuMetadata.address}:${this.lcuMetadata.port}`;
      const isReadyResponse = await (0, _nodeFetch.default)((0, _constants.LCU_READY_URL)(lcuBaseUrl), {
        agent,
        headers: {
          Authorization: `Basic ${Buffer.from(`${this.lcuMetadata.username}:${this.lcuMetadata.password}`).toString('base64')}`
        }
      });

      if (!isReadyResponse.ok) {
        throw new Error('Ready call failed');
      }

      const readyResponseText = await isReadyResponse.text();
      return readyResponseText === 'true';
    }

    return false;
  }

  async lcuGetData() {
    const agent = this.setupAgent();

    if (this.lcuReadyCheck && this.lcuMetadata) {
      const lcuBaseUrl = `${this.lcuMetadata.protocol}://${this.lcuMetadata.address}:${this.lcuMetadata.port}`;
      const platformResponse = await (0, _nodeFetch.default)((0, _constants.LCU_CLIENT_INFO)(lcuBaseUrl), {
        agent,
        headers: {
          Authorization: `Basic ${Buffer.from(`${this.lcuMetadata.username}:${this.lcuMetadata.password}`).toString('base64')}`
        }
      });

      if (platformResponse.ok) {
        const body = await platformResponse.json();
        this.dataCache.region = body.LoginDataPacket.platformId;
      }
    }
  }

  async isResponsive() {
    const agent = this.setupAgent();
    const response = await (0, _nodeFetch.default)(_constants.ACTIVEPLAYERNAME_URL, {
      agent
    });
    return response.ok;
  }

  async getDataFromLoLClient(endPointURL, summonerName = '') {
    const agent = this.setupAgent();
    const response = await (0, _nodeFetch.default)(`${endPointURL}${summonerName}`, {
      agent
    });
    return await response.json();
  }

  async getGameInfo() {
    const gameData = await this.getAllGameData();

    if (this.dataCache.region === undefined) {
      await this.lcuReadyCheck();
      await this.lcuGetData();
    }

    this.isDataCacheValid = this.isPayloadGood(gameData);

    if (this.isDataCacheValid) {
      this.dataCache.gameData = gameData;
      this.appendItemHistory(gameData);
      this.appendAbilityHistory(gameData);

      if (gameData.allPlayers) {
        this.dataCache.allPlayers = (0, _playerMap.createPlayerMap)(gameData.allPlayers, this.dataCache.allPlayers);
      }
    }
  } // arguementless calls


  async getActivePlayer() {
    return this.getDataFromLoLClient(_constants.ACTIVEPLAYER_URL);
  }

  async getActivePlayerAbilities() {
    return this.getDataFromLoLClient(_constants.ACTIVEPLAYERABILITIES_URL);
  }

  async getActivePlayerName() {
    return this.getDataFromLoLClient(_constants.ACTIVEPLAYERNAME_URL);
  }

  async getActivePlayerRunes() {
    return this.getDataFromLoLClient(_constants.ACTIVEPLAYERRUNES_URL);
  }

  async getAllGameData() {
    return this.getDataFromLoLClient(_constants.ALLGAMEDATA_URL);
  }

  async getEvents() {
    return this.getDataFromLoLClient(_constants.EVENTDATA_URL);
  }

  async getGameStats() {
    return this.getDataFromLoLClient(_constants.GAMESTATS_URL);
  }

  async getPlayerList() {
    return this.getDataFromLoLClient(_constants.PLAYERLIST_URL);
  } // calls that take a single arguement of summonerName


  async getPlayerMainRunes(summonerName) {
    return this.getDataFromLoLClient(_constants.PLAYERMAINRUNES_URL, summonerName);
  }

  async getPlayerSummonerSpells(summonerName) {
    return this.getDataFromLoLClient(_constants.PLAYERSUMMONERSPELLS_URL, summonerName);
  }

  async getPlayerItems(summonerName) {
    return this.getDataFromLoLClient(_constants.PLAYERITEMS_URL, summonerName);
  }

  async getPlayerScores(summonerName) {
    return this.getDataFromLoLClient(_constants.PLAYERSCORES_URL, summonerName);
  } // end api calls


  async setupMDaaSConnection(initialPayload) {
    this.config = {
      token: this.configOptions.appTokenID,
      broadcasterIds: [],
      gameId: _constants.LOLGAMEID,
      environment: 'prod',
      onTokenExpired: tokenExpiredStub,
      initialData: initialPayload,
      timeoutMs: 5000,
      debugFn: process.env.TWITCH_LOL_SIDECAR_DEBUG === '1'
    };
  }

  async disconnect() {
    this.MDaaSConnection.disconnect();
    this.activeSessionId = '';
    this.dataFlowRunning = false;
    this.connectionLock = false;
  }

  async sendAllGameData() {
    const payload = (0, _payload.createMDaaSPayload)(this.dataCache);
    await this.MDaaSConnection.updateData(payload);
  }

  stop() {
    if (this.leagueApiPollTimer) {
      clearInterval(this.leagueApiPollTimer);
      this.leagueApiPollTimer = undefined;
      this.disconnect();
    }
  }

  start(updateSessionId) {
    if (!this.leagueApiPollTimer) {
      this.dataCache = this.buildCacheObject();
      this.startDataFlow(updateSessionId);
    }
  }

  async startDataFlow(updateSessionId) {
    this.dataFlowRunning = true;

    if (!this.configOptions.appTokenID) {
      console.error('No token available');
    }

    let connectionRetry = 0;
    this.leagueApiPollTimer = setInterval(async () => {
      try {
        if (this.connectionLock || !this.dataFlowRunning) {
          return;
        }

        await this.getGameInfo();

        if (this.isDataCacheValid) {
          this.connectionLock = true;
          const payload = (0, _payload.createMDaaSPayload)(this.dataCache);

          if (!this.activeSessionId) {
            await this.setupMDaaSConnection(payload);
            this.activeSessionId = await this.MDaaSConnection.connect(this.config);

            if (this.activeSessionId && updateSessionId !== undefined) {
              // if e2 sdk set an active session id lets mark us as connection established and if there is a relevent function to callback lets do so here.
              updateSessionId();
            } else {
              connectionRetry++;

              if (connectionRetry > 10) {
                this.attemptClearConnection();
                connectionRetry = 0;
              }
            }
          } else {
            this.MDaaSConnection.updateData(payload);
          }

          this.connectionLock = false;
        }
      } catch (e) {
        console.error(e);
      }
    }, this.configOptions.pollingInterval);
  }

  attemptClearConnection() {
    this.dataCache = this.buildCacheObject();
    this.MDaaSConnection.disconnect();
    this.activeSessionId = '';
  }

  isPayloadGood(payload) {
    return payload && payload.allPlayers && payload.allPlayers.length > 0;
  }

  appendItemHistory(gameData) {
    if (gameData !== undefined && gameData.gameData !== undefined && gameData.activePlayer !== undefined && gameData.activePlayer.summonerName !== undefined && gameData.allPlayers !== undefined) {
      const activePlayerSummonerName = gameData.activePlayer.summonerName;
      const currentTime = gameData.gameData.gameTime;
      const activePlayerIndex = gameData.allPlayers.findIndex(p => p.summonerName === activePlayerSummonerName);
      const newItems = gameData.allPlayers[activePlayerIndex].items;

      if (newItems !== undefined && currentTime !== undefined) {
        this.dataCache.itemHistory = (0, _itemHistory.createItemHistory)(newItems, this.dataCache.currentItems, this.dataCache.itemHistory, currentTime);
        this.dataCache.currentItems = newItems;
      } else {
        console.log(`Issue starting item history creation. newItems: ${newItems} || currentTime: ${currentTime}`);
      }
    } else {
      console.log(`Issue finding data to create item history.`);
    }
  }

  appendAbilityHistory(gameData) {
    if (gameData !== undefined && gameData.gameData !== undefined && gameData.activePlayer !== undefined && gameData.activePlayer.abilities) {
      const currentTime = gameData.gameData.gameTime;
      const newAbilities = gameData.activePlayer.abilities;

      if (newAbilities !== undefined && currentTime !== undefined) {
        this.dataCache.abilityHistory = (0, _abilityHistory.createAbilityHistory)(newAbilities, this.dataCache.currentAbilities, this.dataCache.abilityHistory, currentTime);
        this.dataCache.currentAbilities = newAbilities;
      } else {
        console.log(`Issue starting ability/skill history creation. newItems: ${newAbilities} || currentTime: ${currentTime}`);
      }
    } else {
      console.log(`Issue finding data to create ability/skill history.`);
    }
  }

  setupAgent() {
    return new _https.Agent({
      rejectUnauthorized: false
    });
  }

  buildCacheObject() {
    const cache = {
      abilityHistory: [],
      itemHistory: [],
      currentAbilities: {
        Q: undefined,
        W: undefined,
        E: undefined,
        Passive: undefined,
        R: undefined
      },
      currentItems: [],
      allPlayers: {}
    };
    return cache;
  }

}

exports.LeagueOfLegendsSidecarClient = LeagueOfLegendsSidecarClient;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9sZWFndWUtb2YtbGVnZW5kcy9jbGllbnQudHMiXSwibmFtZXMiOlsidG9rZW5FeHBpcmVkU3R1YiIsIl9mbiIsImNvbnNvbGUiLCJlcnJvciIsIkRFRkFVTFRfT1BUSU9OUyIsInBvbGxpbmdJbnRlcnZhbCIsImJyb2FkY2FzdGVySUQiLCJhcHBUb2tlbklEIiwiTGVhZ3VlT2ZMZWdlbmRzU2lkZWNhckNsaWVudCIsIlNpZGVjYXJDbGllbnQiLCJjb25zdHJ1Y3RvciIsIm9wdGlvbnMiLCJQT1JUIiwiSE9TVE5BTUUiLCJtZGFhcyIsImRlZmF1bHQiLCJjb25maWdPcHRpb25zIiwiT2JqZWN0IiwiYXNzaWduIiwibGN1Q29ubmVjdG9yIiwiTENVQ29ubmVjdG9yIiwibGN1RXZlbnRFbWl0dGVyIiwiRXZlbnRFbWl0dGVyIiwiZGF0YUNhY2hlIiwiYnVpbGRDYWNoZU9iamVjdCIsImluaXRpYWxpemVMQ1VDb25uZWN0b3IiLCJzdGFydExDVUNvbm5lY3RvciIsImxlYWd1ZUFwaVBvbGxUaW1lciIsInVuZGVmaW5lZCIsIm9uIiwiZGF0YSIsImxjdU1ldGFkYXRhIiwibGN1UnVubmluZyIsImxjdVJlYWR5Q2hlY2tJbnRlcnZhbCIsInNldEludGVydmFsIiwiaXNSZWFkeSIsImxjdVJlYWR5Q2hlY2siLCJlbWl0IiwiY2xlYXJJbnRlcnZhbCIsImUiLCJsb2ciLCJyZWdpb24iLCJsY3VHZXREYXRhIiwic3RhcnQiLCJzdG9wTENVQ29ubmVjdG9yIiwic3RvcCIsImFnZW50Iiwic2V0dXBBZ2VudCIsImxjdUJhc2VVcmwiLCJwcm90b2NvbCIsImFkZHJlc3MiLCJwb3J0IiwiaXNSZWFkeVJlc3BvbnNlIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJCdWZmZXIiLCJmcm9tIiwidXNlcm5hbWUiLCJwYXNzd29yZCIsInRvU3RyaW5nIiwib2siLCJFcnJvciIsInJlYWR5UmVzcG9uc2VUZXh0IiwidGV4dCIsInBsYXRmb3JtUmVzcG9uc2UiLCJib2R5IiwianNvbiIsIkxvZ2luRGF0YVBhY2tldCIsInBsYXRmb3JtSWQiLCJpc1Jlc3BvbnNpdmUiLCJyZXNwb25zZSIsIkFDVElWRVBMQVlFUk5BTUVfVVJMIiwiZ2V0RGF0YUZyb21Mb0xDbGllbnQiLCJlbmRQb2ludFVSTCIsInN1bW1vbmVyTmFtZSIsImdldEdhbWVJbmZvIiwiZ2FtZURhdGEiLCJnZXRBbGxHYW1lRGF0YSIsImlzRGF0YUNhY2hlVmFsaWQiLCJpc1BheWxvYWRHb29kIiwiYXBwZW5kSXRlbUhpc3RvcnkiLCJhcHBlbmRBYmlsaXR5SGlzdG9yeSIsImFsbFBsYXllcnMiLCJnZXRBY3RpdmVQbGF5ZXIiLCJBQ1RJVkVQTEFZRVJfVVJMIiwiZ2V0QWN0aXZlUGxheWVyQWJpbGl0aWVzIiwiQUNUSVZFUExBWUVSQUJJTElUSUVTX1VSTCIsImdldEFjdGl2ZVBsYXllck5hbWUiLCJnZXRBY3RpdmVQbGF5ZXJSdW5lcyIsIkFDVElWRVBMQVlFUlJVTkVTX1VSTCIsIkFMTEdBTUVEQVRBX1VSTCIsImdldEV2ZW50cyIsIkVWRU5UREFUQV9VUkwiLCJnZXRHYW1lU3RhdHMiLCJHQU1FU1RBVFNfVVJMIiwiZ2V0UGxheWVyTGlzdCIsIlBMQVlFUkxJU1RfVVJMIiwiZ2V0UGxheWVyTWFpblJ1bmVzIiwiUExBWUVSTUFJTlJVTkVTX1VSTCIsImdldFBsYXllclN1bW1vbmVyU3BlbGxzIiwiUExBWUVSU1VNTU9ORVJTUEVMTFNfVVJMIiwiZ2V0UGxheWVySXRlbXMiLCJQTEFZRVJJVEVNU19VUkwiLCJnZXRQbGF5ZXJTY29yZXMiLCJQTEFZRVJTQ09SRVNfVVJMIiwic2V0dXBNRGFhU0Nvbm5lY3Rpb24iLCJpbml0aWFsUGF5bG9hZCIsImNvbmZpZyIsInRva2VuIiwiYnJvYWRjYXN0ZXJJZHMiLCJnYW1lSWQiLCJMT0xHQU1FSUQiLCJlbnZpcm9ubWVudCIsIm9uVG9rZW5FeHBpcmVkIiwiaW5pdGlhbERhdGEiLCJ0aW1lb3V0TXMiLCJkZWJ1Z0ZuIiwicHJvY2VzcyIsImVudiIsIlRXSVRDSF9MT0xfU0lERUNBUl9ERUJVRyIsImRpc2Nvbm5lY3QiLCJNRGFhU0Nvbm5lY3Rpb24iLCJhY3RpdmVTZXNzaW9uSWQiLCJkYXRhRmxvd1J1bm5pbmciLCJjb25uZWN0aW9uTG9jayIsInNlbmRBbGxHYW1lRGF0YSIsInBheWxvYWQiLCJ1cGRhdGVEYXRhIiwidXBkYXRlU2Vzc2lvbklkIiwic3RhcnREYXRhRmxvdyIsImNvbm5lY3Rpb25SZXRyeSIsImNvbm5lY3QiLCJhdHRlbXB0Q2xlYXJDb25uZWN0aW9uIiwibGVuZ3RoIiwiYWN0aXZlUGxheWVyIiwiYWN0aXZlUGxheWVyU3VtbW9uZXJOYW1lIiwiY3VycmVudFRpbWUiLCJnYW1lVGltZSIsImFjdGl2ZVBsYXllckluZGV4IiwiZmluZEluZGV4IiwicCIsIm5ld0l0ZW1zIiwiaXRlbXMiLCJpdGVtSGlzdG9yeSIsImN1cnJlbnRJdGVtcyIsImFiaWxpdGllcyIsIm5ld0FiaWxpdGllcyIsImFiaWxpdHlIaXN0b3J5IiwiY3VycmVudEFiaWxpdGllcyIsIkFnZW50IiwicmVqZWN0VW5hdXRob3JpemVkIiwiY2FjaGUiLCJRIiwiVyIsIkUiLCJQYXNzaXZlIiwiUiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUtBOztBQW1CQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLE1BQU1BLGdCQUFzQyxHQUFJQyxHQUFELElBQStCO0FBQzVFQyxFQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBYyxvQ0FBZDtBQUNBLFNBQU8sS0FBUDtBQUNELENBSEQ7O0FBV0EsTUFBTUMsZUFBb0MsR0FBRztBQUMzQ0MsRUFBQUEsZUFBZSxFQUFFLElBRDBCO0FBRTNDQyxFQUFBQSxhQUFhLEVBQUUsRUFGNEI7QUFHM0NDLEVBQUFBLFVBQVUsRUFBRTtBQUgrQixDQUE3Qzs7QUFNTyxNQUFNQyw0QkFBTixTQUEyQ0MsbUJBQTNDLENBQXlEO0FBQzlEO0FBb0JBQyxFQUFBQSxXQUFXLENBQUNDLE9BQTRCLEdBQUdQLGVBQWhDLEVBQWlEO0FBQzFELFVBQU1RLGVBQU4sRUFBWUMsbUJBQVo7O0FBRDBELDZDQW5CbkNDLEtBQUssQ0FBQ0MsT0FBTixFQW1CbUM7O0FBQUEsNkNBbEIxQixLQWtCMEI7O0FBQUE7O0FBQUEsd0NBZi9CLEtBZStCOztBQUFBOztBQUFBLHNDQWJqQyxLQWFpQzs7QUFBQTs7QUFBQTs7QUFBQSw2Q0FWM0IsRUFVMkI7O0FBQUE7O0FBQUEsOENBUHhCLEtBT3dCOztBQUFBOztBQUFBLDRDQUpuQyxLQUltQzs7QUFBQTs7QUFFMUQsU0FBS0MsYUFBTCxHQUFxQkMsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQmQsZUFBbEIsRUFBbUNPLE9BQW5DLENBQXJCO0FBQ0EsU0FBS1EsWUFBTCxHQUFvQixJQUFJQyxxQkFBSixDQUFpQixFQUFqQixDQUFwQjtBQUNBLFNBQUtDLGVBQUwsR0FBdUIsSUFBSUMsb0JBQUosRUFBdkI7QUFDQSxTQUFLQyxTQUFMLEdBQWlCLEtBQUtDLGdCQUFMLEVBQWpCO0FBRUEsU0FBS0Msc0JBQUw7QUFDQSxTQUFLQyxpQkFBTDtBQUNBLFNBQUtDLGtCQUFMLEdBQTBCQyxTQUExQjtBQUNEOztBQUVNSCxFQUFBQSxzQkFBUCxHQUFnQztBQUM5QixTQUFLTixZQUFMLENBQWtCVSxFQUFsQixDQUFxQixTQUFyQixFQUFpQ0MsSUFBRCxJQUF1QjtBQUNyRCxXQUFLQyxXQUFMLEdBQW1CRCxJQUFuQjtBQUNBLFdBQUtFLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxZQUFNQyxxQkFBcUIsR0FBR0MsV0FBVyxDQUFDLFlBQVk7QUFDcEQsWUFBSTtBQUNGLGdCQUFNQyxPQUFPLEdBQUcsTUFBTSxLQUFLQyxhQUFMLEVBQXRCOztBQUNBLGNBQUlELE9BQUosRUFBYTtBQUNYLGlCQUFLZCxlQUFMLENBQXFCZ0IsSUFBckIsQ0FBMEIsT0FBMUI7QUFDQUMsWUFBQUEsYUFBYSxDQUFDTCxxQkFBRCxDQUFiO0FBQ0Q7QUFDRixTQU5ELENBTUUsT0FBT00sQ0FBUCxFQUFVO0FBQ1ZyQyxVQUFBQSxPQUFPLENBQUNzQyxHQUFSLENBQVlELENBQVo7QUFDRDtBQUNGLE9BVndDLEVBVXRDLEdBVnNDLENBQXpDO0FBV0EsV0FBS0gsYUFBTDtBQUNELEtBZkQ7QUFpQkEsU0FBS2pCLFlBQUwsQ0FBa0JVLEVBQWxCLENBQXFCLFlBQXJCLEVBQW1DLE1BQU07QUFDdkMsV0FBS0csVUFBTCxHQUFrQixLQUFsQjtBQUNBLFdBQUtELFdBQUwsR0FBbUJILFNBQW5CO0FBQ0EsV0FBS0wsU0FBTCxDQUFla0IsTUFBZixHQUF3QmIsU0FBeEI7QUFDRCxLQUpEO0FBTUEsU0FBS1AsZUFBTCxDQUFxQlEsRUFBckIsQ0FBd0IsT0FBeEIsRUFBaUMsTUFBTTtBQUNyQyxXQUFLYSxVQUFMO0FBQ0QsS0FGRDtBQUdEOztBQUVNaEIsRUFBQUEsaUJBQVAsR0FBMkI7QUFDekIsU0FBS1AsWUFBTCxDQUFrQndCLEtBQWxCO0FBQ0Q7O0FBRU1DLEVBQUFBLGdCQUFQLEdBQTBCO0FBQ3hCLFNBQUt6QixZQUFMLENBQWtCMEIsSUFBbEI7QUFDRDs7QUFFRCxRQUFhVCxhQUFiLEdBQTZCO0FBQzNCLFVBQU1VLEtBQUssR0FBRyxLQUFLQyxVQUFMLEVBQWQ7O0FBQ0EsUUFBSSxLQUFLZixVQUFMLElBQW1CLEtBQUtELFdBQTVCLEVBQXlDO0FBQ3ZDLFlBQU1pQixVQUFVLEdBQUksR0FBRSxLQUFLakIsV0FBTCxDQUFpQmtCLFFBQVMsTUFBSyxLQUFLbEIsV0FBTCxDQUFpQm1CLE9BQVEsSUFBRyxLQUFLbkIsV0FBTCxDQUFpQm9CLElBQUssRUFBdkc7QUFFQSxZQUFNQyxlQUFlLEdBQUcsTUFBTSx3QkFBTSw4QkFBY0osVUFBZCxDQUFOLEVBQWlDO0FBQzdERixRQUFBQSxLQUQ2RDtBQUU3RE8sUUFBQUEsT0FBTyxFQUFFO0FBQ1BDLFVBQUFBLGFBQWEsRUFBRyxTQUFRQyxNQUFNLENBQUNDLElBQVAsQ0FBYSxHQUFFLEtBQUt6QixXQUFMLENBQWlCMEIsUUFBUyxJQUFHLEtBQUsxQixXQUFMLENBQWlCMkIsUUFBUyxFQUF0RSxFQUF5RUMsUUFBekUsQ0FDdEIsUUFEc0IsQ0FFdEI7QUFISztBQUZvRCxPQUFqQyxDQUE5Qjs7QUFTQSxVQUFJLENBQUNQLGVBQWUsQ0FBQ1EsRUFBckIsRUFBeUI7QUFDdkIsY0FBTSxJQUFJQyxLQUFKLENBQVUsbUJBQVYsQ0FBTjtBQUNEOztBQUVELFlBQU1DLGlCQUFpQixHQUFHLE1BQU1WLGVBQWUsQ0FBQ1csSUFBaEIsRUFBaEM7QUFDQSxhQUFPRCxpQkFBaUIsS0FBSyxNQUE3QjtBQUNEOztBQUNELFdBQU8sS0FBUDtBQUNEOztBQUVELFFBQWFwQixVQUFiLEdBQTBCO0FBQ3hCLFVBQU1JLEtBQUssR0FBRyxLQUFLQyxVQUFMLEVBQWQ7O0FBQ0EsUUFBSSxLQUFLWCxhQUFMLElBQXNCLEtBQUtMLFdBQS9CLEVBQTRDO0FBQzFDLFlBQU1pQixVQUFVLEdBQUksR0FBRSxLQUFLakIsV0FBTCxDQUFpQmtCLFFBQVMsTUFBSyxLQUFLbEIsV0FBTCxDQUFpQm1CLE9BQVEsSUFBRyxLQUFLbkIsV0FBTCxDQUFpQm9CLElBQUssRUFBdkc7QUFDQSxZQUFNYSxnQkFBZ0IsR0FBRyxNQUFNLHdCQUFNLGdDQUFnQmhCLFVBQWhCLENBQU4sRUFBbUM7QUFDaEVGLFFBQUFBLEtBRGdFO0FBRWhFTyxRQUFBQSxPQUFPLEVBQUU7QUFDUEMsVUFBQUEsYUFBYSxFQUFHLFNBQVFDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFhLEdBQUUsS0FBS3pCLFdBQUwsQ0FBaUIwQixRQUFTLElBQUcsS0FBSzFCLFdBQUwsQ0FBaUIyQixRQUFTLEVBQXRFLEVBQXlFQyxRQUF6RSxDQUN0QixRQURzQixDQUV0QjtBQUhLO0FBRnVELE9BQW5DLENBQS9COztBQVNBLFVBQUlLLGdCQUFnQixDQUFDSixFQUFyQixFQUF5QjtBQUN2QixjQUFNSyxJQUFJLEdBQUcsTUFBTUQsZ0JBQWdCLENBQUNFLElBQWpCLEVBQW5CO0FBQ0EsYUFBSzNDLFNBQUwsQ0FBZWtCLE1BQWYsR0FBd0J3QixJQUFJLENBQUNFLGVBQUwsQ0FBcUJDLFVBQTdDO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFFBQWFDLFlBQWIsR0FBOEM7QUFDNUMsVUFBTXZCLEtBQUssR0FBRyxLQUFLQyxVQUFMLEVBQWQ7QUFDQSxVQUFNdUIsUUFBUSxHQUFHLE1BQU0sd0JBQU1DLCtCQUFOLEVBQTRCO0FBQUV6QixNQUFBQTtBQUFGLEtBQTVCLENBQXZCO0FBQ0EsV0FBT3dCLFFBQVEsQ0FBQ1YsRUFBaEI7QUFDRDs7QUFFRCxRQUFhWSxvQkFBYixDQUFrQ0MsV0FBbEMsRUFBdURDLFlBQW9CLEdBQUcsRUFBOUUsRUFBa0Y7QUFDaEYsVUFBTTVCLEtBQUssR0FBRyxLQUFLQyxVQUFMLEVBQWQ7QUFDQSxVQUFNdUIsUUFBUSxHQUFHLE1BQU0sd0JBQU8sR0FBRUcsV0FBWSxHQUFFQyxZQUFhLEVBQXBDLEVBQXVDO0FBQUU1QixNQUFBQTtBQUFGLEtBQXZDLENBQXZCO0FBQ0EsV0FBTyxNQUFNd0IsUUFBUSxDQUFDSixJQUFULEVBQWI7QUFDRDs7QUFFRCxRQUFhUyxXQUFiLEdBQTJCO0FBQ3pCLFVBQU1DLFFBQXFCLEdBQUcsTUFBTSxLQUFLQyxjQUFMLEVBQXBDOztBQUVBLFFBQUksS0FBS3RELFNBQUwsQ0FBZWtCLE1BQWYsS0FBMEJiLFNBQTlCLEVBQXlDO0FBQ3ZDLFlBQU0sS0FBS1EsYUFBTCxFQUFOO0FBQ0EsWUFBTSxLQUFLTSxVQUFMLEVBQU47QUFDRDs7QUFDRCxTQUFLb0MsZ0JBQUwsR0FBd0IsS0FBS0MsYUFBTCxDQUFtQkgsUUFBbkIsQ0FBeEI7O0FBRUEsUUFBSSxLQUFLRSxnQkFBVCxFQUEyQjtBQUN6QixXQUFLdkQsU0FBTCxDQUFlcUQsUUFBZixHQUEwQkEsUUFBMUI7QUFDQSxXQUFLSSxpQkFBTCxDQUF1QkosUUFBdkI7QUFDQSxXQUFLSyxvQkFBTCxDQUEwQkwsUUFBMUI7O0FBQ0EsVUFBSUEsUUFBUSxDQUFDTSxVQUFiLEVBQXlCO0FBQ3ZCLGFBQUszRCxTQUFMLENBQWUyRCxVQUFmLEdBQTRCLGdDQUFnQk4sUUFBUSxDQUFDTSxVQUF6QixFQUFxQyxLQUFLM0QsU0FBTCxDQUFlMkQsVUFBcEQsQ0FBNUI7QUFDRDtBQUNGO0FBQ0YsR0EvSTZELENBaUo5RDs7O0FBQ0EsUUFBYUMsZUFBYixHQUFnRDtBQUM5QyxXQUFPLEtBQUtYLG9CQUFMLENBQTBCWSwyQkFBMUIsQ0FBUDtBQUNEOztBQUNELFFBQWFDLHdCQUFiLEdBQXlEO0FBQ3ZELFdBQU8sS0FBS2Isb0JBQUwsQ0FBMEJjLG9DQUExQixDQUFQO0FBQ0Q7O0FBQ0QsUUFBYUMsbUJBQWIsR0FBb0Q7QUFDbEQsV0FBTyxLQUFLZixvQkFBTCxDQUEwQkQsK0JBQTFCLENBQVA7QUFDRDs7QUFDRCxRQUFhaUIsb0JBQWIsR0FBcUQ7QUFDbkQsV0FBTyxLQUFLaEIsb0JBQUwsQ0FBMEJpQixnQ0FBMUIsQ0FBUDtBQUNEOztBQUNELFFBQWFaLGNBQWIsR0FBb0Q7QUFDbEQsV0FBTyxLQUFLTCxvQkFBTCxDQUEwQmtCLDBCQUExQixDQUFQO0FBQ0Q7O0FBQ0QsUUFBYUMsU0FBYixHQUEwQztBQUN4QyxXQUFPLEtBQUtuQixvQkFBTCxDQUEwQm9CLHdCQUExQixDQUFQO0FBQ0Q7O0FBQ0QsUUFBYUMsWUFBYixHQUE2QztBQUMzQyxXQUFPLEtBQUtyQixvQkFBTCxDQUEwQnNCLHdCQUExQixDQUFQO0FBQ0Q7O0FBQ0QsUUFBYUMsYUFBYixHQUE4QztBQUM1QyxXQUFPLEtBQUt2QixvQkFBTCxDQUEwQndCLHlCQUExQixDQUFQO0FBQ0QsR0F6SzZELENBMks5RDs7O0FBQ0EsUUFBYUMsa0JBQWIsQ0FBZ0N2QixZQUFoQyxFQUF1RTtBQUNyRSxXQUFPLEtBQUtGLG9CQUFMLENBQTBCMEIsOEJBQTFCLEVBQStDeEIsWUFBL0MsQ0FBUDtBQUNEOztBQUNELFFBQWF5Qix1QkFBYixDQUFxQ3pCLFlBQXJDLEVBQTRFO0FBQzFFLFdBQU8sS0FBS0Ysb0JBQUwsQ0FBMEI0QixtQ0FBMUIsRUFBb0QxQixZQUFwRCxDQUFQO0FBQ0Q7O0FBQ0QsUUFBYTJCLGNBQWIsQ0FBNEIzQixZQUE1QixFQUFtRTtBQUNqRSxXQUFPLEtBQUtGLG9CQUFMLENBQTBCOEIsMEJBQTFCLEVBQTJDNUIsWUFBM0MsQ0FBUDtBQUNEOztBQUNELFFBQWE2QixlQUFiLENBQTZCN0IsWUFBN0IsRUFBb0U7QUFDbEUsV0FBTyxLQUFLRixvQkFBTCxDQUEwQmdDLDJCQUExQixFQUE0QzlCLFlBQTVDLENBQVA7QUFDRCxHQXZMNkQsQ0F3TDlEOzs7QUFFQSxRQUFhK0Isb0JBQWIsQ0FBa0NDLGNBQWxDLEVBQThEO0FBQzVELFNBQUtDLE1BQUwsR0FBYztBQUNaQyxNQUFBQSxLQUFLLEVBQUUsS0FBSzVGLGFBQUwsQ0FBbUJULFVBRGQ7QUFFWnNHLE1BQUFBLGNBQWMsRUFBRSxFQUZKO0FBR1pDLE1BQUFBLE1BQU0sRUFBRUMsb0JBSEk7QUFJWkMsTUFBQUEsV0FBVyxFQUFFLE1BSkQ7QUFLWkMsTUFBQUEsY0FBYyxFQUFFakgsZ0JBTEo7QUFNWmtILE1BQUFBLFdBQVcsRUFBRVIsY0FORDtBQU9aUyxNQUFBQSxTQUFTLEVBQUUsSUFQQztBQVFaQyxNQUFBQSxPQUFPLEVBQUVDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyx3QkFBWixLQUF5QztBQVJ0QyxLQUFkO0FBVUQ7O0FBRUQsUUFBYUMsVUFBYixHQUEwQjtBQUN4QixTQUFLQyxlQUFMLENBQXFCRCxVQUFyQjtBQUNBLFNBQUtFLGVBQUwsR0FBdUIsRUFBdkI7QUFDQSxTQUFLQyxlQUFMLEdBQXVCLEtBQXZCO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixLQUF0QjtBQUNEOztBQUVELFFBQWFDLGVBQWIsR0FBOEM7QUFDNUMsVUFBTUMsT0FBTyxHQUFHLGlDQUFtQixLQUFLdkcsU0FBeEIsQ0FBaEI7QUFDQSxVQUFNLEtBQUtrRyxlQUFMLENBQXFCTSxVQUFyQixDQUFnQ0QsT0FBaEMsQ0FBTjtBQUNEOztBQUVNakYsRUFBQUEsSUFBUCxHQUFjO0FBQ1osUUFBSSxLQUFLbEIsa0JBQVQsRUFBNkI7QUFDM0JXLE1BQUFBLGFBQWEsQ0FBQyxLQUFLWCxrQkFBTixDQUFiO0FBQ0EsV0FBS0Esa0JBQUwsR0FBMEJDLFNBQTFCO0FBQ0EsV0FBSzRGLFVBQUw7QUFDRDtBQUNGOztBQUVNN0UsRUFBQUEsS0FBUCxDQUFhcUYsZUFBYixFQUF5QztBQUN2QyxRQUFJLENBQUMsS0FBS3JHLGtCQUFWLEVBQThCO0FBQzVCLFdBQUtKLFNBQUwsR0FBaUIsS0FBS0MsZ0JBQUwsRUFBakI7QUFDQSxXQUFLeUcsYUFBTCxDQUFtQkQsZUFBbkI7QUFDRDtBQUNGOztBQUVELFFBQWFDLGFBQWIsQ0FBMkJELGVBQTNCLEVBQXVEO0FBQ3JELFNBQUtMLGVBQUwsR0FBdUIsSUFBdkI7O0FBQ0EsUUFBSSxDQUFDLEtBQUszRyxhQUFMLENBQW1CVCxVQUF4QixFQUFvQztBQUNsQ0wsTUFBQUEsT0FBTyxDQUFDQyxLQUFSLENBQWMsb0JBQWQ7QUFDRDs7QUFDRCxRQUFJK0gsZUFBZSxHQUFHLENBQXRCO0FBQ0EsU0FBS3ZHLGtCQUFMLEdBQTBCTyxXQUFXLENBQUMsWUFBWTtBQUNoRCxVQUFJO0FBQ0YsWUFBSSxLQUFLMEYsY0FBTCxJQUF1QixDQUFDLEtBQUtELGVBQWpDLEVBQWtEO0FBQ2hEO0FBQ0Q7O0FBQ0QsY0FBTSxLQUFLaEQsV0FBTCxFQUFOOztBQUNBLFlBQUksS0FBS0csZ0JBQVQsRUFBMkI7QUFDekIsZUFBSzhDLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxnQkFBTUUsT0FBTyxHQUFHLGlDQUFtQixLQUFLdkcsU0FBeEIsQ0FBaEI7O0FBQ0EsY0FBSSxDQUFDLEtBQUttRyxlQUFWLEVBQTJCO0FBQ3pCLGtCQUFNLEtBQUtqQixvQkFBTCxDQUEwQnFCLE9BQTFCLENBQU47QUFDQSxpQkFBS0osZUFBTCxHQUF1QixNQUFNLEtBQUtELGVBQUwsQ0FBcUJVLE9BQXJCLENBQTZCLEtBQUt4QixNQUFsQyxDQUE3Qjs7QUFDQSxnQkFBSSxLQUFLZSxlQUFMLElBQXdCTSxlQUFlLEtBQUtwRyxTQUFoRCxFQUEyRDtBQUN6RDtBQUNBb0csY0FBQUEsZUFBZTtBQUNoQixhQUhELE1BR087QUFDTEUsY0FBQUEsZUFBZTs7QUFDZixrQkFBSUEsZUFBZSxHQUFHLEVBQXRCLEVBQTBCO0FBQ3hCLHFCQUFLRSxzQkFBTDtBQUNBRixnQkFBQUEsZUFBZSxHQUFHLENBQWxCO0FBQ0Q7QUFDRjtBQUNGLFdBYkQsTUFhTztBQUNMLGlCQUFLVCxlQUFMLENBQXFCTSxVQUFyQixDQUFnQ0QsT0FBaEM7QUFDRDs7QUFDRCxlQUFLRixjQUFMLEdBQXNCLEtBQXRCO0FBQ0Q7QUFDRixPQTFCRCxDQTBCRSxPQUFPckYsQ0FBUCxFQUFVO0FBQ1ZyQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY29DLENBQWQ7QUFDRDtBQUNGLEtBOUJvQyxFQThCbEMsS0FBS3ZCLGFBQUwsQ0FBbUJYLGVBOUJlLENBQXJDO0FBK0JEOztBQUNPK0gsRUFBQUEsc0JBQVIsR0FBaUM7QUFDL0IsU0FBSzdHLFNBQUwsR0FBaUIsS0FBS0MsZ0JBQUwsRUFBakI7QUFDQSxTQUFLaUcsZUFBTCxDQUFxQkQsVUFBckI7QUFDQSxTQUFLRSxlQUFMLEdBQXVCLEVBQXZCO0FBQ0Q7O0FBRU8zQyxFQUFBQSxhQUFSLENBQXNCK0MsT0FBdEIsRUFBdUQ7QUFDckQsV0FBT0EsT0FBTyxJQUFJQSxPQUFPLENBQUM1QyxVQUFuQixJQUFpQzRDLE9BQU8sQ0FBQzVDLFVBQVIsQ0FBbUJtRCxNQUFuQixHQUE0QixDQUFwRTtBQUNEOztBQUVPckQsRUFBQUEsaUJBQVIsQ0FBMEJKLFFBQTFCLEVBQWlEO0FBQy9DLFFBQ0VBLFFBQVEsS0FBS2hELFNBQWIsSUFDQWdELFFBQVEsQ0FBQ0EsUUFBVCxLQUFzQmhELFNBRHRCLElBRUFnRCxRQUFRLENBQUMwRCxZQUFULEtBQTBCMUcsU0FGMUIsSUFHQWdELFFBQVEsQ0FBQzBELFlBQVQsQ0FBc0I1RCxZQUF0QixLQUF1QzlDLFNBSHZDLElBSUFnRCxRQUFRLENBQUNNLFVBQVQsS0FBd0J0RCxTQUwxQixFQU1FO0FBQ0EsWUFBTTJHLHdCQUF3QixHQUFHM0QsUUFBUSxDQUFDMEQsWUFBVCxDQUFzQjVELFlBQXZEO0FBQ0EsWUFBTThELFdBQVcsR0FBRzVELFFBQVEsQ0FBQ0EsUUFBVCxDQUFrQjZELFFBQXRDO0FBQ0EsWUFBTUMsaUJBQXlCLEdBQUc5RCxRQUFRLENBQUNNLFVBQVQsQ0FBb0J5RCxTQUFwQixDQUE4QkMsQ0FBQyxJQUFJQSxDQUFDLENBQUNsRSxZQUFGLEtBQW1CNkQsd0JBQXRELENBQWxDO0FBQ0EsWUFBTU0sUUFBUSxHQUFHakUsUUFBUSxDQUFDTSxVQUFULENBQW9Cd0QsaUJBQXBCLEVBQXVDSSxLQUF4RDs7QUFDQSxVQUFJRCxRQUFRLEtBQUtqSCxTQUFiLElBQTBCNEcsV0FBVyxLQUFLNUcsU0FBOUMsRUFBeUQ7QUFDdkQsYUFBS0wsU0FBTCxDQUFld0gsV0FBZixHQUE2QixvQ0FDM0JGLFFBRDJCLEVBRTNCLEtBQUt0SCxTQUFMLENBQWV5SCxZQUZZLEVBRzNCLEtBQUt6SCxTQUFMLENBQWV3SCxXQUhZLEVBSTNCUCxXQUoyQixDQUE3QjtBQU1BLGFBQUtqSCxTQUFMLENBQWV5SCxZQUFmLEdBQThCSCxRQUE5QjtBQUNELE9BUkQsTUFRTztBQUNMM0ksUUFBQUEsT0FBTyxDQUFDc0MsR0FBUixDQUFhLG1EQUFrRHFHLFFBQVMsb0JBQW1CTCxXQUFZLEVBQXZHO0FBQ0Q7QUFDRixLQXRCRCxNQXNCTztBQUNMdEksTUFBQUEsT0FBTyxDQUFDc0MsR0FBUixDQUFhLDRDQUFiO0FBQ0Q7QUFDRjs7QUFFT3lDLEVBQUFBLG9CQUFSLENBQTZCTCxRQUE3QixFQUFvRDtBQUNsRCxRQUNFQSxRQUFRLEtBQUtoRCxTQUFiLElBQ0FnRCxRQUFRLENBQUNBLFFBQVQsS0FBc0JoRCxTQUR0QixJQUVBZ0QsUUFBUSxDQUFDMEQsWUFBVCxLQUEwQjFHLFNBRjFCLElBR0FnRCxRQUFRLENBQUMwRCxZQUFULENBQXNCVyxTQUp4QixFQUtFO0FBQ0EsWUFBTVQsV0FBVyxHQUFHNUQsUUFBUSxDQUFDQSxRQUFULENBQWtCNkQsUUFBdEM7QUFDQSxZQUFNUyxZQUFZLEdBQUd0RSxRQUFRLENBQUMwRCxZQUFULENBQXNCVyxTQUEzQzs7QUFDQSxVQUFJQyxZQUFZLEtBQUt0SCxTQUFqQixJQUE4QjRHLFdBQVcsS0FBSzVHLFNBQWxELEVBQTZEO0FBQzNELGFBQUtMLFNBQUwsQ0FBZTRILGNBQWYsR0FBZ0MsMENBQzlCRCxZQUQ4QixFQUU5QixLQUFLM0gsU0FBTCxDQUFlNkgsZ0JBRmUsRUFHOUIsS0FBSzdILFNBQUwsQ0FBZTRILGNBSGUsRUFJOUJYLFdBSjhCLENBQWhDO0FBTUEsYUFBS2pILFNBQUwsQ0FBZTZILGdCQUFmLEdBQWtDRixZQUFsQztBQUNELE9BUkQsTUFRTztBQUNMaEosUUFBQUEsT0FBTyxDQUFDc0MsR0FBUixDQUNHLDREQUEyRDBHLFlBQWEsb0JBQW1CVixXQUFZLEVBRDFHO0FBR0Q7QUFDRixLQXJCRCxNQXFCTztBQUNMdEksTUFBQUEsT0FBTyxDQUFDc0MsR0FBUixDQUFhLHFEQUFiO0FBQ0Q7QUFDRjs7QUFFT08sRUFBQUEsVUFBUixHQUFxQjtBQUNuQixXQUFPLElBQUlzRyxZQUFKLENBQVU7QUFDZkMsTUFBQUEsa0JBQWtCLEVBQUU7QUFETCxLQUFWLENBQVA7QUFHRDs7QUFFTzlILEVBQUFBLGdCQUFSLEdBQXFEO0FBQ25ELFVBQU0rSCxLQUFLLEdBQUc7QUFDWkosTUFBQUEsY0FBYyxFQUFFLEVBREo7QUFFWkosTUFBQUEsV0FBVyxFQUFFLEVBRkQ7QUFHWkssTUFBQUEsZ0JBQWdCLEVBQUU7QUFBRUksUUFBQUEsQ0FBQyxFQUFFNUgsU0FBTDtBQUFnQjZILFFBQUFBLENBQUMsRUFBRTdILFNBQW5CO0FBQThCOEgsUUFBQUEsQ0FBQyxFQUFFOUgsU0FBakM7QUFBNEMrSCxRQUFBQSxPQUFPLEVBQUUvSCxTQUFyRDtBQUFnRWdJLFFBQUFBLENBQUMsRUFBRWhJO0FBQW5FLE9BSE47QUFJWm9ILE1BQUFBLFlBQVksRUFBRSxFQUpGO0FBS1o5RCxNQUFBQSxVQUFVLEVBQUU7QUFMQSxLQUFkO0FBUUEsV0FBT3FFLEtBQVA7QUFDRDs7QUF6VjZEIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW1wb3J0IHsgQ29uZmlndXJhdGlvbiwgRGF0YSwgSURhdGFTb3VyY2UsIFRva2VuRXhwaXJlZEZuLCBUb2tlblJlZnJlc2hGbiB9IGZyb20gJ2VuaGFuY2VkLWV4cGVyaWVuY2VzLXNkayc7XG5pbXBvcnQgKiBhcyBtZGFhcyBmcm9tICdlbmhhbmNlZC1leHBlcmllbmNlcy1zZGsnO1xuaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnZXZlbnRzJztcbmltcG9ydCB7IEFnZW50IH0gZnJvbSAnaHR0cHMnO1xuaW1wb3J0IExDVUNvbm5lY3RvciBmcm9tICdsY3UtY29ubmVjdG9yJztcbmltcG9ydCBmZXRjaCBmcm9tICdub2RlLWZldGNoJztcbmltcG9ydCB7IFNpZGVjYXJDbGllbnQgfSBmcm9tICcuLi9iYXNlJztcbmltcG9ydCB7IExDVU1ldGFkYXRhIH0gZnJvbSAnLi4vbW9kZWxzL2xjdS1tZXRhZGF0YSc7XG5pbXBvcnQgeyBBbGxHYW1lRGF0YSB9IGZyb20gJy4uL21vZGVscy9sZWFndWUtYXBpL2FsbC1nYW1lLWRhdGEnO1xuaW1wb3J0IHsgTGVhZ3VlT2ZMZWdlbmRzRGF0YUNhY2hlIH0gZnJvbSAnLi4vbW9kZWxzL2xlYWd1ZS1kYXRhLWNhY2hlJztcbmltcG9ydCB7IE1EYWFTUGF5bG9hZCB9IGZyb20gJy4vLi4vbW9kZWxzL21kYWFzLXBheWxvYWQnO1xuaW1wb3J0IHtcbiAgQUNUSVZFUExBWUVSX1VSTCxcbiAgQUNUSVZFUExBWUVSQUJJTElUSUVTX1VSTCxcbiAgQUNUSVZFUExBWUVSTkFNRV9VUkwsXG4gIEFDVElWRVBMQVlFUlJVTkVTX1VSTCxcbiAgQUxMR0FNRURBVEFfVVJMLFxuICBFVkVOVERBVEFfVVJMLFxuICBHQU1FU1RBVFNfVVJMLFxuICBIT1NUTkFNRSxcbiAgTENVX0NMSUVOVF9JTkZPLFxuICBMQ1VfUkVBRFlfVVJMLFxuICBMT0xHQU1FSUQsXG4gIFBMQVlFUklURU1TX1VSTCxcbiAgUExBWUVSTElTVF9VUkwsXG4gIFBMQVlFUk1BSU5SVU5FU19VUkwsXG4gIFBMQVlFUlNDT1JFU19VUkwsXG4gIFBMQVlFUlNVTU1PTkVSU1BFTExTX1VSTCxcbiAgUE9SVFxufSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgeyBjcmVhdGVBYmlsaXR5SGlzdG9yeSB9IGZyb20gJy4vdHJhbnNmb3Jtcy9hYmlsaXR5LWhpc3RvcnknO1xuaW1wb3J0IHsgY3JlYXRlSXRlbUhpc3RvcnkgfSBmcm9tICcuL3RyYW5zZm9ybXMvaXRlbS1oaXN0b3J5JztcbmltcG9ydCB7IGNyZWF0ZU1EYWFTUGF5bG9hZCB9IGZyb20gJy4vdHJhbnNmb3Jtcy9wYXlsb2FkJztcbmltcG9ydCB7IGNyZWF0ZVBsYXllck1hcCB9IGZyb20gJy4vdHJhbnNmb3Jtcy9wbGF5ZXItbWFwJztcblxuY29uc3QgdG9rZW5FeHBpcmVkU3R1YjogbWRhYXMuVG9rZW5FeHBpcmVkRm4gPSAoX2ZuOiBtZGFhcy5Ub2tlblJlZnJlc2hGbikgPT4ge1xuICBjb25zb2xlLmVycm9yKCd1bmV4cGVjdGVkIHRva2VuIHJlZnJlc2ggY2FsbC1iYWNrJyk7XG4gIHJldHVybiBmYWxzZTtcbn07XG5cbmV4cG9ydCBpbnRlcmZhY2UgTGVhZ3VlQ2xpZW50T3B0aW9ucyB7XG4gIHBvbGxpbmdJbnRlcnZhbDogbnVtYmVyO1xuICBicm9hZGNhc3RlcklEOiBzdHJpbmc7XG4gIGFwcFRva2VuSUQ6IHN0cmluZztcbn1cblxuY29uc3QgREVGQVVMVF9PUFRJT05TOiBMZWFndWVDbGllbnRPcHRpb25zID0ge1xuICBwb2xsaW5nSW50ZXJ2YWw6IDEwMDAsXG4gIGJyb2FkY2FzdGVySUQ6ICcnLFxuICBhcHBUb2tlbklEOiAnJ1xufTtcblxuZXhwb3J0IGNsYXNzIExlYWd1ZU9mTGVnZW5kc1NpZGVjYXJDbGllbnQgZXh0ZW5kcyBTaWRlY2FyQ2xpZW50IHtcbiAgLy8gcHVibGljIEFjdGl2ZVBsYXllck5hbWU6IHN0cmluZyB8IHVuZGVmaW5lZDtcbiAgcHVibGljIE1EYWFTQ29ubmVjdGlvbiA9IG1kYWFzLmRlZmF1bHQoKTtcbiAgcHVibGljIGRhdGFGbG93UnVubmluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIHB1YmxpYyBsY3VDb25uZWN0b3I6IExDVUNvbm5lY3RvcjtcbiAgcHVibGljIGxjdVJ1bm5pbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHVibGljIGxjdU1ldGFkYXRhPzogTENVTWV0YWRhdGE7XG4gIHB1YmxpYyBsY3VSZWFkeTogYm9vbGVhbiA9IGZhbHNlO1xuICBwdWJsaWMgY29uZmlnITogbWRhYXMuQ29uZmlndXJhdGlvbjtcbiAgcHVibGljIGNvbmZpZ09wdGlvbnM6IExlYWd1ZUNsaWVudE9wdGlvbnM7XG4gIHB1YmxpYyBhY3RpdmVTZXNzaW9uSWQ6IHN0cmluZyA9ICcnO1xuXG4gIHByaXZhdGUgbGN1RXZlbnRFbWl0dGVyOiBFdmVudEVtaXR0ZXI7XG4gIHByaXZhdGUgaXNEYXRhQ2FjaGVWYWxpZDogYm9vbGVhbiA9IGZhbHNlO1xuICBwcml2YXRlIGRhdGFDYWNoZTogTGVhZ3VlT2ZMZWdlbmRzRGF0YUNhY2hlO1xuXG4gIHByaXZhdGUgY29ubmVjdGlvbkxvY2sgPSBmYWxzZTtcblxuICBwcml2YXRlIGxlYWd1ZUFwaVBvbGxUaW1lcj86IE5vZGVKUy5UaW1lcjtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiBMZWFndWVDbGllbnRPcHRpb25zID0gREVGQVVMVF9PUFRJT05TKSB7XG4gICAgc3VwZXIoUE9SVCwgSE9TVE5BTUUpO1xuICAgIHRoaXMuY29uZmlnT3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIERFRkFVTFRfT1BUSU9OUywgb3B0aW9ucyk7XG4gICAgdGhpcy5sY3VDb25uZWN0b3IgPSBuZXcgTENVQ29ubmVjdG9yKCcnKTtcbiAgICB0aGlzLmxjdUV2ZW50RW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICB0aGlzLmRhdGFDYWNoZSA9IHRoaXMuYnVpbGRDYWNoZU9iamVjdCgpO1xuXG4gICAgdGhpcy5pbml0aWFsaXplTENVQ29ubmVjdG9yKCk7XG4gICAgdGhpcy5zdGFydExDVUNvbm5lY3RvcigpO1xuICAgIHRoaXMubGVhZ3VlQXBpUG9sbFRpbWVyID0gdW5kZWZpbmVkO1xuICB9XG5cbiAgcHVibGljIGluaXRpYWxpemVMQ1VDb25uZWN0b3IoKSB7XG4gICAgdGhpcy5sY3VDb25uZWN0b3Iub24oJ2Nvbm5lY3QnLCAoZGF0YTogTENVTWV0YWRhdGEpID0+IHtcbiAgICAgIHRoaXMubGN1TWV0YWRhdGEgPSBkYXRhO1xuICAgICAgdGhpcy5sY3VSdW5uaW5nID0gdHJ1ZTtcbiAgICAgIGNvbnN0IGxjdVJlYWR5Q2hlY2tJbnRlcnZhbCA9IHNldEludGVydmFsKGFzeW5jICgpID0+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICBjb25zdCBpc1JlYWR5ID0gYXdhaXQgdGhpcy5sY3VSZWFkeUNoZWNrKCk7XG4gICAgICAgICAgaWYgKGlzUmVhZHkpIHtcbiAgICAgICAgICAgIHRoaXMubGN1RXZlbnRFbWl0dGVyLmVtaXQoJ3JlYWR5Jyk7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKGxjdVJlYWR5Q2hlY2tJbnRlcnZhbCk7XG4gICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgIH1cbiAgICAgIH0sIDIwMCk7XG4gICAgICB0aGlzLmxjdVJlYWR5Q2hlY2soKTtcbiAgICB9KTtcblxuICAgIHRoaXMubGN1Q29ubmVjdG9yLm9uKCdkaXNjb25uZWN0JywgKCkgPT4ge1xuICAgICAgdGhpcy5sY3VSdW5uaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLmxjdU1ldGFkYXRhID0gdW5kZWZpbmVkO1xuICAgICAgdGhpcy5kYXRhQ2FjaGUucmVnaW9uID0gdW5kZWZpbmVkO1xuICAgIH0pO1xuXG4gICAgdGhpcy5sY3VFdmVudEVtaXR0ZXIub24oJ3JlYWR5JywgKCkgPT4ge1xuICAgICAgdGhpcy5sY3VHZXREYXRhKCk7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc3RhcnRMQ1VDb25uZWN0b3IoKSB7XG4gICAgdGhpcy5sY3VDb25uZWN0b3Iuc3RhcnQoKTtcbiAgfVxuXG4gIHB1YmxpYyBzdG9wTENVQ29ubmVjdG9yKCkge1xuICAgIHRoaXMubGN1Q29ubmVjdG9yLnN0b3AoKTtcbiAgfVxuXG4gIHB1YmxpYyBhc3luYyBsY3VSZWFkeUNoZWNrKCkge1xuICAgIGNvbnN0IGFnZW50ID0gdGhpcy5zZXR1cEFnZW50KCk7XG4gICAgaWYgKHRoaXMubGN1UnVubmluZyAmJiB0aGlzLmxjdU1ldGFkYXRhKSB7XG4gICAgICBjb25zdCBsY3VCYXNlVXJsID0gYCR7dGhpcy5sY3VNZXRhZGF0YS5wcm90b2NvbH06Ly8ke3RoaXMubGN1TWV0YWRhdGEuYWRkcmVzc306JHt0aGlzLmxjdU1ldGFkYXRhLnBvcnR9YDtcblxuICAgICAgY29uc3QgaXNSZWFkeVJlc3BvbnNlID0gYXdhaXQgZmV0Y2goTENVX1JFQURZX1VSTChsY3VCYXNlVXJsKSwge1xuICAgICAgICBhZ2VudCxcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCYXNpYyAke0J1ZmZlci5mcm9tKGAke3RoaXMubGN1TWV0YWRhdGEudXNlcm5hbWV9OiR7dGhpcy5sY3VNZXRhZGF0YS5wYXNzd29yZH1gKS50b1N0cmluZyhcbiAgICAgICAgICAgICdiYXNlNjQnXG4gICAgICAgICAgKX1gXG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICBpZiAoIWlzUmVhZHlSZXNwb25zZS5vaykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlYWR5IGNhbGwgZmFpbGVkJyk7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHJlYWR5UmVzcG9uc2VUZXh0ID0gYXdhaXQgaXNSZWFkeVJlc3BvbnNlLnRleHQoKTtcbiAgICAgIHJldHVybiByZWFkeVJlc3BvbnNlVGV4dCA9PT0gJ3RydWUnO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgbGN1R2V0RGF0YSgpIHtcbiAgICBjb25zdCBhZ2VudCA9IHRoaXMuc2V0dXBBZ2VudCgpO1xuICAgIGlmICh0aGlzLmxjdVJlYWR5Q2hlY2sgJiYgdGhpcy5sY3VNZXRhZGF0YSkge1xuICAgICAgY29uc3QgbGN1QmFzZVVybCA9IGAke3RoaXMubGN1TWV0YWRhdGEucHJvdG9jb2x9Oi8vJHt0aGlzLmxjdU1ldGFkYXRhLmFkZHJlc3N9OiR7dGhpcy5sY3VNZXRhZGF0YS5wb3J0fWA7XG4gICAgICBjb25zdCBwbGF0Zm9ybVJlc3BvbnNlID0gYXdhaXQgZmV0Y2goTENVX0NMSUVOVF9JTkZPKGxjdUJhc2VVcmwpLCB7XG4gICAgICAgIGFnZW50LFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJhc2ljICR7QnVmZmVyLmZyb20oYCR7dGhpcy5sY3VNZXRhZGF0YS51c2VybmFtZX06JHt0aGlzLmxjdU1ldGFkYXRhLnBhc3N3b3JkfWApLnRvU3RyaW5nKFxuICAgICAgICAgICAgJ2Jhc2U2NCdcbiAgICAgICAgICApfWBcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIGlmIChwbGF0Zm9ybVJlc3BvbnNlLm9rKSB7XG4gICAgICAgIGNvbnN0IGJvZHkgPSBhd2FpdCBwbGF0Zm9ybVJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgdGhpcy5kYXRhQ2FjaGUucmVnaW9uID0gYm9keS5Mb2dpbkRhdGFQYWNrZXQucGxhdGZvcm1JZDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgaXNSZXNwb25zaXZlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIGNvbnN0IGFnZW50ID0gdGhpcy5zZXR1cEFnZW50KCk7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaChBQ1RJVkVQTEFZRVJOQU1FX1VSTCwgeyBhZ2VudCB9KTtcbiAgICByZXR1cm4gcmVzcG9uc2Uub2s7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgZ2V0RGF0YUZyb21Mb0xDbGllbnQoZW5kUG9pbnRVUkw6IHN0cmluZywgc3VtbW9uZXJOYW1lOiBzdHJpbmcgPSAnJykge1xuICAgIGNvbnN0IGFnZW50ID0gdGhpcy5zZXR1cEFnZW50KCk7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaChgJHtlbmRQb2ludFVSTH0ke3N1bW1vbmVyTmFtZX1gLCB7IGFnZW50IH0pO1xuICAgIHJldHVybiBhd2FpdCByZXNwb25zZS5qc29uKCk7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgZ2V0R2FtZUluZm8oKSB7XG4gICAgY29uc3QgZ2FtZURhdGE6IEFsbEdhbWVEYXRhID0gYXdhaXQgdGhpcy5nZXRBbGxHYW1lRGF0YSgpO1xuXG4gICAgaWYgKHRoaXMuZGF0YUNhY2hlLnJlZ2lvbiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICBhd2FpdCB0aGlzLmxjdVJlYWR5Q2hlY2soKTtcbiAgICAgIGF3YWl0IHRoaXMubGN1R2V0RGF0YSgpO1xuICAgIH1cbiAgICB0aGlzLmlzRGF0YUNhY2hlVmFsaWQgPSB0aGlzLmlzUGF5bG9hZEdvb2QoZ2FtZURhdGEpO1xuXG4gICAgaWYgKHRoaXMuaXNEYXRhQ2FjaGVWYWxpZCkge1xuICAgICAgdGhpcy5kYXRhQ2FjaGUuZ2FtZURhdGEgPSBnYW1lRGF0YTtcbiAgICAgIHRoaXMuYXBwZW5kSXRlbUhpc3RvcnkoZ2FtZURhdGEpO1xuICAgICAgdGhpcy5hcHBlbmRBYmlsaXR5SGlzdG9yeShnYW1lRGF0YSk7XG4gICAgICBpZiAoZ2FtZURhdGEuYWxsUGxheWVycykge1xuICAgICAgICB0aGlzLmRhdGFDYWNoZS5hbGxQbGF5ZXJzID0gY3JlYXRlUGxheWVyTWFwKGdhbWVEYXRhLmFsbFBsYXllcnMsIHRoaXMuZGF0YUNhY2hlLmFsbFBsYXllcnMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8vIGFyZ3VlbWVudGxlc3MgY2FsbHNcbiAgcHVibGljIGFzeW5jIGdldEFjdGl2ZVBsYXllcigpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmdldERhdGFGcm9tTG9MQ2xpZW50KEFDVElWRVBMQVlFUl9VUkwpO1xuICB9XG4gIHB1YmxpYyBhc3luYyBnZXRBY3RpdmVQbGF5ZXJBYmlsaXRpZXMoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXRhRnJvbUxvTENsaWVudChBQ1RJVkVQTEFZRVJBQklMSVRJRVNfVVJMKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0QWN0aXZlUGxheWVyTmFtZSgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmdldERhdGFGcm9tTG9MQ2xpZW50KEFDVElWRVBMQVlFUk5BTUVfVVJMKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0QWN0aXZlUGxheWVyUnVuZXMoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXRhRnJvbUxvTENsaWVudChBQ1RJVkVQTEFZRVJSVU5FU19VUkwpO1xuICB9XG4gIHB1YmxpYyBhc3luYyBnZXRBbGxHYW1lRGF0YSgpOiBQcm9taXNlPEFsbEdhbWVEYXRhPiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RGF0YUZyb21Mb0xDbGllbnQoQUxMR0FNRURBVEFfVVJMKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0RXZlbnRzKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RGF0YUZyb21Mb0xDbGllbnQoRVZFTlREQVRBX1VSTCk7XG4gIH1cbiAgcHVibGljIGFzeW5jIGdldEdhbWVTdGF0cygpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmdldERhdGFGcm9tTG9MQ2xpZW50KEdBTUVTVEFUU19VUkwpO1xuICB9XG4gIHB1YmxpYyBhc3luYyBnZXRQbGF5ZXJMaXN0KCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RGF0YUZyb21Mb0xDbGllbnQoUExBWUVSTElTVF9VUkwpO1xuICB9XG5cbiAgLy8gY2FsbHMgdGhhdCB0YWtlIGEgc2luZ2xlIGFyZ3VlbWVudCBvZiBzdW1tb25lck5hbWVcbiAgcHVibGljIGFzeW5jIGdldFBsYXllck1haW5SdW5lcyhzdW1tb25lck5hbWU6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RGF0YUZyb21Mb0xDbGllbnQoUExBWUVSTUFJTlJVTkVTX1VSTCwgc3VtbW9uZXJOYW1lKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0UGxheWVyU3VtbW9uZXJTcGVsbHMoc3VtbW9uZXJOYW1lOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmdldERhdGFGcm9tTG9MQ2xpZW50KFBMQVlFUlNVTU1PTkVSU1BFTExTX1VSTCwgc3VtbW9uZXJOYW1lKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0UGxheWVySXRlbXMoc3VtbW9uZXJOYW1lOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmdldERhdGFGcm9tTG9MQ2xpZW50KFBMQVlFUklURU1TX1VSTCwgc3VtbW9uZXJOYW1lKTtcbiAgfVxuICBwdWJsaWMgYXN5bmMgZ2V0UGxheWVyU2NvcmVzKHN1bW1vbmVyTmFtZTogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXRhRnJvbUxvTENsaWVudChQTEFZRVJTQ09SRVNfVVJMLCBzdW1tb25lck5hbWUpO1xuICB9XG4gIC8vIGVuZCBhcGkgY2FsbHNcblxuICBwdWJsaWMgYXN5bmMgc2V0dXBNRGFhU0Nvbm5lY3Rpb24oaW5pdGlhbFBheWxvYWQ6IG1kYWFzLkRhdGEpIHtcbiAgICB0aGlzLmNvbmZpZyA9IHtcbiAgICAgIHRva2VuOiB0aGlzLmNvbmZpZ09wdGlvbnMuYXBwVG9rZW5JRCxcbiAgICAgIGJyb2FkY2FzdGVySWRzOiBbXSxcbiAgICAgIGdhbWVJZDogTE9MR0FNRUlELFxuICAgICAgZW52aXJvbm1lbnQ6ICdwcm9kJyxcbiAgICAgIG9uVG9rZW5FeHBpcmVkOiB0b2tlbkV4cGlyZWRTdHViLFxuICAgICAgaW5pdGlhbERhdGE6IGluaXRpYWxQYXlsb2FkLFxuICAgICAgdGltZW91dE1zOiA1MDAwLFxuICAgICAgZGVidWdGbjogcHJvY2Vzcy5lbnYuVFdJVENIX0xPTF9TSURFQ0FSX0RFQlVHID09PSAnMSdcbiAgICB9O1xuICB9XG5cbiAgcHVibGljIGFzeW5jIGRpc2Nvbm5lY3QoKSB7XG4gICAgdGhpcy5NRGFhU0Nvbm5lY3Rpb24uZGlzY29ubmVjdCgpO1xuICAgIHRoaXMuYWN0aXZlU2Vzc2lvbklkID0gJyc7XG4gICAgdGhpcy5kYXRhRmxvd1J1bm5pbmcgPSBmYWxzZTtcbiAgICB0aGlzLmNvbm5lY3Rpb25Mb2NrID0gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgc2VuZEFsbEdhbWVEYXRhKCk6IFByb21pc2U8dm9pZD4ge1xuICAgIGNvbnN0IHBheWxvYWQgPSBjcmVhdGVNRGFhU1BheWxvYWQodGhpcy5kYXRhQ2FjaGUpO1xuICAgIGF3YWl0IHRoaXMuTURhYVNDb25uZWN0aW9uLnVwZGF0ZURhdGEocGF5bG9hZCk7XG4gIH1cblxuICBwdWJsaWMgc3RvcCgpIHtcbiAgICBpZiAodGhpcy5sZWFndWVBcGlQb2xsVGltZXIpIHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5sZWFndWVBcGlQb2xsVGltZXIpO1xuICAgICAgdGhpcy5sZWFndWVBcGlQb2xsVGltZXIgPSB1bmRlZmluZWQ7XG4gICAgICB0aGlzLmRpc2Nvbm5lY3QoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc3RhcnQodXBkYXRlU2Vzc2lvbklkPzogRnVuY3Rpb24pIHtcbiAgICBpZiAoIXRoaXMubGVhZ3VlQXBpUG9sbFRpbWVyKSB7XG4gICAgICB0aGlzLmRhdGFDYWNoZSA9IHRoaXMuYnVpbGRDYWNoZU9iamVjdCgpO1xuICAgICAgdGhpcy5zdGFydERhdGFGbG93KHVwZGF0ZVNlc3Npb25JZCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGFzeW5jIHN0YXJ0RGF0YUZsb3codXBkYXRlU2Vzc2lvbklkPzogRnVuY3Rpb24pIHtcbiAgICB0aGlzLmRhdGFGbG93UnVubmluZyA9IHRydWU7XG4gICAgaWYgKCF0aGlzLmNvbmZpZ09wdGlvbnMuYXBwVG9rZW5JRCkge1xuICAgICAgY29uc29sZS5lcnJvcignTm8gdG9rZW4gYXZhaWxhYmxlJyk7XG4gICAgfVxuICAgIGxldCBjb25uZWN0aW9uUmV0cnkgPSAwO1xuICAgIHRoaXMubGVhZ3VlQXBpUG9sbFRpbWVyID0gc2V0SW50ZXJ2YWwoYXN5bmMgKCkgPT4ge1xuICAgICAgdHJ5IHtcbiAgICAgICAgaWYgKHRoaXMuY29ubmVjdGlvbkxvY2sgfHwgIXRoaXMuZGF0YUZsb3dSdW5uaW5nKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGF3YWl0IHRoaXMuZ2V0R2FtZUluZm8oKTtcbiAgICAgICAgaWYgKHRoaXMuaXNEYXRhQ2FjaGVWYWxpZCkge1xuICAgICAgICAgIHRoaXMuY29ubmVjdGlvbkxvY2sgPSB0cnVlO1xuICAgICAgICAgIGNvbnN0IHBheWxvYWQgPSBjcmVhdGVNRGFhU1BheWxvYWQodGhpcy5kYXRhQ2FjaGUpO1xuICAgICAgICAgIGlmICghdGhpcy5hY3RpdmVTZXNzaW9uSWQpIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuc2V0dXBNRGFhU0Nvbm5lY3Rpb24ocGF5bG9hZCk7XG4gICAgICAgICAgICB0aGlzLmFjdGl2ZVNlc3Npb25JZCA9IGF3YWl0IHRoaXMuTURhYVNDb25uZWN0aW9uLmNvbm5lY3QodGhpcy5jb25maWcpO1xuICAgICAgICAgICAgaWYgKHRoaXMuYWN0aXZlU2Vzc2lvbklkICYmIHVwZGF0ZVNlc3Npb25JZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIC8vIGlmIGUyIHNkayBzZXQgYW4gYWN0aXZlIHNlc3Npb24gaWQgbGV0cyBtYXJrIHVzIGFzIGNvbm5lY3Rpb24gZXN0YWJsaXNoZWQgYW5kIGlmIHRoZXJlIGlzIGEgcmVsZXZlbnQgZnVuY3Rpb24gdG8gY2FsbGJhY2sgbGV0cyBkbyBzbyBoZXJlLlxuICAgICAgICAgICAgICB1cGRhdGVTZXNzaW9uSWQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGNvbm5lY3Rpb25SZXRyeSsrO1xuICAgICAgICAgICAgICBpZiAoY29ubmVjdGlvblJldHJ5ID4gMTApIHtcbiAgICAgICAgICAgICAgICB0aGlzLmF0dGVtcHRDbGVhckNvbm5lY3Rpb24oKTtcbiAgICAgICAgICAgICAgICBjb25uZWN0aW9uUmV0cnkgPSAwO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuTURhYVNDb25uZWN0aW9uLnVwZGF0ZURhdGEocGF5bG9hZCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuY29ubmVjdGlvbkxvY2sgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xuICAgICAgfVxuICAgIH0sIHRoaXMuY29uZmlnT3B0aW9ucy5wb2xsaW5nSW50ZXJ2YWwpO1xuICB9XG4gIHByaXZhdGUgYXR0ZW1wdENsZWFyQ29ubmVjdGlvbigpIHtcbiAgICB0aGlzLmRhdGFDYWNoZSA9IHRoaXMuYnVpbGRDYWNoZU9iamVjdCgpO1xuICAgIHRoaXMuTURhYVNDb25uZWN0aW9uLmRpc2Nvbm5lY3QoKTtcbiAgICB0aGlzLmFjdGl2ZVNlc3Npb25JZCA9ICcnO1xuICB9XG5cbiAgcHJpdmF0ZSBpc1BheWxvYWRHb29kKHBheWxvYWQ6IG1kYWFzLkRhdGEgfCB1bmRlZmluZWQpIHtcbiAgICByZXR1cm4gcGF5bG9hZCAmJiBwYXlsb2FkLmFsbFBsYXllcnMgJiYgcGF5bG9hZC5hbGxQbGF5ZXJzLmxlbmd0aCA+IDA7XG4gIH1cblxuICBwcml2YXRlIGFwcGVuZEl0ZW1IaXN0b3J5KGdhbWVEYXRhOiBBbGxHYW1lRGF0YSkge1xuICAgIGlmIChcbiAgICAgIGdhbWVEYXRhICE9PSB1bmRlZmluZWQgJiZcbiAgICAgIGdhbWVEYXRhLmdhbWVEYXRhICE9PSB1bmRlZmluZWQgJiZcbiAgICAgIGdhbWVEYXRhLmFjdGl2ZVBsYXllciAhPT0gdW5kZWZpbmVkICYmXG4gICAgICBnYW1lRGF0YS5hY3RpdmVQbGF5ZXIuc3VtbW9uZXJOYW1lICE9PSB1bmRlZmluZWQgJiZcbiAgICAgIGdhbWVEYXRhLmFsbFBsYXllcnMgIT09IHVuZGVmaW5lZFxuICAgICkge1xuICAgICAgY29uc3QgYWN0aXZlUGxheWVyU3VtbW9uZXJOYW1lID0gZ2FtZURhdGEuYWN0aXZlUGxheWVyLnN1bW1vbmVyTmFtZTtcbiAgICAgIGNvbnN0IGN1cnJlbnRUaW1lID0gZ2FtZURhdGEuZ2FtZURhdGEuZ2FtZVRpbWU7XG4gICAgICBjb25zdCBhY3RpdmVQbGF5ZXJJbmRleDogbnVtYmVyID0gZ2FtZURhdGEuYWxsUGxheWVycy5maW5kSW5kZXgocCA9PiBwLnN1bW1vbmVyTmFtZSA9PT0gYWN0aXZlUGxheWVyU3VtbW9uZXJOYW1lKTtcbiAgICAgIGNvbnN0IG5ld0l0ZW1zID0gZ2FtZURhdGEuYWxsUGxheWVyc1thY3RpdmVQbGF5ZXJJbmRleF0uaXRlbXM7XG4gICAgICBpZiAobmV3SXRlbXMgIT09IHVuZGVmaW5lZCAmJiBjdXJyZW50VGltZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHRoaXMuZGF0YUNhY2hlLml0ZW1IaXN0b3J5ID0gY3JlYXRlSXRlbUhpc3RvcnkoXG4gICAgICAgICAgbmV3SXRlbXMsXG4gICAgICAgICAgdGhpcy5kYXRhQ2FjaGUuY3VycmVudEl0ZW1zLFxuICAgICAgICAgIHRoaXMuZGF0YUNhY2hlLml0ZW1IaXN0b3J5LFxuICAgICAgICAgIGN1cnJlbnRUaW1lXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuZGF0YUNhY2hlLmN1cnJlbnRJdGVtcyA9IG5ld0l0ZW1zO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coYElzc3VlIHN0YXJ0aW5nIGl0ZW0gaGlzdG9yeSBjcmVhdGlvbi4gbmV3SXRlbXM6ICR7bmV3SXRlbXN9IHx8IGN1cnJlbnRUaW1lOiAke2N1cnJlbnRUaW1lfWApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhgSXNzdWUgZmluZGluZyBkYXRhIHRvIGNyZWF0ZSBpdGVtIGhpc3RvcnkuYCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhcHBlbmRBYmlsaXR5SGlzdG9yeShnYW1lRGF0YTogQWxsR2FtZURhdGEpIHtcbiAgICBpZiAoXG4gICAgICBnYW1lRGF0YSAhPT0gdW5kZWZpbmVkICYmXG4gICAgICBnYW1lRGF0YS5nYW1lRGF0YSAhPT0gdW5kZWZpbmVkICYmXG4gICAgICBnYW1lRGF0YS5hY3RpdmVQbGF5ZXIgIT09IHVuZGVmaW5lZCAmJlxuICAgICAgZ2FtZURhdGEuYWN0aXZlUGxheWVyLmFiaWxpdGllc1xuICAgICkge1xuICAgICAgY29uc3QgY3VycmVudFRpbWUgPSBnYW1lRGF0YS5nYW1lRGF0YS5nYW1lVGltZTtcbiAgICAgIGNvbnN0IG5ld0FiaWxpdGllcyA9IGdhbWVEYXRhLmFjdGl2ZVBsYXllci5hYmlsaXRpZXM7XG4gICAgICBpZiAobmV3QWJpbGl0aWVzICE9PSB1bmRlZmluZWQgJiYgY3VycmVudFRpbWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLmRhdGFDYWNoZS5hYmlsaXR5SGlzdG9yeSA9IGNyZWF0ZUFiaWxpdHlIaXN0b3J5KFxuICAgICAgICAgIG5ld0FiaWxpdGllcyxcbiAgICAgICAgICB0aGlzLmRhdGFDYWNoZS5jdXJyZW50QWJpbGl0aWVzLFxuICAgICAgICAgIHRoaXMuZGF0YUNhY2hlLmFiaWxpdHlIaXN0b3J5LFxuICAgICAgICAgIGN1cnJlbnRUaW1lXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuZGF0YUNhY2hlLmN1cnJlbnRBYmlsaXRpZXMgPSBuZXdBYmlsaXRpZXM7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgICBgSXNzdWUgc3RhcnRpbmcgYWJpbGl0eS9za2lsbCBoaXN0b3J5IGNyZWF0aW9uLiBuZXdJdGVtczogJHtuZXdBYmlsaXRpZXN9IHx8IGN1cnJlbnRUaW1lOiAke2N1cnJlbnRUaW1lfWBcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS5sb2coYElzc3VlIGZpbmRpbmcgZGF0YSB0byBjcmVhdGUgYWJpbGl0eS9za2lsbCBoaXN0b3J5LmApO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0dXBBZ2VudCgpIHtcbiAgICByZXR1cm4gbmV3IEFnZW50KHtcbiAgICAgIHJlamVjdFVuYXV0aG9yaXplZDogZmFsc2VcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYnVpbGRDYWNoZU9iamVjdCgpOiBMZWFndWVPZkxlZ2VuZHNEYXRhQ2FjaGUge1xuICAgIGNvbnN0IGNhY2hlID0ge1xuICAgICAgYWJpbGl0eUhpc3Rvcnk6IFtdLFxuICAgICAgaXRlbUhpc3Rvcnk6IFtdLFxuICAgICAgY3VycmVudEFiaWxpdGllczogeyBROiB1bmRlZmluZWQsIFc6IHVuZGVmaW5lZCwgRTogdW5kZWZpbmVkLCBQYXNzaXZlOiB1bmRlZmluZWQsIFI6IHVuZGVmaW5lZCB9LFxuICAgICAgY3VycmVudEl0ZW1zOiBbXSxcbiAgICAgIGFsbFBsYXllcnM6IHt9XG4gICAgfTtcblxuICAgIHJldHVybiBjYWNoZTtcbiAgfVxufVxuIl19