"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createPlayerMap = createPlayerMap;

var _allGameData = require("../../models/league-api/all-game-data");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function createPlayerMap(players, currentMap) {
  const newMap = Object.assign({}, currentMap);
  let chaosIndex = 0;
  let orderIndex = 0;

  for (const player of players) {
    if (!player.summonerName) {
      continue;
    }

    if (player.team === _allGameData.Team.Chaos) {
      const orderedAllPlayer = _objectSpread({}, player, {
        order: chaosIndex
      });

      newMap[player.summonerName] = orderedAllPlayer;
      chaosIndex++;
    } else if (player.team === _allGameData.Team.Order) {
      const orderedAllPlayer = _objectSpread({}, player, {
        order: orderIndex
      });

      newMap[player.summonerName] = orderedAllPlayer;
      orderIndex++;
    }
  }

  return newMap;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9sZWFndWUtb2YtbGVnZW5kcy90cmFuc2Zvcm1zL3BsYXllci1tYXAudHMiXSwibmFtZXMiOlsiY3JlYXRlUGxheWVyTWFwIiwicGxheWVycyIsImN1cnJlbnRNYXAiLCJuZXdNYXAiLCJPYmplY3QiLCJhc3NpZ24iLCJjaGFvc0luZGV4Iiwib3JkZXJJbmRleCIsInBsYXllciIsInN1bW1vbmVyTmFtZSIsInRlYW0iLCJUZWFtIiwiQ2hhb3MiLCJvcmRlcmVkQWxsUGxheWVyIiwib3JkZXIiLCJPcmRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7OztBQUdPLFNBQVNBLGVBQVQsQ0FBeUJDLE9BQXpCLEVBQStDQyxVQUEvQyxFQUFnRztBQUNyRyxRQUFNQyxNQUFNLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JILFVBQWxCLENBQWY7QUFDQSxNQUFJSSxVQUFVLEdBQUcsQ0FBakI7QUFDQSxNQUFJQyxVQUFVLEdBQUcsQ0FBakI7O0FBRUEsT0FBSyxNQUFNQyxNQUFYLElBQXFCUCxPQUFyQixFQUE4QjtBQUM1QixRQUFJLENBQUNPLE1BQU0sQ0FBQ0MsWUFBWixFQUEwQjtBQUN4QjtBQUNEOztBQUVELFFBQUlELE1BQU0sQ0FBQ0UsSUFBUCxLQUFnQkMsa0JBQUtDLEtBQXpCLEVBQWdDO0FBQzlCLFlBQU1DLGdCQUFnQixxQkFDakJMLE1BRGlCO0FBRXBCTSxRQUFBQSxLQUFLLEVBQUVSO0FBRmEsUUFBdEI7O0FBS0FILE1BQUFBLE1BQU0sQ0FBQ0ssTUFBTSxDQUFDQyxZQUFSLENBQU4sR0FBOEJJLGdCQUE5QjtBQUVBUCxNQUFBQSxVQUFVO0FBQ1gsS0FURCxNQVNPLElBQUlFLE1BQU0sQ0FBQ0UsSUFBUCxLQUFnQkMsa0JBQUtJLEtBQXpCLEVBQWdDO0FBQ3JDLFlBQU1GLGdCQUFnQixxQkFDakJMLE1BRGlCO0FBRXBCTSxRQUFBQSxLQUFLLEVBQUVQO0FBRmEsUUFBdEI7O0FBS0FKLE1BQUFBLE1BQU0sQ0FBQ0ssTUFBTSxDQUFDQyxZQUFSLENBQU4sR0FBOEJJLGdCQUE5QjtBQUNBTixNQUFBQSxVQUFVO0FBQ1g7QUFDRjs7QUFDRCxTQUFPSixNQUFQO0FBQ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBbGxQbGF5ZXIsIFRlYW0gfSBmcm9tICcuLi8uLi9tb2RlbHMvbGVhZ3VlLWFwaS9hbGwtZ2FtZS1kYXRhJztcclxuaW1wb3J0IHsgT3JkZXJlZEFsbFBsYXllciB9IGZyb20gJy4uLy4uL21vZGVscy9vcmRlcmVkLXBsYXllcic7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlUGxheWVyTWFwKHBsYXllcnM6IEFsbFBsYXllcltdLCBjdXJyZW50TWFwOiB7IFtrZXk6IHN0cmluZ106IE9yZGVyZWRBbGxQbGF5ZXIgfSkge1xyXG4gIGNvbnN0IG5ld01hcCA9IE9iamVjdC5hc3NpZ24oe30sIGN1cnJlbnRNYXApO1xyXG4gIGxldCBjaGFvc0luZGV4ID0gMDtcclxuICBsZXQgb3JkZXJJbmRleCA9IDA7XHJcblxyXG4gIGZvciAoY29uc3QgcGxheWVyIG9mIHBsYXllcnMpIHtcclxuICAgIGlmICghcGxheWVyLnN1bW1vbmVyTmFtZSkge1xyXG4gICAgICBjb250aW51ZTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAocGxheWVyLnRlYW0gPT09IFRlYW0uQ2hhb3MpIHtcclxuICAgICAgY29uc3Qgb3JkZXJlZEFsbFBsYXllciA9IHtcclxuICAgICAgICAuLi5wbGF5ZXIsXHJcbiAgICAgICAgb3JkZXI6IGNoYW9zSW5kZXhcclxuICAgICAgfTtcclxuXHJcbiAgICAgIG5ld01hcFtwbGF5ZXIuc3VtbW9uZXJOYW1lXSA9IG9yZGVyZWRBbGxQbGF5ZXI7XHJcblxyXG4gICAgICBjaGFvc0luZGV4Kys7XHJcbiAgICB9IGVsc2UgaWYgKHBsYXllci50ZWFtID09PSBUZWFtLk9yZGVyKSB7XHJcbiAgICAgIGNvbnN0IG9yZGVyZWRBbGxQbGF5ZXIgPSB7XHJcbiAgICAgICAgLi4ucGxheWVyLFxyXG4gICAgICAgIG9yZGVyOiBvcmRlckluZGV4XHJcbiAgICAgIH07XHJcblxyXG4gICAgICBuZXdNYXBbcGxheWVyLnN1bW1vbmVyTmFtZV0gPSBvcmRlcmVkQWxsUGxheWVyO1xyXG4gICAgICBvcmRlckluZGV4Kys7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJldHVybiBuZXdNYXA7XHJcbn1cclxuIl19