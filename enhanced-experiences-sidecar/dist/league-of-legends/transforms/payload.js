"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMDaaSPayload = createMDaaSPayload;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function createMDaaSPayload(dataCache) {
  return _objectSpread({}, dataCache.gameData, {
    abilityHistory: dataCache.abilityHistory,
    itemHistory: dataCache.itemHistory,
    region: dataCache.region || 'N/A',
    allPlayers: dataCache.allPlayers
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9sZWFndWUtb2YtbGVnZW5kcy90cmFuc2Zvcm1zL3BheWxvYWQudHMiXSwibmFtZXMiOlsiY3JlYXRlTURhYVNQYXlsb2FkIiwiZGF0YUNhY2hlIiwiZ2FtZURhdGEiLCJhYmlsaXR5SGlzdG9yeSIsIml0ZW1IaXN0b3J5IiwicmVnaW9uIiwiYWxsUGxheWVycyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUdPLFNBQVNBLGtCQUFULENBQTRCQyxTQUE1QixFQUErRTtBQUNwRiwyQkFDS0EsU0FBUyxDQUFDQyxRQURmO0FBRUVDLElBQUFBLGNBQWMsRUFBRUYsU0FBUyxDQUFDRSxjQUY1QjtBQUdFQyxJQUFBQSxXQUFXLEVBQUVILFNBQVMsQ0FBQ0csV0FIekI7QUFJRUMsSUFBQUEsTUFBTSxFQUFFSixTQUFTLENBQUNJLE1BQVYsSUFBb0IsS0FKOUI7QUFLRUMsSUFBQUEsVUFBVSxFQUFFTCxTQUFTLENBQUNLO0FBTHhCO0FBT0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNRGFhU1BheWxvYWQgfSBmcm9tICcuLi8uLi9tb2RlbHMvbWRhYXMtcGF5bG9hZCc7XHJcbmltcG9ydCB7IExlYWd1ZU9mTGVnZW5kc0RhdGFDYWNoZSB9IGZyb20gJy4vLi4vLi4vbW9kZWxzL2xlYWd1ZS1kYXRhLWNhY2hlJztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVNRGFhU1BheWxvYWQoZGF0YUNhY2hlOiBMZWFndWVPZkxlZ2VuZHNEYXRhQ2FjaGUpOiBNRGFhU1BheWxvYWQge1xyXG4gIHJldHVybiB7XHJcbiAgICAuLi5kYXRhQ2FjaGUuZ2FtZURhdGEsXHJcbiAgICBhYmlsaXR5SGlzdG9yeTogZGF0YUNhY2hlLmFiaWxpdHlIaXN0b3J5LFxyXG4gICAgaXRlbUhpc3Rvcnk6IGRhdGFDYWNoZS5pdGVtSGlzdG9yeSxcclxuICAgIHJlZ2lvbjogZGF0YUNhY2hlLnJlZ2lvbiB8fCAnTi9BJyxcclxuICAgIGFsbFBsYXllcnM6IGRhdGFDYWNoZS5hbGxQbGF5ZXJzXHJcbiAgfTtcclxufVxyXG4iXX0=