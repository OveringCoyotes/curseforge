const cp = require('child_process');
const path = require('path');

if (/^win/.test(process.platform)) {
  console.log('Windows detected, skipping repair-permissions.')
} else {
  console.log('Mac detected, repairing permissions...')
  const agentPath = path.join('dist', 'mac', 'Contents', 'MacOS', 'TwitchAgent');

  const proc = cp.exec('chmod +x "' + agentPath + '"', function (error, stdout, stderr) {
    if (error) {
      console.log(`ERROR: ${error.message}`);
      return;
    }
    if (stderr) {
      console.log(`ERROR: ${stderr}`);
      return;
    }
    console.log(stdout);
  });

  proc.on('exit', function (exitCode) {
    if (!exitCode) {
      console.log('TwitchAgent permissions repaired.');
    } else {
      console.log('Unable to repair TwitchAgent permissions. Exit code: ' + exitCode);
    }
  });
}
