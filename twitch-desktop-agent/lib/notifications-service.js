"use strict";
// *** Generated by Curse.Radium.DesktopClientGenerator, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null ***
Object.defineProperty(exports, "__esModule", { value: true });
class NotificationsService {
    constructor(serviceClient) {
        this.serviceClient = serviceClient;
    }
    updateNotifications(request) {
        return this.serviceClient.sendAndReceive('/NotificationsService/UpdateNotifications', { request });
    }
}
exports.NotificationsService = NotificationsService;
// tslint:disable-next-line:max-file-line-count
//# sourceMappingURL=notifications-service.js.map