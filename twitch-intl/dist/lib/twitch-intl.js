"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var IntlMessageFormat = require("intl-messageformat");
var number_short_utils_1 = require("./number-short-utils");
// This bit works around the fact that the locale data files
// just expect to be able to inject into a global object, so
// we have to manually expose the libraries for them.
var globalScope = typeof window !== 'undefined' ? window : global;
// tslint:disable:no-any
globalScope.IntlMessageFormat = IntlMessageFormat;
var TwitchIntl = /** @class */ (function () {
    /**
     * Creates a new intl instance, but does NOT load any polyfills or localized string bundles.
     * Call loadLocale()/loadLocaleSync() after constructing in order to make translations available.
     *
     * @example
     * ```
     * const intl = intl = new TwitchIntl([
     *   {
     *     name: 'English',
     *     languageCode: 'en',
     *     loader: () => Promise.resolve({}),
     *     default: true,
     *   },
     *   {
     *     name: 'Deutsch',
     *     languageCode: 'de',
     *     loader: () => System.import('twilight/assets/messages/de.json'),
     *   },
     * ]);
     * await intl.loadLocale();
     * ```
     *
     * @param allLocales An array of all languages to be made available in the system, the first item will be used as the default fallback.
     * These should be in priority order (eg if en-GB comes before en-US then en-GB will be the default en locale).
     */
    function TwitchIntl(allLocales) {
        var _this = this;
        this.mapping = {};
        this.formatters = {};
        this.messages = {};
        this.uids = {};
        /**
         * Returns the language code of the locale
         * @example return 'en'
         */
        this.getLanguageCode = function () {
            if (!_this.locale) {
                return null;
            }
            return _this.locale.languageCode;
        };
        /**
         * Returns the currently active locale
         * @example return 'en-US'
         */
        this.getActiveLocale = function () {
            if (!_this.locale) {
                return null;
            }
            return _this.locale.locale;
        };
        this.formatDate = function (date, options) {
            if (typeof options === 'string') {
                options = IntlMessageFormat.formats.date[options];
            }
            else if (!options) {
                options = {};
            }
            var cldrLocale = _this.getCldrLocale();
            var key = "[DateFormatter - " + cldrLocale + " - " + _this.stableProps(options) + "]";
            if (!_this.formatters[key]) {
                _this.formatters[key] = new Intl.DateTimeFormat(cldrLocale, options);
            }
            return _this.formatters[key].format(date);
        };
        this.formatTime = function (date, options) {
            if (typeof options === 'string') {
                options = IntlMessageFormat.formats.time[options];
            }
            else if (!options) {
                options = {};
            }
            if (options.hour === undefined) {
                options.hour = 'numeric';
            }
            if (options.minute === undefined) {
                options.minute = 'numeric';
            }
            return _this.formatDate(date, options);
        };
        // Algorithm Overview: https://www.unicode.org/reports/tr35/tr35-numbers.html#Compact_Number_Formats
        this.formatNumberShort = function (numberValue, options) {
            try {
                var localeDataKey = _this.getIntlMessageFormatKey();
                var countValue = globalScope.IntlMessageFormat.__localeData__[localeDataKey].pluralRuleFunction(numberValue);
                // Don't overwrite minFractDigits if already set because then
                // the default of maximumFractionDigits=0 doesn't make sense
                if (!options || !options.minimumFractionDigits) {
                    options = __assign(__assign({}, number_short_utils_1.DEFAULT_OPTIONS), options);
                }
                var cldrLocale = _this.getCldrLocale();
                var localeRules = _this.formatDataByLocale[cldrLocale].numberShort;
                var _a = number_short_utils_1.getTypeAndPattern(numberValue, localeRules, countValue), type = _a.type, pattern = _a.pattern;
                if (pattern === null || pattern === '0') {
                    return _this.formatNumber(numberValue, options);
                }
                var numZeroes = (pattern.match(/0+/g) || [''])[0].length;
                var typeRemovedZeroes = parseInt(number_short_utils_1.removeZeroesFromEnd(type, numZeroes - 1), 10);
                var rawShortNumber = numberValue / typeRemovedZeroes;
                if (options.round) {
                    // There are extra edge cases with maximumSignificantDigits
                    // and other options, but they're not used in twitch so far
                    if (options.maximumFractionDigits) {
                        rawShortNumber *= Math.pow(10, options.maximumFractionDigits);
                    }
                    rawShortNumber = options.round(rawShortNumber);
                    if (options.maximumFractionDigits) {
                        rawShortNumber /= Math.pow(10, options.maximumFractionDigits);
                    }
                }
                var formattedNumber = _this.formatNumber(rawShortNumber, options);
                return pattern.replace(/0+/, formattedNumber);
            }
            catch (error) {
                console.error('[Twitch Intl] Error Formatting Short Number', {
                    number: numberValue,
                    error: error,
                });
                return _this.formatNumber(numberValue, options);
            }
        };
        this.formatNumber = function (numberValue, options) {
            if (typeof options === 'string') {
                options = IntlMessageFormat.formats.number[options];
            }
            else if (!options) {
                options = {};
            }
            var cldrLocale = _this.getCldrLocale();
            var key = "[NumberFormatter - " + cldrLocale + " - " + _this.stableProps(options) + "]";
            if (!_this.formatters[key]) {
                _this.formatters[key] = new Intl.NumberFormat(cldrLocale, options);
            }
            return _this.formatters[key].format(numberValue);
        };
        // Private for these looser type signatures.
        // Is exported publicly with strict types below
        this.internalFormatMessage = function (message, valuesOrKey, namespace) {
            var valuesRichKeys = {};
            if (typeof valuesOrKey === 'string') {
                namespace = valuesOrKey;
            }
            else if (valuesOrKey) {
                valuesRichKeys = valuesOrKey;
            }
            /**
             * Non xtag substitution values
             */
            var values = {};
            // Separating XTagFuncs from RichKeys
            Object.keys(valuesRichKeys).forEach(function (key) {
                values[key] = valuesRichKeys[key];
            });
            var messageKey = namespace ? "[" + namespace + "] " + message : message;
            /**
             * The actual values we will pass to the underlying formatter.
             * JSX instances (non-xtags) will be replaced with unique placeholder tokens.
             */
            var tokenizedValues = {};
            if (Object.keys(values).length) {
                // Creates a token with a random UID that should not be guessable or
                // conflict with other parts of the `message` string.
                if (!_this.uids[messageKey]) {
                    _this.uids[messageKey] = Math.floor(Math.random() * 0x10000000000).toString(16);
                }
                // Loop through all non-xtag substitution values,
                // generating new placeholder tokens for any JSX values
                for (var _i = 0, _a = Object.keys(values); _i < _a.length; _i++) {
                    var valueKey = _a[_i];
                    var value = values[valueKey];
                    tokenizedValues[valueKey] = value;
                }
            }
            var formattedMessage = '';
            // Create a new formatter instance if this is the first time we've formatted this string
            if (!_this.formatters[messageKey]) {
                var translatedMessage = message;
                var cldrLocale = _this.defaultLocale.cldrLocale || _this.defaultLocale.languageCode;
                if (_this.messages && _this.messages[messageKey]) {
                    translatedMessage = _this.messages[messageKey];
                    cldrLocale = _this.getCldrLocale();
                }
                try {
                    _this.formatters[messageKey] = new IntlMessageFormat(translatedMessage, cldrLocale);
                    formattedMessage = _this.formatters[messageKey].format(tokenizedValues || values);
                }
                catch (e) {
                    // fall back to english in the case of formatting errors
                    var defaultCldrLocale = _this.defaultLocale.cldrLocale || _this.defaultLocale.languageCode;
                    _this.formatters[messageKey] = new IntlMessageFormat(message, defaultCldrLocale);
                    console.error('[Twitch Intl] Formatting failed', {
                        messageKey: messageKey,
                        locale: cldrLocale,
                    });
                }
            }
            // Use a cached formatter if previous instantiation block failed to give us a formatted message
            if (!formattedMessage) {
                formattedMessage = _this.formatters[messageKey].format(tokenizedValues || values);
            }
            return formattedMessage;
        };
        // Strict type signatures applied for public methods here:
        // tslint:disable-next-line:member-ordering no-any
        this.formatMessage = this.internalFormatMessage;
        this.allLocales = allLocales;
        this.defaultLocale = this.determineDefaultLocale(allLocales);
        this.buildMapping();
        this.formatDataByLocale = {};
    }
    /**
     * Calling this will cause an Intl polyfill to be loaded, if needed,
     * and then localization strings will be loaded for the current locale.
     * @return The locale data for the resolved locale
     */
    TwitchIntl.prototype.loadLocale = function (preferredLanguageCodes) {
        return __awaiter(this, void 0, void 0, function () {
            var localeData, locale, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setLocale(preferredLanguageCodes);
                        locale = this.locale;
                        if (!('loader' in locale)) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, locale.loader()];
                    case 2:
                        localeData = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.error('[Twitch Intl] Error loading locale data', {
                            locale: locale.name,
                            error: error_1,
                        });
                        return [2 /*return*/];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        localeData = locale.data;
                        _a.label = 6;
                    case 6:
                        this.loadLocaleData(localeData);
                        return [2 /*return*/, localeData];
                }
            });
        });
    };
    /**
     * This is the same as loadLocale but works only with synchronous locale loaders.
     * @throws If locale resolution leads to this attempting to use an async loader function
     */
    TwitchIntl.prototype.loadLocaleSync = function (preferredLanguageCodes) {
        this.setLocale(preferredLanguageCodes);
        var locale = this.locale;
        if ('loader' in locale) {
            throw new Error("[Twitch Intl] Attempted to sync load locale " + locale.name + " with an async loader");
        }
        this.loadLocaleData(locale.data);
        return locale.data;
    };
    TwitchIntl.prototype.resolveLocaleFromCode = function (languageCode) {
        if (!languageCode) {
            return null;
        }
        var codeParts = languageCode.toLowerCase().split('-');
        var idx = codeParts.length;
        while (idx > 0) {
            // try matching with decreasing specifity on language scopes
            var fuzzyLanguageCode = codeParts.slice(0, idx).join('-');
            if (this.mapping[fuzzyLanguageCode]) {
                return this.mapping[fuzzyLanguageCode];
            }
            idx--;
        }
        return null;
    };
    // tslint:disable-next-line:no-any
    TwitchIntl.prototype.stableProps = function (options) {
        var sortedProps = Object.keys(options)
            .sort()
            .map(function (elem) {
            var _a;
            return (_a = {}, _a[elem] = options[elem], _a);
        });
        return JSON.stringify(sortedProps);
    };
    TwitchIntl.prototype.buildMapping = function () {
        var _this = this;
        this.mapping = {};
        var _loop_1 = function (locale) {
            var addMappingEntry = function (_, idx, codeParts) {
                // build mappings with increasing specificity for each scope addition
                var code = codeParts.slice(0, idx + 1).join('-');
                if (!_this.mapping[code]) {
                    _this.mapping[code] = locale;
                }
            };
            locale.languageCode
                .toLowerCase()
                .split('-')
                .forEach(addMappingEntry);
            if (locale.cldrLocale) {
                locale.cldrLocale
                    .toLowerCase()
                    .split('-')
                    .forEach(addMappingEntry);
            }
        };
        for (var _i = 0, _a = this.allLocales; _i < _a.length; _i++) {
            var locale = _a[_i];
            _loop_1(locale);
        }
    };
    // Return first explicitly flagged default locale, otherwise return first in array.
    TwitchIntl.prototype.determineDefaultLocale = function (allLocales) {
        for (var _i = 0, allLocales_1 = allLocales; _i < allLocales_1.length; _i++) {
            var locale = allLocales_1[_i];
            if (locale.default) {
                return locale;
            }
        }
        return allLocales[0];
    };
    TwitchIntl.prototype.getCldrLocale = function () {
        var locale = this.locale || this.defaultLocale;
        return locale.cldrLocale || locale.languageCode;
    };
    TwitchIntl.prototype.getIntlMessageFormatKey = function () {
        var locale = this.locale || this.defaultLocale;
        return locale.intlMessageFormatKey || locale.languageCode;
    };
    // sets locale to first match from preferredLanguageCodes, otherwise uses default
    TwitchIntl.prototype.setLocale = function (preferredLanguageCodes) {
        for (var _i = 0, preferredLanguageCodes_1 = preferredLanguageCodes; _i < preferredLanguageCodes_1.length; _i++) {
            var langCode = preferredLanguageCodes_1[_i];
            var locale = this.resolveLocaleFromCode(langCode);
            if (locale) {
                this.locale = locale;
                return;
            }
        }
        this.locale = this.defaultLocale;
    };
    // loads formatData and messages and resets formatters
    TwitchIntl.prototype.loadLocaleData = function (localeData) {
        if (localeData.formatData) {
            this.formatDataByLocale[this.getCldrLocale()] = localeData.formatData;
        }
        if (localeData.messages) {
            this.messages = localeData.messages;
        }
        this.formatters = {};
    };
    return TwitchIntl;
}());
exports.TwitchIntl = TwitchIntl;
