"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_OPTIONS = {
    maximumFractionDigits: 0,
};
function removeZeroesFromEnd(type, numZeroes) {
    return type.slice(0, type.length - numZeroes);
}
exports.removeZeroesFromEnd = removeZeroesFromEnd;
function getTypeAndPattern(numberValue, localeRules, countValue) {
    if (localeRules === void 0) { localeRules = {}; }
    var numLength = Math.floor(numberValue).toString().length;
    var type = Math.pow(10, numLength - 1).toString();
    return {
        type: type,
        pattern: localeRules[type + "-count-" + countValue] || '0',
    };
}
exports.getTypeAndPattern = getTypeAndPattern;
