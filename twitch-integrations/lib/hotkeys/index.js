"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** key type for pressed events (single event) or momentary for (up/down events) */
var HotkeyType;
(function (HotkeyType) {
    HotkeyType["Switch"] = "switch";
    HotkeyType["Momentary"] = "momentary";
})(HotkeyType || (HotkeyType = {}));
exports.HotkeysIntegrationHotkeyType = HotkeyType;
/** KeyStates, pressed for single event, up/down for key down/up events */
var KeyState;
(function (KeyState) {
    KeyState["Pressed"] = "pressed";
    KeyState["Down"] = "down";
    KeyState["Up"] = "up";
})(KeyState || (KeyState = {}));
exports.HotkeysIntegrationKeyState = KeyState;
/** Mouse 'key' enums */
var MouseKey;
(function (MouseKey) {
    MouseKey["Button1"] = "mb1";
    MouseKey["Button2"] = "mb2";
    MouseKey["Button3"] = "mb3";
    MouseKey["Button4"] = "mb4";
    MouseKey["Button5"] = "mb5";
    MouseKey["ScrollUp"] = "mwup";
    MouseKey["ScrollDown"] = "mwdown";
    MouseKey["TiltLeft"] = "mwleft";
    MouseKey["TiltRight"] = "mwright";
})(MouseKey || (MouseKey = {}));
exports.HotkeysIntegrationMouseKeys = MouseKey;
//# sourceMappingURL=index.js.map