"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Special menu types.
 * @property Separator A horizontal line to split groups of menu items
 * @property A toggleable checkbox menu item
 * @property A switchable menu item
 */
var MenuIntegrationItemType;
(function (MenuIntegrationItemType) {
    MenuIntegrationItemType["Separator"] = "separator";
    MenuIntegrationItemType["Checkbox"] = "checkbox";
    MenuIntegrationItemType["Radio"] = "radio";
})(MenuIntegrationItemType = exports.MenuIntegrationItemType || (exports.MenuIntegrationItemType = {}));
//# sourceMappingURL=item.js.map