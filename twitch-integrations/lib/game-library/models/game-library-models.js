"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** General error types raised from GameLibrary */
var GameLibraryIntegrationGeneralErrorType;
(function (GameLibraryIntegrationGeneralErrorType) {
    GameLibraryIntegrationGeneralErrorType["NetworkFailure"] = "Network Failure";
    GameLibraryIntegrationGeneralErrorType["SyncFailure"] = "Sync Failure";
    GameLibraryIntegrationGeneralErrorType["UnknownError"] = "Uknown Error";
    GameLibraryIntegrationGeneralErrorType["AuthError"] = "Auth Error";
    GameLibraryIntegrationGeneralErrorType["DbCorruptionDetectedAndWiped"] = "Db Corruption Detected and Wiped";
    GameLibraryIntegrationGeneralErrorType["DbLockFailedToObtain"] = "Db Lock Failed To Obtain";
    GameLibraryIntegrationGeneralErrorType["DbCorruptionDetectedAndUnableToWipe"] = "Db Corruption Detected and Unable to Wipe";
})(GameLibraryIntegrationGeneralErrorType = exports.GameLibraryIntegrationGeneralErrorType || (exports.GameLibraryIntegrationGeneralErrorType = {}));
/** Installation Error types raised from GameLibrary */
var GameLibraryIntegrationInstallErrorType;
(function (GameLibraryIntegrationInstallErrorType) {
    GameLibraryIntegrationInstallErrorType["None"] = "None";
    GameLibraryIntegrationInstallErrorType["Cancelled"] = "Cancelled";
    GameLibraryIntegrationInstallErrorType["UacDenial"] = "UAC Denial";
    GameLibraryIntegrationInstallErrorType["GenericUacHelperAppFailure"] = "Generic UAC Helper App Failure";
    GameLibraryIntegrationInstallErrorType["UnknownElevationFailure"] = "Uknown Elevation Failure";
    GameLibraryIntegrationInstallErrorType["GenericFailure"] = "Generic Failure";
    GameLibraryIntegrationInstallErrorType["GenericInstallOrUpdateProductException"] = "Generic Install Or Update Product Exception";
    GameLibraryIntegrationInstallErrorType["GenericInstallOrUpdateProductWithSleepProtectException"] = "Generic Install Or Update Product With Sleep Protect Exception";
    GameLibraryIntegrationInstallErrorType["GenericInstallOrUpdateGameCoreException"] = "Generic Install Or Update Game Core Exception";
    GameLibraryIntegrationInstallErrorType["AuthFailure"] = "Auth Failure";
    GameLibraryIntegrationInstallErrorType["ConfigFileMissing"] = "Config File Missing";
    GameLibraryIntegrationInstallErrorType["Busy"] = "Busy";
    GameLibraryIntegrationInstallErrorType["NotEntitled"] = "Not Entitled";
    GameLibraryIntegrationInstallErrorType["VersionManifestDownloadFailed"] = "Version Manifest Download Failed";
    GameLibraryIntegrationInstallErrorType["DownloadFailure"] = "Download Failure";
    GameLibraryIntegrationInstallErrorType["PostInstallFailure"] = "Post Install Failure";
    GameLibraryIntegrationInstallErrorType["NotEnoughDiskSpace"] = "Not Enough Disk Space";
    GameLibraryIntegrationInstallErrorType["UserIsNotAdmin"] = "User Is Not Admin";
    GameLibraryIntegrationInstallErrorType["NetworkFailure"] = "Network Failure";
    GameLibraryIntegrationInstallErrorType["CreateDirFailure"] = "Create Dir Failure";
    GameLibraryIntegrationInstallErrorType["InstallInfoNotFoundInDb"] = "Install Info Not Found In DB";
    GameLibraryIntegrationInstallErrorType["AlreadyRunning"] = "Already Running";
    GameLibraryIntegrationInstallErrorType["LockedFiles"] = "Locked Files";
})(GameLibraryIntegrationInstallErrorType = exports.GameLibraryIntegrationInstallErrorType || (exports.GameLibraryIntegrationInstallErrorType = {}));
/** Uninstall error types raised from GameLibrary */
var GameLibraryIntegrationUninstallErrorType;
(function (GameLibraryIntegrationUninstallErrorType) {
    GameLibraryIntegrationUninstallErrorType["NotInstalled"] = "Not Installed";
    GameLibraryIntegrationUninstallErrorType["Unsuccessful"] = "Unsuccessful";
    GameLibraryIntegrationUninstallErrorType["UnknownExceptionEncountered"] = "Uknown Exception Encountered";
    GameLibraryIntegrationUninstallErrorType["UacDenial"] = "UAC Denial";
})(GameLibraryIntegrationUninstallErrorType = exports.GameLibraryIntegrationUninstallErrorType || (exports.GameLibraryIntegrationUninstallErrorType = {}));
/** Install state for game */
var GameLibraryIntegrationProductInstallState;
(function (GameLibraryIntegrationProductInstallState) {
    GameLibraryIntegrationProductInstallState["NotInstalled"] = "Not Installed";
    GameLibraryIntegrationProductInstallState["Installed"] = "Installed";
    GameLibraryIntegrationProductInstallState["InstalledAndNeedsUpdate"] = "Installed And Needs Update";
    GameLibraryIntegrationProductInstallState["Running"] = "Running";
})(GameLibraryIntegrationProductInstallState = exports.GameLibraryIntegrationProductInstallState || (exports.GameLibraryIntegrationProductInstallState = {}));
/** Running state of game */
var GameLibraryIntegrationProductRunningState;
(function (GameLibraryIntegrationProductRunningState) {
    GameLibraryIntegrationProductRunningState["NotRunning"] = "Not Running";
    GameLibraryIntegrationProductRunningState["Running"] = "Running";
})(GameLibraryIntegrationProductRunningState = exports.GameLibraryIntegrationProductRunningState || (exports.GameLibraryIntegrationProductRunningState = {}));
/** Available action for game */
var GameLibraryIntegrationInvokeType;
(function (GameLibraryIntegrationInvokeType) {
    GameLibraryIntegrationInvokeType["None"] = "None";
    GameLibraryIntegrationInvokeType["Install"] = "Install";
    GameLibraryIntegrationInvokeType["Update"] = "Update";
    GameLibraryIntegrationInvokeType["Launch"] = "Launch";
})(GameLibraryIntegrationInvokeType = exports.GameLibraryIntegrationInvokeType || (exports.GameLibraryIntegrationInvokeType = {}));
/** Library refresh result */
var GameLibraryIntegrationRefreshResult;
(function (GameLibraryIntegrationRefreshResult) {
    GameLibraryIntegrationRefreshResult["Success"] = "Success";
    GameLibraryIntegrationRefreshResult["Skipped_Throttled"] = "Skipped Throttled";
    GameLibraryIntegrationRefreshResult["Skipped_Disabled"] = "Skipped Disabled";
    GameLibraryIntegrationRefreshResult["Failed_LoginFailure"] = "Failed Login Failure";
    GameLibraryIntegrationRefreshResult["Failed_NotInitialized"] = "Failed Not Initialized";
    GameLibraryIntegrationRefreshResult["Failed"] = "Failed";
})(GameLibraryIntegrationRefreshResult = exports.GameLibraryIntegrationRefreshResult || (exports.GameLibraryIntegrationRefreshResult = {}));
/** Installation Completion Reason */
var GameLibraryIntegrationInstallUpdateCompletionReason;
(function (GameLibraryIntegrationInstallUpdateCompletionReason) {
    GameLibraryIntegrationInstallUpdateCompletionReason["Successful"] = "Successful";
    GameLibraryIntegrationInstallUpdateCompletionReason["Error"] = "Error";
    GameLibraryIntegrationInstallUpdateCompletionReason["Cancelled"] = "Cancelled";
})(GameLibraryIntegrationInstallUpdateCompletionReason = exports.GameLibraryIntegrationInstallUpdateCompletionReason || (exports.GameLibraryIntegrationInstallUpdateCompletionReason = {}));
/** Installation Request result, different from the above because this is only for the initial request */
var GameLibraryIntegrationInstallOrUpdateResult;
(function (GameLibraryIntegrationInstallOrUpdateResult) {
    GameLibraryIntegrationInstallOrUpdateResult["Success"] = "Success";
    GameLibraryIntegrationInstallOrUpdateResult["InvalidProductId"] = "Invalid ProductId";
})(GameLibraryIntegrationInstallOrUpdateResult = exports.GameLibraryIntegrationInstallOrUpdateResult || (exports.GameLibraryIntegrationInstallOrUpdateResult = {}));
/** Status of the library, whether or not GameLibrary is doing some work in the background */
var GameLibraryIntegrationState;
(function (GameLibraryIntegrationState) {
    GameLibraryIntegrationState["Init"] = "Init";
    GameLibraryIntegrationState["Idle"] = "Idle";
    GameLibraryIntegrationState["Loading"] = "Loading";
    GameLibraryIntegrationState["Disabled"] = "Disabled";
})(GameLibraryIntegrationState = exports.GameLibraryIntegrationState || (exports.GameLibraryIntegrationState = {}));
/** Uninstall state */
var GameLibraryIntegrationProductUninstallEvent;
(function (GameLibraryIntegrationProductUninstallEvent) {
    GameLibraryIntegrationProductUninstallEvent["Uninstalling"] = "Uninstalling";
    GameLibraryIntegrationProductUninstallEvent["UninstallComplete"] = "Uninstall Complete";
})(GameLibraryIntegrationProductUninstallEvent = exports.GameLibraryIntegrationProductUninstallEvent || (exports.GameLibraryIntegrationProductUninstallEvent = {}));
/** Cancelation Result */
var GameLibraryIntegrationCancelInstallResult;
(function (GameLibraryIntegrationCancelInstallResult) {
    GameLibraryIntegrationCancelInstallResult["Success"] = "Success";
    GameLibraryIntegrationCancelInstallResult["Fail"] = "Fail";
})(GameLibraryIntegrationCancelInstallResult = exports.GameLibraryIntegrationCancelInstallResult || (exports.GameLibraryIntegrationCancelInstallResult = {}));
/** A result indicating whether the launch of a game was successful or not */
var GameLibraryIntegrationLaunchProductResult;
(function (GameLibraryIntegrationLaunchProductResult) {
    GameLibraryIntegrationLaunchProductResult["Succeeded"] = "Succeeded";
    GameLibraryIntegrationLaunchProductResult["Cancelled"] = "Cancelled";
    GameLibraryIntegrationLaunchProductResult["Failed"] = "Failed";
    GameLibraryIntegrationLaunchProductResult["InstallationOrUpdateInProgress"] = "InstallationOrUpdateInProgress";
    GameLibraryIntegrationLaunchProductResult["NeedsUpdate"] = "NeedsUpdate";
    GameLibraryIntegrationLaunchProductResult["NotEntitled"] = "NotEntitled";
    GameLibraryIntegrationLaunchProductResult["NotInstalled"] = "NotInstalled";
    GameLibraryIntegrationLaunchProductResult["NotLoggedIn"] = "NotLoggedIn";
    GameLibraryIntegrationLaunchProductResult["VersionManifestDownloadFailed"] = "VersionManifestDownloadFailed";
    GameLibraryIntegrationLaunchProductResult["ConfigFileMissing"] = "ConfigFileMissing";
    GameLibraryIntegrationLaunchProductResult["InvalidConfigFile"] = "InvalidConfigFile";
    GameLibraryIntegrationLaunchProductResult["ProcessLaunchFailed"] = "ProcessLaunchFailed";
    GameLibraryIntegrationLaunchProductResult["ProcessLaunchFailedAnotherInProgress"] = "ProcessLaunchFailedAnotherInProgress";
    GameLibraryIntegrationLaunchProductResult["ProcessLaunchFailedAlreadyRunning"] = "ProcessLaunchFailedAlreadyRunning";
    GameLibraryIntegrationLaunchProductResult["ProcessLaunchFailedWrongArchitecture"] = "ProcessLaunchFailedWrongArchitecture";
    GameLibraryIntegrationLaunchProductResult["ProcessLaunchFailedTokenExchangeFailure"] = "ProcessLaunchFailedTokenExchangeFailure";
    GameLibraryIntegrationLaunchProductResult["NetworkFailure"] = "NetworkFailure";
    GameLibraryIntegrationLaunchProductResult["FileNotFound"] = "FileNotFound";
})(GameLibraryIntegrationLaunchProductResult = exports.GameLibraryIntegrationLaunchProductResult || (exports.GameLibraryIntegrationLaunchProductResult = {}));
/** A result indicating whether the uninstall of a game was successful or not */
var GameLibraryIntegrationUninstallProductResult;
(function (GameLibraryIntegrationUninstallProductResult) {
    GameLibraryIntegrationUninstallProductResult["Success"] = "Success";
    GameLibraryIntegrationUninstallProductResult["Failed"] = "Failed";
})(GameLibraryIntegrationUninstallProductResult = exports.GameLibraryIntegrationUninstallProductResult || (exports.GameLibraryIntegrationUninstallProductResult = {}));
/** A result indicating whether the uninstall of a game was successful or not */
var GameLibraryIntegrationCreateShortcutResult;
(function (GameLibraryIntegrationCreateShortcutResult) {
    GameLibraryIntegrationCreateShortcutResult["Success"] = "Success";
    GameLibraryIntegrationCreateShortcutResult["Failed"] = "Failed";
})(GameLibraryIntegrationCreateShortcutResult = exports.GameLibraryIntegrationCreateShortcutResult || (exports.GameLibraryIntegrationCreateShortcutResult = {}));
/** .NET 4.5 Installation Request result, this is only for the initial request */
var GameLibraryIntegrationInstallDotNet45RequestResult;
(function (GameLibraryIntegrationInstallDotNet45RequestResult) {
    GameLibraryIntegrationInstallDotNet45RequestResult["Success"] = "Success";
    GameLibraryIntegrationInstallDotNet45RequestResult["Failed"] = "Failed";
    GameLibraryIntegrationInstallDotNet45RequestResult["AlreadyInstalled"] = "AlreadyInstalled";
})(GameLibraryIntegrationInstallDotNet45RequestResult = exports.GameLibraryIntegrationInstallDotNet45RequestResult || (exports.GameLibraryIntegrationInstallDotNet45RequestResult = {}));
/** This is the result of the .NET 4.5 install */
var GameLibraryIntegrationInstallDotNet45CompletedResult;
(function (GameLibraryIntegrationInstallDotNet45CompletedResult) {
    GameLibraryIntegrationInstallDotNet45CompletedResult["Failed"] = "Failed";
    GameLibraryIntegrationInstallDotNet45CompletedResult["Success"] = "Success";
    GameLibraryIntegrationInstallDotNet45CompletedResult["SuccessRestartRequested"] = "SuccessRestartRequested";
    GameLibraryIntegrationInstallDotNet45CompletedResult["FailedDownload"] = "FailedDownload";
    GameLibraryIntegrationInstallDotNet45CompletedResult["FailedInstallExecute"] = "FailedInstallExecute";
    GameLibraryIntegrationInstallDotNet45CompletedResult["FailedInstallTimeout"] = "FailedInstallTimeout";
    GameLibraryIntegrationInstallDotNet45CompletedResult["FailedInstallGettingExitCode"] = "FailedInstallGettingExitCode";
    GameLibraryIntegrationInstallDotNet45CompletedResult["FailedInstallExitCode"] = "FailedInstallExitCode";
    GameLibraryIntegrationInstallDotNet45CompletedResult["Cancelled"] = "Cancelled";
})(GameLibraryIntegrationInstallDotNet45CompletedResult = exports.GameLibraryIntegrationInstallDotNet45CompletedResult || (exports.GameLibraryIntegrationInstallDotNet45CompletedResult = {}));
//# sourceMappingURL=game-library-models.js.map