"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AudioPlaybackRepeatMode;
(function (AudioPlaybackRepeatMode) {
    AudioPlaybackRepeatMode["Playlist"] = "playlist";
    AudioPlaybackRepeatMode["Track"] = "track";
    AudioPlaybackRepeatMode["Off"] = "off";
})(AudioPlaybackRepeatMode = exports.AudioPlaybackRepeatMode || (exports.AudioPlaybackRepeatMode = {}));
var AudioPlaybackContainerType;
(function (AudioPlaybackContainerType) {
    AudioPlaybackContainerType["Playlist"] = "playlist";
    AudioPlaybackContainerType["Station"] = "station";
})(AudioPlaybackContainerType = exports.AudioPlaybackContainerType || (exports.AudioPlaybackContainerType = {}));
//# sourceMappingURL=index.js.map