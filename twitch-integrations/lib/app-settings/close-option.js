"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppSettingsIntegrationCloseOption;
(function (AppSettingsIntegrationCloseOption) {
    AppSettingsIntegrationCloseOption["Minimize"] = "minimize";
    AppSettingsIntegrationCloseOption["Hide"] = "hide";
    AppSettingsIntegrationCloseOption["Close"] = "close";
})(AppSettingsIntegrationCloseOption = exports.AppSettingsIntegrationCloseOption || (exports.AppSettingsIntegrationCloseOption = {}));
//# sourceMappingURL=close-option.js.map