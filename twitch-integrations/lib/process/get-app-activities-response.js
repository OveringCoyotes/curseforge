"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProcessIntegrationAppActivityName;
(function (ProcessIntegrationAppActivityName) {
    ProcessIntegrationAppActivityName["VideoPlayer"] = "videoPlayer";
    ProcessIntegrationAppActivityName["GameInstall"] = "gameInstall";
    ProcessIntegrationAppActivityName["AudioPlayback"] = "audioPlayback";
})(ProcessIntegrationAppActivityName = exports.ProcessIntegrationAppActivityName || (exports.ProcessIntegrationAppActivityName = {}));
//# sourceMappingURL=get-app-activities-response.js.map