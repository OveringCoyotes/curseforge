"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProcessIntegrationConnectionStatus;
(function (ProcessIntegrationConnectionStatus) {
    ProcessIntegrationConnectionStatus["Connected"] = "connected";
    ProcessIntegrationConnectionStatus["Connecting"] = "connecting";
    ProcessIntegrationConnectionStatus["Disconnected"] = "disconnected";
})(ProcessIntegrationConnectionStatus = exports.ProcessIntegrationConnectionStatus || (exports.ProcessIntegrationConnectionStatus = {}));
//# sourceMappingURL=state-changed-message.js.map