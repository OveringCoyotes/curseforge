"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** The status of the FastLoginResponse. */
var FastLoginStatusCode;
(function (FastLoginStatusCode) {
    FastLoginStatusCode[FastLoginStatusCode["Success"] = 1] = "Success";
    FastLoginStatusCode[FastLoginStatusCode["Error"] = 2] = "Error";
})(FastLoginStatusCode = exports.FastLoginStatusCode || (exports.FastLoginStatusCode = {}));
//# sourceMappingURL=fast-login-response.js.map