"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Name of the localStorage item for storing the mods token. This token is expected
 * to be created and set through localStorage in Electron, and then consumed in Twilight
 * for API calls to Mods services.
 */
exports.SessionIntegrationStoredModsTokenName = 'desktop-mods-token';
/**
 * Types of auth tokens that apply in SessionIntegration requests.
 */
var SessionIntegrationTokenType;
(function (SessionIntegrationTokenType) {
    SessionIntegrationTokenType["Mods"] = "mods";
    SessionIntegrationTokenType["MyGames"] = "games";
})(SessionIntegrationTokenType = exports.SessionIntegrationTokenType || (exports.SessionIntegrationTokenType = {}));
/**
 * Types of results that can occur from a token validation
 * request.
 */
var SessionIntegrationTokenValidationResult;
(function (SessionIntegrationTokenValidationResult) {
    /**
     * A token exists and is valid.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["Valid"] = 1] = "Valid";
    /**
     * A token is either invalid or does not currently exist.
     * Only returned if refreshIfInvalid is false.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["Invalid"] = 2] = "Invalid";
    /**
     * A token was absent or invalid and was refreshed successfully.
     * Only done if refreshIfInvalid was set to true.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["Refreshed"] = 3] = "Refreshed";
    /**
     * A refresh was attempted due to refreshIfInvalid being true, but
     * the refresh was unsuccessful due to an unknown error.
     * An error state can be shown here. A suggestion to log in could
     * fix the problem, but that is not guaranteed.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["ErrorWithRefresh"] = 4] = "ErrorWithRefresh";
    /**
     * A refresh was attempted due to refreshIfInvalid being true, but
     * the refresh is not possible unless the user performs a full login.
     * The UI should recommend that the user log back in, which should
     * automatically refresh the tokens successfully. The token cannot
     * be refreshed without such login, due to some missing credential
     * such as a persistent cookie.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["NeedsLogin"] = 5] = "NeedsLogin";
    /**
     * An unknown or unhandled error has occurred at some point. This
     * is not a known error state and can probably be displayed as a
     * generic error. A login or restart may resolve it, but this is
     * not guaranteed.
     */
    SessionIntegrationTokenValidationResult[SessionIntegrationTokenValidationResult["Error"] = 6] = "Error";
})(SessionIntegrationTokenValidationResult = exports.SessionIntegrationTokenValidationResult || (exports.SessionIntegrationTokenValidationResult = {}));
/**
 * Types of results that can occur from a token renewal
 * request.
 */
var SessionIntegrationTokenRefreshResult;
(function (SessionIntegrationTokenRefreshResult) {
    /**
     * A refresh was requested and was successful. The newly
     * refreshed token should be valid now.
     */
    SessionIntegrationTokenRefreshResult[SessionIntegrationTokenRefreshResult["Success"] = 1] = "Success";
    /**
     * A refresh was requested but a valid token already existed,
     * so no change is needed.
     */
    SessionIntegrationTokenRefreshResult[SessionIntegrationTokenRefreshResult["NoChangeNeeded"] = 2] = "NoChangeNeeded";
    /**
     * A refresh was attempted, but the refresh is not possible
     * unless the user performs a full login. The UI
     * should recommend that the user log back in, which should
     * automatically refresh the tokens successfully. The token cannot
     * be refreshed without such login, due to some missing credential
     * such as a persistent cookie.
     */
    SessionIntegrationTokenRefreshResult[SessionIntegrationTokenRefreshResult["NeedsLogin"] = 3] = "NeedsLogin";
    /**
     * An unknown or unhandled error has occurred at some point. This
     * is not a known error state and can probably be displayed as a
     * generic error. A login or restart may resolve it, but this is
     * not guaranteed.
     */
    SessionIntegrationTokenRefreshResult[SessionIntegrationTokenRefreshResult["Error"] = 4] = "Error";
})(SessionIntegrationTokenRefreshResult = exports.SessionIntegrationTokenRefreshResult || (exports.SessionIntegrationTokenRefreshResult = {}));
//# sourceMappingURL=stored-tokens.js.map