"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EditMode;
(function (EditMode) {
    EditMode["Preview"] = "preview";
    EditMode["Edit"] = "edit";
})(EditMode = exports.EditMode || (exports.EditMode = {}));
//# sourceMappingURL=editor.js.map