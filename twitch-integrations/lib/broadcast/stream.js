"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VideoEncoderType;
(function (VideoEncoderType) {
    VideoEncoderType["Nvenc"] = "nvenc";
    VideoEncoderType["X264"] = "x264";
    VideoEncoderType["Quicksync"] = "quicksync";
    VideoEncoderType["VideoToolbox"] = "videotoolbox";
})(VideoEncoderType = exports.VideoEncoderType || (exports.VideoEncoderType = {}));
var X264CpuPreset;
(function (X264CpuPreset) {
    X264CpuPreset["UltraFast"] = "ultrafast";
    X264CpuPreset["SuperFast"] = "superfast";
    X264CpuPreset["VeryFast"] = "veryfast";
    X264CpuPreset["Faster"] = "faster";
    X264CpuPreset["Fast"] = "fast";
    X264CpuPreset["Medium"] = "medium";
    X264CpuPreset["Slow"] = "slow";
    X264CpuPreset["Slower"] = "slower";
    X264CpuPreset["VerySlow"] = "veryslow";
})(X264CpuPreset = exports.X264CpuPreset || (exports.X264CpuPreset = {}));
var X264Tuning;
(function (X264Tuning) {
    X264Tuning["None"] = "none";
    X264Tuning["Film"] = "film";
    X264Tuning["Animation"] = "animation";
    X264Tuning["Grain"] = "grain";
    X264Tuning["StillImage"] = "stillimage";
    X264Tuning["Psnr"] = "psnr";
    X264Tuning["Ssim"] = "ssim";
    X264Tuning["FastDecode"] = "fastdecode";
    X264Tuning["ZeroLatency"] = "zerolatency";
})(X264Tuning = exports.X264Tuning || (exports.X264Tuning = {}));
var QuicksyncPreset;
(function (QuicksyncPreset) {
    QuicksyncPreset["Speed"] = "speed";
    QuicksyncPreset["Balanced"] = "balanced";
    QuicksyncPreset["Quality"] = "quality";
})(QuicksyncPreset = exports.QuicksyncPreset || (exports.QuicksyncPreset = {}));
var EncoderState;
(function (EncoderState) {
    /**
     * The encoder is in the normal, idle state
     */
    EncoderState["Idle"] = "idle";
    /**
     * The encoder is in the running state
     */
    EncoderState["Running"] = "running";
    /**
     * The current encoder is in a failed state
     */
    EncoderState["Failed"] = "failed";
})(EncoderState = exports.EncoderState || (exports.EncoderState = {}));
/**
 * Type specifying which aspect of the video pipeline
 * has changed in a state change event.
 */
var VideoPipelineStateChangeType;
(function (VideoPipelineStateChangeType) {
    VideoPipelineStateChangeType["Encoder"] = "encoder";
    VideoPipelineStateChangeType["Network"] = "network";
})(VideoPipelineStateChangeType = exports.VideoPipelineStateChangeType || (exports.VideoPipelineStateChangeType = {}));
/**
 * The thing that caused a state change to occur.
 */
var PipelineStateChangeReason;
(function (PipelineStateChangeReason) {
    PipelineStateChangeReason["Unknown"] = "unknown";
    PipelineStateChangeReason["UserInitiated"] = "userinitiated";
    PipelineStateChangeReason["EncoderFailure"] = "encoderfailure";
    PipelineStateChangeReason["NetworkFailure"] = "networkfailure";
    PipelineStateChangeReason["Other"] = "other";
})(PipelineStateChangeReason = exports.PipelineStateChangeReason || (exports.PipelineStateChangeReason = {}));
/**
 * The state of a given broadcasting pipeline
 */
var PipelineState;
(function (PipelineState) {
    /**
     * Not attempting to do work.
     */
    PipelineState["Idle"] = "idle";
    /**
     * Initializing and connecting to the server.
     */
    PipelineState["Connecting"] = "connecting";
    /**
     * Connected and successfully producing audio/video.
     */
    PipelineState["Connected"] = "connected";
    /**
     * Encountered an error, and is attempting to recover.
     */
    PipelineState["Reconnecting"] = "reconnecting";
    /**
     * Disconnecting and shutting down the pipeline.
     */
    PipelineState["Disconnecting"] = "disconnecting";
    /**
     * Encountered a fatal error, and is stopped.
     */
    PipelineState["Failed"] = "failed";
})(PipelineState = exports.PipelineState || (exports.PipelineState = {}));
/**
 * Type of broadcasting pipeline
 */
var PipelineType;
(function (PipelineType) {
    /**
     * Pipeline for streaming to Twitch
     */
    PipelineType["Streaming"] = "streaming";
    /**
     * Pipeline for saving video to a file.
     */
    PipelineType["Recording"] = "recording";
    /**
     * Pipeline that tests the network capacity of sending to Twitch's ingest server.
     */
    PipelineType["BandwidthTest"] = "bandwidth-test";
    /**
     * Pipeline that tests the rendering/encoding capabilities of the hardware.
     */
    PipelineType["HardwareTest"] = "hardware-test";
})(PipelineType = exports.PipelineType || (exports.PipelineType = {}));
//# sourceMappingURL=stream.js.map