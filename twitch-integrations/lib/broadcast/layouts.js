"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Video layer sizing states */
var ObjectFit;
(function (ObjectFit) {
    /** The default option. The width and height of the layer contents preserve their existing aspect ratio and scale to keep the contents inside of the layer bounds */
    ObjectFit["Contain"] = "contain";
    /** The width and height of the layer contents preserve their existing aspect ratio and scale to fill the layer bounds. Any content outside of the bounds is cropped */
    ObjectFit["Fill"] = "fill";
    /** The width and height of the layer contents are stretched to fit the layer bounds */
    ObjectFit["Stretch"] = "stretch";
})(ObjectFit = exports.ObjectFit || (exports.ObjectFit = {}));
//# sourceMappingURL=layouts.js.map