"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Grouping of audio layers */
var AudioGroupType;
(function (AudioGroupType) {
    /** The audio source the user sets as their main audio for stream */
    AudioGroupType["PrimaryInput"] = "primary-input";
    /** All other audio not primary */
    AudioGroupType["Aux"] = "aux";
    /** Custom user-defined groups */
    AudioGroupType["Custom"] = "custom";
})(AudioGroupType = exports.AudioGroupType || (exports.AudioGroupType = {}));
/** If the device is input or output in the OS */
var AudioCaptureType;
(function (AudioCaptureType) {
    AudioCaptureType["Input"] = "input";
    AudioCaptureType["Output"] = "output";
})(AudioCaptureType = exports.AudioCaptureType || (exports.AudioCaptureType = {}));
var AudioOutputChannel;
(function (AudioOutputChannel) {
    /** Usually the default, goes to stream only */
    AudioOutputChannel["OutputOnly"] = "output-only";
    /** Used when creating an audio layer in order to preview VU */
    AudioOutputChannel["MeterOnly"] = "meter-only";
    /** Sends audio sources both to monitoring device and primary output group */
    AudioOutputChannel["MonitorAndOutput"] = "monitor-and-output";
    /** Sends audio sources only to the users monitoring device */
    AudioOutputChannel["MonitorOnly"] = "monitor-only";
    /** Is detached from the mixer and sends no audio */
    AudioOutputChannel["Disabled"] = "disabled";
})(AudioOutputChannel = exports.AudioOutputChannel || (exports.AudioOutputChannel = {}));
/** Audio channel enumeration */
var Speaker;
(function (Speaker) {
    Speaker["None"] = "none";
    Speaker["Mixed"] = "mixed";
    Speaker["FrontLeft"] = "front-left";
    Speaker["FrontRight"] = "front-right";
    Speaker["FrontCenter"] = "front-center";
    Speaker["LowFreq"] = "low-freq";
    Speaker["BackLeft"] = "back-left";
    Speaker["BackRight"] = "back-right";
    Speaker["FrontLeftCenter"] = "front-center-right";
    Speaker["FrontRightCenter"] = "front-right-center";
    Speaker["BackCenter"] = "back-center";
    Speaker["SideLeft"] = "side-left";
    Speaker["SideRight"] = "side-right";
})(Speaker = exports.Speaker || (exports.Speaker = {}));
//# sourceMappingURL=audio.js.map