"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StandardFilterPluginID;
(function (StandardFilterPluginID) {
    /** Audio delay applied to a source */
    StandardFilterPluginID["AudioDelay"] = "audioDelay";
    /** Colored border around the content */
    StandardFilterPluginID["Border"] = "border";
    /** Chroma key for green screen effects */
    StandardFilterPluginID["ChromaKey"] = "chromaKey";
    /** Color effects for a video source */
    StandardFilterPluginID["ColorFilter"] = "colorFilter";
    /** Prevent audio sources from clipping */
    StandardFilterPluginID["Compressor"] = "compressor";
    /** Prevent noise below a certain level */
    StandardFilterPluginID["NoiseGate"] = "noiseGate";
    /** Silence background noise */
    StandardFilterPluginID["NoiseSuppressor"] = "noiseSuppressor";
})(StandardFilterPluginID = exports.StandardFilterPluginID || (exports.StandardFilterPluginID = {}));
var StandardPluginID;
(function (StandardPluginID) {
    /** First Party Alerts */
    StandardPluginID["Alert"] = "alerts";
    /** Electron browser source layer */
    StandardPluginID["BrowserSource"] = "browserSource";
    /** Color gradient */
    StandardPluginID["ColorGradient"] = "colorGradient";
    /** Solid color layer */
    StandardPluginID["ColorInput"] = "colorInput";
    /** Capture entire screen */
    StandardPluginID["DesktopDuplicator"] = "desktopDuplicator";
    /** Game capture layer */
    StandardPluginID["GameCapture"] = "gamecapture";
    /** Image source */
    StandardPluginID["ImageSource"] = "imageSource";
    /** Screen share plugin shared by all layouts */
    StandardPluginID["PrimaryScreenShare"] = "primaryScreenShare";
    /** Aggregate plugin that supports several different source types */
    StandardPluginID["ScreenShare"] = "screenshare";
    /** Formatted text layer */
    StandardPluginID["Text"] = "text";
    /** Cameras or other video capture devices */
    StandardPluginID["Webcam"] = "windowsVideoCapture";
    /** Capture individual window */
    StandardPluginID["WindowCapture"] = "windowCapture";
})(StandardPluginID = exports.StandardPluginID || (exports.StandardPluginID = {}));
/** Functional type of the plugin */
var PluginType;
(function (PluginType) {
    /** Plugin can be used as a visual layer or audio channel */
    PluginType["Source"] = "source";
    /** Plugin can be used combine multiple inputs into a single output (scene compositor or transition) */
    PluginType["Transform"] = "transform";
    /** Plugin can be used as a single input, single output filter on another source or transform */
    PluginType["Filter"] = "filter";
})(PluginType = exports.PluginType || (exports.PluginType = {}));
/** Media types supported by a plugin */
var SupportedMedia;
(function (SupportedMedia) {
    /** Audio-only */
    SupportedMedia["Audio"] = "audio";
    /** Video-only */
    SupportedMedia["Video"] = "video";
    /** Audio and Video */
    SupportedMedia["AudioVideo"] = "audio-video";
})(SupportedMedia = exports.SupportedMedia || (exports.SupportedMedia = {}));
/** Enum tag values to differentiate property types */
var PluginPropertyType;
(function (PluginPropertyType) {
    PluginPropertyType["VideoPreview"] = "video-preview";
    PluginPropertyType["Checkbox"] = "checkbox";
    PluginPropertyType["Toggle"] = "toggle";
    PluginPropertyType["TextInput"] = "text-input";
    PluginPropertyType["URLInput"] = "url-input";
    PluginPropertyType["BoxSize"] = "box-size";
    PluginPropertyType["BoxSizeWithAspectLock"] = "box-size-with-aspect-lock";
    PluginPropertyType["TextArea"] = "text-area";
    PluginPropertyType["Color"] = "color";
    PluginPropertyType["File"] = "file";
    PluginPropertyType["Select"] = "select";
    PluginPropertyType["Range"] = "range";
    PluginPropertyType["Button"] = "button";
    PluginPropertyType["ScreenShare"] = "screen-share";
    PluginPropertyType["Drawer"] = "drawer";
    PluginPropertyType["Alert"] = "alert";
    PluginPropertyType["Separator"] = "separator";
    PluginPropertyType["SvgToPng"] = "svg-to-png";
    PluginPropertyType["Font"] = "font";
    PluginPropertyType["Alignment"] = "alignment";
    PluginPropertyType["VolumeControl"] = "volume-control";
    PluginPropertyType["MediaLibrary"] = "media-library";
    PluginPropertyType["WebcamSelect"] = "webcam-select";
    PluginPropertyType["Duration"] = "duration";
})(PluginPropertyType = exports.PluginPropertyType || (exports.PluginPropertyType = {}));
/** The type of source for a screen share layer. */
var ScreenShareType;
(function (ScreenShareType) {
    ScreenShareType["None"] = "none";
    ScreenShareType["Automatic"] = "automatic";
    ScreenShareType["Display"] = "display";
    ScreenShareType["Window"] = "window";
    ScreenShareType["Webcam"] = "webcam";
    ScreenShareType["CaptureCard"] = "capture-card";
    ScreenShareType["NDI"] = "ndi";
})(ScreenShareType = exports.ScreenShareType || (exports.ScreenShareType = {}));
/** Text alignment options (CSS.TextAlignProperty) */
var TextAlign;
(function (TextAlign) {
    TextAlign["Left"] = "left";
    TextAlign["Center"] = "center";
    TextAlign["Right"] = "right";
    TextAlign["Justify"] = "justify";
})(TextAlign = exports.TextAlign || (exports.TextAlign = {}));
/** Vertical child alignment options (CSS.AlignSelfProperty) */
var AlignSelf;
(function (AlignSelf) {
    AlignSelf["Start"] = "flex-start";
    AlignSelf["Center"] = "center";
    AlignSelf["End"] = "flex-end";
})(AlignSelf = exports.AlignSelf || (exports.AlignSelf = {}));
/** Media types to surface to the user */
var MediaTypes;
(function (MediaTypes) {
    MediaTypes["Image"] = "image";
    MediaTypes["Video"] = "video";
    MediaTypes["Sound"] = "sound";
})(MediaTypes = exports.MediaTypes || (exports.MediaTypes = {}));
/**
 * A category for an image loaded from the media library. Generally
 * used to label a specific sort of static asset, such as one meant
 * to be used as a wallpaper.
 */
var MediaLibraryImageCategory;
(function (MediaLibraryImageCategory) {
    MediaLibraryImageCategory["Wallpaper"] = "wallpaper";
    MediaLibraryImageCategory["Pattern"] = "pattern";
    MediaLibraryImageCategory["AccentImage"] = "accentImage";
})(MediaLibraryImageCategory = exports.MediaLibraryImageCategory || (exports.MediaLibraryImageCategory = {}));
//# sourceMappingURL=plugin.js.map