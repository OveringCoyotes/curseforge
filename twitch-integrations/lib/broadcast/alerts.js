"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** The type of alert that will show based on a stream event */
var AlertType;
(function (AlertType) {
    AlertType["Follow"] = "follow";
    AlertType["Host"] = "host";
    AlertType["Cheer"] = "cheer";
    AlertType["Raid"] = "raid";
    AlertType["Subscription"] = "subscription";
})(AlertType = exports.AlertType || (exports.AlertType = {}));
/** The availability that an alert type has for a streamer */
var AlertAccess;
(function (AlertAccess) {
    AlertAccess["Everyone"] = "everyone";
    AlertAccess["Affiliate"] = "affiliate";
    AlertAccess["Partner"] = "partner";
})(AlertAccess = exports.AlertAccess || (exports.AlertAccess = {}));
//# sourceMappingURL=alerts.js.map