"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WindowIntegrationButton;
(function (WindowIntegrationButton) {
    WindowIntegrationButton["Menu"] = "menu";
    WindowIntegrationButton["Home"] = "home";
    WindowIntegrationButton["Back"] = "back";
    WindowIntegrationButton["Forward"] = "forward";
    WindowIntegrationButton["Refresh"] = "refresh";
})(WindowIntegrationButton = exports.WindowIntegrationButton || (exports.WindowIntegrationButton = {}));
var WindowIntegrationButtonVisibility;
(function (WindowIntegrationButtonVisibility) {
    /* Action is visible and enabled */
    WindowIntegrationButtonVisibility["Visible"] = "visible";
    /* Action is invisible and does not hold space */
    WindowIntegrationButtonVisibility["Invisible"] = "invisible";
    /* Action is disabled */
    WindowIntegrationButtonVisibility["Disabled"] = "disabled";
    /* Action is invisible, but should hold space */
    WindowIntegrationButtonVisibility["Hidden"] = "hidden";
})(WindowIntegrationButtonVisibility = exports.WindowIntegrationButtonVisibility || (exports.WindowIntegrationButtonVisibility = {}));
//# sourceMappingURL=button.js.map