"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Severity of banner shown by notification. Will affect banner appearance.
 */
var BannerSeverity;
(function (BannerSeverity) {
    BannerSeverity["Error"] = "Error";
    BannerSeverity["Info"] = "Info";
    BannerSeverity["Success"] = "Success";
    BannerSeverity["Warning"] = "Warning";
})(BannerSeverity = exports.BannerSeverity || (exports.BannerSeverity = {}));
//# sourceMappingURL=banner-notification.js.map