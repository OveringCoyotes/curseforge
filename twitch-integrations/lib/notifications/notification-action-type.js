"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Used to target the notification actions to a portion of UI.
 * The TwitchAgent process currently providing toast UI for
 * notifications requires this information in order to properly
 * report back which part of the toast was clicked.
 */
var NotificationsIntegrationNotificationActionType;
(function (NotificationsIntegrationNotificationActionType) {
    /** Describes the Body of the Toast UI. */
    NotificationsIntegrationNotificationActionType["Body"] = "Body";
    /** Describes a button on the Toast UI. */
    NotificationsIntegrationNotificationActionType["Button"] = "Button";
    /** Describes the close button on the Toast UI. */
    NotificationsIntegrationNotificationActionType["Close"] = "Close";
})(NotificationsIntegrationNotificationActionType = exports.NotificationsIntegrationNotificationActionType || (exports.NotificationsIntegrationNotificationActionType = {}));
//# sourceMappingURL=notification-action-type.js.map