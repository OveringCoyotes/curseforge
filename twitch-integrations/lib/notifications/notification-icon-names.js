"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Describes the Desklight resource names for action icons.
 * They are used to lookup the resource by name in the TwitchAgent
 * process. It is done this way because the Current UI design cannot
 * accomodate custom strings at this time,
 */
var NotificationsIntegrationNotificationIconNames;
(function (NotificationsIntegrationNotificationIconNames) {
    NotificationsIntegrationNotificationIconNames["AcceptCallIcon"] = "AcceptCallIcon";
    NotificationsIntegrationNotificationIconNames["AcceptIcon"] = "AcceptIcon";
    NotificationsIntegrationNotificationIconNames["AcceptVideoCallIcon"] = "AcceptVideoCallIcon";
    NotificationsIntegrationNotificationIconNames["CancelIcon"] = "CancelIcon";
    NotificationsIntegrationNotificationIconNames["DeclineCallIcon"] = "DeclineCallIcon";
    NotificationsIntegrationNotificationIconNames["MicIcon"] = "MicIcon";
    NotificationsIntegrationNotificationIconNames["MicXIcon"] = "MicXIcon";
    NotificationsIntegrationNotificationIconNames["MicXIconRed"] = "MicXIconRed";
    NotificationsIntegrationNotificationIconNames["SpeakerIcon"] = "SpeakerIcon";
    NotificationsIntegrationNotificationIconNames["SpeakerXIcon"] = "SpeakerXIcon";
})(NotificationsIntegrationNotificationIconNames = exports.NotificationsIntegrationNotificationIconNames || (exports.NotificationsIntegrationNotificationIconNames = {}));
//# sourceMappingURL=notification-icon-names.js.map