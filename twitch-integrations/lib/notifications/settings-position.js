"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NotificationsIntegrationPosition;
(function (NotificationsIntegrationPosition) {
    NotificationsIntegrationPosition["BottomLeft"] = "bottom-left";
    NotificationsIntegrationPosition["BottomRight"] = "bottom-right";
    NotificationsIntegrationPosition["TopLeft"] = "top-left";
    NotificationsIntegrationPosition["TopRight"] = "top-right";
})(NotificationsIntegrationPosition = exports.NotificationsIntegrationPosition || (exports.NotificationsIntegrationPosition = {}));
//# sourceMappingURL=settings-position.js.map