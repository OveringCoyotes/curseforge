"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PixelFormat;
(function (PixelFormat) {
    PixelFormat["RGBA"] = "rgba";
    PixelFormat["BGRA"] = "bgra";
})(PixelFormat || (PixelFormat = {}));
exports.VideoPreviewIntegrationPixelFormat = PixelFormat;
//# sourceMappingURL=index.js.map