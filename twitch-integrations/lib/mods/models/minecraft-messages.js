"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Reason code for MinecraftInstancesChanged event message   */
var ModsIntegrationMinecraftInstancesChangedReason;
(function (ModsIntegrationMinecraftInstancesChangedReason) {
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["Initialized"] = 1] = "Initialized";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["LoadedFromDisk"] = 2] = "LoadedFromDisk";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["SavedToDisk"] = 3] = "SavedToDisk";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["AddedToDatabase"] = 4] = "AddedToDatabase";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["RemovedFromDatabase"] = 5] = "RemovedFromDatabase";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["UpdatedProperties"] = 6] = "UpdatedProperties";
    ModsIntegrationMinecraftInstancesChangedReason[ModsIntegrationMinecraftInstancesChangedReason["UnknownModResultsChanged"] = 7] = "UnknownModResultsChanged";
})(ModsIntegrationMinecraftInstancesChangedReason = exports.ModsIntegrationMinecraftInstancesChangedReason || (exports.ModsIntegrationMinecraftInstancesChangedReason = {}));
//# sourceMappingURL=minecraft-messages.js.map