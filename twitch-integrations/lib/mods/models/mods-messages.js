"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Mods PubSub enums
 */
/** Reason code for GameInstancesChanged firing */
var ModsIntegrationGameInstancesChangedReason;
(function (ModsIntegrationGameInstancesChangedReason) {
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["Initialized"] = 0] = "Initialized";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["PermissionsChanged"] = 1] = "PermissionsChanged";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["AddedToDatabase"] = 2] = "AddedToDatabase";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["RemovedFromDatabase"] = 3] = "RemovedFromDatabase";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["Saved"] = 4] = "Saved";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["IsValidChanged"] = 5] = "IsValidChanged";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["IsEnabledChanged"] = 6] = "IsEnabledChanged";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["HasSufficientPermissionsChanged"] = 7] = "HasSufficientPermissionsChanged";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["SyncSettingsChanged"] = 8] = "SyncSettingsChanged";
    ModsIntegrationGameInstancesChangedReason[ModsIntegrationGameInstancesChangedReason["UpdateCountsChanged"] = 9] = "UpdateCountsChanged";
})(ModsIntegrationGameInstancesChangedReason = exports.ModsIntegrationGameInstancesChangedReason || (exports.ModsIntegrationGameInstancesChangedReason = {}));
/** Result status code for game detection async event completion */
var ModsIntegrationGameDetectionCompletedResultCode;
(function (ModsIntegrationGameDetectionCompletedResultCode) {
    ModsIntegrationGameDetectionCompletedResultCode[ModsIntegrationGameDetectionCompletedResultCode["Success"] = 0] = "Success";
    ModsIntegrationGameDetectionCompletedResultCode[ModsIntegrationGameDetectionCompletedResultCode["ErrorScanningVolume"] = 1] = "ErrorScanningVolume";
    ModsIntegrationGameDetectionCompletedResultCode[ModsIntegrationGameDetectionCompletedResultCode["ErrorScanningDirectory"] = 2] = "ErrorScanningDirectory";
})(ModsIntegrationGameDetectionCompletedResultCode = exports.ModsIntegrationGameDetectionCompletedResultCode || (exports.ModsIntegrationGameDetectionCompletedResultCode = {}));
/** Reason code for InstallTaskProgressChanged firing */
var ModsIntegrationInstallTaskProgressReason;
(function (ModsIntegrationInstallTaskProgressReason) {
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenValue"] = 0] = "UpdateInstallTokenValue";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenStatusText"] = 1] = "UpdateInstallTokenStatusText";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenStatusTitle"] = 2] = "UpdateInstallTokenStatusTitle";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenProgress"] = 3] = "UpdateInstallTokenProgress";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenIsActive"] = 4] = "UpdateInstallTokenIsActive";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenIsPending"] = 5] = "UpdateInstallTokenIsPending";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["UpdateInstallTokenIsCancellationRequested"] = 6] = "UpdateInstallTokenIsCancellationRequested";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["Completed"] = 7] = "Completed";
    ModsIntegrationInstallTaskProgressReason[ModsIntegrationInstallTaskProgressReason["Errored"] = 8] = "Errored";
})(ModsIntegrationInstallTaskProgressReason = exports.ModsIntegrationInstallTaskProgressReason || (exports.ModsIntegrationInstallTaskProgressReason = {}));
/** Result status code for fingerprint async event completion */
var ModsIntegrationGameInstanceScanCompletedReason;
(function (ModsIntegrationGameInstanceScanCompletedReason) {
    ModsIntegrationGameInstanceScanCompletedReason[ModsIntegrationGameInstanceScanCompletedReason["Success"] = 0] = "Success";
    ModsIntegrationGameInstanceScanCompletedReason[ModsIntegrationGameInstanceScanCompletedReason["Error"] = 1] = "Error";
})(ModsIntegrationGameInstanceScanCompletedReason = exports.ModsIntegrationGameInstanceScanCompletedReason || (exports.ModsIntegrationGameInstanceScanCompletedReason = {}));
/** Reason code for installed addons changed message */
var ModsIntegrationInstalledAddonsChangedReason;
(function (ModsIntegrationInstalledAddonsChangedReason) {
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["Initialized"] = 0] = "Initialized";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["ScanResult"] = 1] = "ScanResult";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["Installed"] = 2] = "Installed";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["Uninstalled"] = 3] = "Uninstalled";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["Refreshed"] = 4] = "Refreshed";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["Updated"] = 5] = "Updated";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateFilenameOnDisk"] = 6] = "UpdateFilenameOnDisk";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateIsEnabled"] = 7] = "UpdateIsEnabled";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateInstalledFile"] = 8] = "UpdateInstalledFile";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateLatestFile"] = 9] = "UpdateLatestFile";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdatePreferenceReleaseType"] = 10] = "UpdatePreferenceReleaseType";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdatePreferenceAlternateFile"] = 11] = "UpdatePreferenceAlternateFile";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdatePreferenceAutoInstallUpdates"] = 12] = "UpdatePreferenceAutoInstallUpdates";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdatePreferenceIsIgnored"] = 13] = "UpdatePreferenceIsIgnored";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateIsWorkingCopy"] = 14] = "UpdateIsWorkingCopy";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateIsModified"] = 15] = "UpdateIsModified";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["UpdateIsFuzzyMatch"] = 16] = "UpdateIsFuzzyMatch";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["AddonTaskComplete"] = 17] = "AddonTaskComplete";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferenceAutoInstallUpdatesChanged"] = 18] = "DefaultPreferenceAutoInstallUpdatesChanged";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferenceProcessFileCommandsChanged"] = 19] = "DefaultPreferenceProcessFileCommandsChanged";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferenceDeleteSavedVariablesChanged"] = 20] = "DefaultPreferenceDeleteSavedVariablesChanged";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferenceQuickDeleteLibrariesChanged"] = 21] = "DefaultPreferenceQuickDeleteLibrariesChanged";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferencePreferenceAlternateFileChanged"] = 22] = "DefaultPreferencePreferenceAlternateFileChanged";
    ModsIntegrationInstalledAddonsChangedReason[ModsIntegrationInstalledAddonsChangedReason["DefaultPreferencePreferenceReleaseTypeChanged"] = 23] = "DefaultPreferencePreferenceReleaseTypeChanged";
})(ModsIntegrationInstalledAddonsChangedReason = exports.ModsIntegrationInstalledAddonsChangedReason || (exports.ModsIntegrationInstalledAddonsChangedReason = {}));
//# sourceMappingURL=mods-messages.js.map