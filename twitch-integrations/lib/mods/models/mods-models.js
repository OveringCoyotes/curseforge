"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * GameService enums
 */
/** Result status code for AddGameInstance methods */
var ModsIntegrationAddGameInstanceStatus;
(function (ModsIntegrationAddGameInstanceStatus) {
    ModsIntegrationAddGameInstanceStatus[ModsIntegrationAddGameInstanceStatus["Successful"] = 0] = "Successful";
    ModsIntegrationAddGameInstanceStatus[ModsIntegrationAddGameInstanceStatus["DirectoryNotFound"] = 1] = "DirectoryNotFound";
    ModsIntegrationAddGameInstanceStatus[ModsIntegrationAddGameInstanceStatus["InvalidDirectory"] = 2] = "InvalidDirectory";
    ModsIntegrationAddGameInstanceStatus[ModsIntegrationAddGameInstanceStatus["AlreadyExists"] = 3] = "AlreadyExists";
})(ModsIntegrationAddGameInstanceStatus = exports.ModsIntegrationAddGameInstanceStatus || (exports.ModsIntegrationAddGameInstanceStatus = {}));
/** Release type for addon file */
var ModsIntegrationFileType;
(function (ModsIntegrationFileType) {
    ModsIntegrationFileType[ModsIntegrationFileType["Beta"] = 0] = "Beta";
    ModsIntegrationFileType[ModsIntegrationFileType["Release"] = 1] = "Release";
    ModsIntegrationFileType[ModsIntegrationFileType["Alpha"] = 2] = "Alpha";
})(ModsIntegrationFileType = exports.ModsIntegrationFileType || (exports.ModsIntegrationFileType = {}));
/** Addon status code */
var ModsIntegrationAddonStatus;
(function (ModsIntegrationAddonStatus) {
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["OutOfDate"] = 3] = "OutOfDate";
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["RecentlyUpdated"] = 4] = "RecentlyUpdated";
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["Normal"] = 5] = "Normal";
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["WorkingCopy"] = 6] = "WorkingCopy";
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["Ignored"] = 7] = "Ignored";
    ModsIntegrationAddonStatus[ModsIntegrationAddonStatus["Pending"] = 10] = "Pending";
})(ModsIntegrationAddonStatus = exports.ModsIntegrationAddonStatus || (exports.ModsIntegrationAddonStatus = {}));
/** Addon install task completion reason */
var ModsIntegrationInstallTaskCompletionReason;
(function (ModsIntegrationInstallTaskCompletionReason) {
    ModsIntegrationInstallTaskCompletionReason[ModsIntegrationInstallTaskCompletionReason["Successful"] = 1] = "Successful";
    ModsIntegrationInstallTaskCompletionReason[ModsIntegrationInstallTaskCompletionReason["Error"] = 2] = "Error";
    ModsIntegrationInstallTaskCompletionReason[ModsIntegrationInstallTaskCompletionReason["Cancelled"] = 99] = "Cancelled";
})(ModsIntegrationInstallTaskCompletionReason = exports.ModsIntegrationInstallTaskCompletionReason || (exports.ModsIntegrationInstallTaskCompletionReason = {}));
/** Addon install failure type */
var ModsIntegrationInstallTaskErrorReason;
(function (ModsIntegrationInstallTaskErrorReason) {
    ModsIntegrationInstallTaskErrorReason[ModsIntegrationInstallTaskErrorReason["General"] = 1] = "General";
    ModsIntegrationInstallTaskErrorReason[ModsIntegrationInstallTaskErrorReason["Permissions"] = 2] = "Permissions";
    ModsIntegrationInstallTaskErrorReason[ModsIntegrationInstallTaskErrorReason["IOException"] = 3] = "IOException";
    ModsIntegrationInstallTaskErrorReason[ModsIntegrationInstallTaskErrorReason["MinecraftErrorJavaNotInstalled"] = 70] = "MinecraftErrorJavaNotInstalled";
})(ModsIntegrationInstallTaskErrorReason = exports.ModsIntegrationInstallTaskErrorReason || (exports.ModsIntegrationInstallTaskErrorReason = {}));
//# sourceMappingURL=mods-models.js.map