"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Minecraft settings option for preferred release type  */
var ModsIntegrationMinecraftPreferredReleaseOption;
(function (ModsIntegrationMinecraftPreferredReleaseOption) {
    ModsIntegrationMinecraftPreferredReleaseOption[ModsIntegrationMinecraftPreferredReleaseOption["Release"] = 0] = "Release";
    ModsIntegrationMinecraftPreferredReleaseOption[ModsIntegrationMinecraftPreferredReleaseOption["Beta"] = 1] = "Beta";
    ModsIntegrationMinecraftPreferredReleaseOption[ModsIntegrationMinecraftPreferredReleaseOption["Alpha"] = 2] = "Alpha";
})(ModsIntegrationMinecraftPreferredReleaseOption = exports.ModsIntegrationMinecraftPreferredReleaseOption || (exports.ModsIntegrationMinecraftPreferredReleaseOption = {}));
/** Minecraft settings option for launcher window behavior on launch */
var ModsIntegrationMinecraftLauncherVisibilityOption;
(function (ModsIntegrationMinecraftLauncherVisibilityOption) {
    ModsIntegrationMinecraftLauncherVisibilityOption[ModsIntegrationMinecraftLauncherVisibilityOption["Close"] = 0] = "Close";
    ModsIntegrationMinecraftLauncherVisibilityOption[ModsIntegrationMinecraftLauncherVisibilityOption["Keep"] = 1] = "Keep";
    ModsIntegrationMinecraftLauncherVisibilityOption[ModsIntegrationMinecraftLauncherVisibilityOption["Hide"] = 2] = "Hide";
})(ModsIntegrationMinecraftLauncherVisibilityOption = exports.ModsIntegrationMinecraftLauncherVisibilityOption || (exports.ModsIntegrationMinecraftLauncherVisibilityOption = {}));
/** Minecraft settings option for preferred launcher method */
var ModsIntegrationMinecraftLauncherMethod;
(function (ModsIntegrationMinecraftLauncherMethod) {
    ModsIntegrationMinecraftLauncherMethod[ModsIntegrationMinecraftLauncherMethod["UseNativeLauncher"] = 0] = "UseNativeLauncher";
    ModsIntegrationMinecraftLauncherMethod[ModsIntegrationMinecraftLauncherMethod["UseJarLauncher"] = 1] = "UseJarLauncher";
})(ModsIntegrationMinecraftLauncherMethod = exports.ModsIntegrationMinecraftLauncherMethod || (exports.ModsIntegrationMinecraftLauncherMethod = {}));
//# sourceMappingURL=minecraft-settings-enums.js.map