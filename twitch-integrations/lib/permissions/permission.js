"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PermissionsIntegrationPermissionType;
(function (PermissionsIntegrationPermissionType) {
    PermissionsIntegrationPermissionType["Accessibility"] = "accessibility";
    PermissionsIntegrationPermissionType["DesktopAudioCapture"] = "desktop-audio-capture";
    PermissionsIntegrationPermissionType["Microphone"] = "microphone";
    PermissionsIntegrationPermissionType["ScreenRecording"] = "screen-recording";
    PermissionsIntegrationPermissionType["Webcam"] = "webcam";
})(PermissionsIntegrationPermissionType = exports.PermissionsIntegrationPermissionType || (exports.PermissionsIntegrationPermissionType = {}));
var PermissionsIntegrationPermissionStatus;
(function (PermissionsIntegrationPermissionStatus) {
    PermissionsIntegrationPermissionStatus["NotGranted"] = "not-granted";
    PermissionsIntegrationPermissionStatus["Granted"] = "granted";
    PermissionsIntegrationPermissionStatus["Granting"] = "granting";
})(PermissionsIntegrationPermissionStatus = exports.PermissionsIntegrationPermissionStatus || (exports.PermissionsIntegrationPermissionStatus = {}));
//# sourceMappingURL=permission.js.map