"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NetworkIntegrationConnectionStatus;
(function (NetworkIntegrationConnectionStatus) {
    NetworkIntegrationConnectionStatus["NetworkDisconnected"] = "network-disconnected";
    NetworkIntegrationConnectionStatus["WebsiteUnreachable"] = "website-unreachable";
    NetworkIntegrationConnectionStatus["Online"] = "online";
})(NetworkIntegrationConnectionStatus = exports.NetworkIntegrationConnectionStatus || (exports.NetworkIntegrationConnectionStatus = {}));
//# sourceMappingURL=connection-status.js.map