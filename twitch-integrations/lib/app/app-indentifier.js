"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The client's identifier for an application
 */
var AppIntegrationID;
(function (AppIntegrationID) {
    AppIntegrationID["Desklight"] = "com.twitch.desklight";
    AppIntegrationID["Spotlight"] = "com.twitch.spotlight";
    AppIntegrationID["Radio"] = "com.twitch.radio";
})(AppIntegrationID = exports.AppIntegrationID || (exports.AppIntegrationID = {}));
//# sourceMappingURL=app-indentifier.js.map