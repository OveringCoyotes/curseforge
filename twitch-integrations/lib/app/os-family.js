"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppIntegrationOSFamily;
(function (AppIntegrationOSFamily) {
    AppIntegrationOSFamily["Windows"] = "windows";
    AppIntegrationOSFamily["MacOS"] = "macOS";
})(AppIntegrationOSFamily = exports.AppIntegrationOSFamily || (exports.AppIntegrationOSFamily = {}));
//# sourceMappingURL=os-family.js.map