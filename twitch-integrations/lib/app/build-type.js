"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The compiled build type for an application
 */
var AppIntegrationBuildType;
(function (AppIntegrationBuildType) {
    AppIntegrationBuildType["Development"] = "development";
    AppIntegrationBuildType["Production"] = "production";
    AppIntegrationBuildType["Test"] = "test";
    AppIntegrationBuildType["Internal"] = "internal";
})(AppIntegrationBuildType = exports.AppIntegrationBuildType || (exports.AppIntegrationBuildType = {}));
//# sourceMappingURL=build-type.js.map