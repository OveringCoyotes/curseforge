"use strict";
/** Request params for returning the Sidecar Status */
Object.defineProperty(exports, "__esModule", { value: true });
/** Sidecar Status */
var SidecarStatusType;
(function (SidecarStatusType) {
    SidecarStatusType["ConnectionFailed"] = "ConnectionFailed";
    SidecarStatusType["Idle"] = "Idle";
    SidecarStatusType["Connected"] = "Connected";
    SidecarStatusType["Connecting"] = "Connecting";
})(SidecarStatusType = exports.SidecarStatusType || (exports.SidecarStatusType = {}));
//# sourceMappingURL=sidecar-status.js.map