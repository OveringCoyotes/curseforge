"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function clearTwitchIntegrationsInstance() {
    delete window.__twitchIntegrations;
}
exports.clearTwitchIntegrationsInstance = clearTwitchIntegrationsInstance;
function getTwitchIntegrationsInstance() {
    return window.__twitchIntegrations;
}
exports.getTwitchIntegrationsInstance = getTwitchIntegrationsInstance;
function setTwitchIntegrationsInstance(instance) {
    window.__twitchIntegrations = instance;
}
exports.setTwitchIntegrationsInstance = setTwitchIntegrationsInstance;
//# sourceMappingURL=utils.js.map