"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReleaseIntegrationUpdateStatus;
(function (ReleaseIntegrationUpdateStatus) {
    ReleaseIntegrationUpdateStatus["UpdateCheckFailed"] = "update-check-failed";
    ReleaseIntegrationUpdateStatus["UpToDate"] = "up-to-date";
    ReleaseIntegrationUpdateStatus["Updating"] = "updating";
    ReleaseIntegrationUpdateStatus["UpdateAvailable"] = "update-available";
    ReleaseIntegrationUpdateStatus["UpdateFailed"] = "update-failed";
})(ReleaseIntegrationUpdateStatus = exports.ReleaseIntegrationUpdateStatus || (exports.ReleaseIntegrationUpdateStatus = {}));
var ReleaseIntegrationReleaseStage;
(function (ReleaseIntegrationReleaseStage) {
    ReleaseIntegrationReleaseStage["Live"] = "live";
    ReleaseIntegrationReleaseStage["RC"] = "rc";
    ReleaseIntegrationReleaseStage["Canary"] = "canary";
})(ReleaseIntegrationReleaseStage = exports.ReleaseIntegrationReleaseStage || (exports.ReleaseIntegrationReleaseStage = {}));
//# sourceMappingURL=update-status.js.map