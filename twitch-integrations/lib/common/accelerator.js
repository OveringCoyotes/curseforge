"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A list of non-literal key codes for keybinds
 * If the key doesn't exist in this list it's just the key itself
 * e.g. a is "a"
 */
var TwitchIntegrationAcceleratorKey;
(function (TwitchIntegrationAcceleratorKey) {
    TwitchIntegrationAcceleratorKey["Plus"] = "Plus";
    TwitchIntegrationAcceleratorKey["Space"] = "Space";
    TwitchIntegrationAcceleratorKey["Tab"] = "Tab";
    TwitchIntegrationAcceleratorKey["Backspace"] = "Backspace";
    TwitchIntegrationAcceleratorKey["Delete"] = "Delete";
    TwitchIntegrationAcceleratorKey["Insert"] = "Insert";
    TwitchIntegrationAcceleratorKey["Return"] = "Return";
    TwitchIntegrationAcceleratorKey["Enter"] = "Enter";
    TwitchIntegrationAcceleratorKey["Up"] = "Up";
    TwitchIntegrationAcceleratorKey["Down"] = "Down";
    TwitchIntegrationAcceleratorKey["Left"] = "Left";
    TwitchIntegrationAcceleratorKey["Right"] = "Right";
    TwitchIntegrationAcceleratorKey["Home"] = "Home";
    TwitchIntegrationAcceleratorKey["End"] = "End";
    TwitchIntegrationAcceleratorKey["PageUp"] = "PageUp";
    TwitchIntegrationAcceleratorKey["PageDown"] = "PageDown";
    TwitchIntegrationAcceleratorKey["Escape"] = "Escape";
    TwitchIntegrationAcceleratorKey["VolumeUp"] = "VolumeUp";
    TwitchIntegrationAcceleratorKey["VolumeDown"] = "VolumeDown";
    TwitchIntegrationAcceleratorKey["VolumeMute"] = "VolumeMute";
    TwitchIntegrationAcceleratorKey["MediaNextTrack"] = "MediaNextTrack";
    TwitchIntegrationAcceleratorKey["MediaPreviousTrack"] = "MediaPreviousTrack";
    TwitchIntegrationAcceleratorKey["MediaStop"] = "MediaStop";
    TwitchIntegrationAcceleratorKey["MediaPlayPause"] = "MediaPlayPause";
    TwitchIntegrationAcceleratorKey["PrintScreen"] = "PrintScreen";
})(TwitchIntegrationAcceleratorKey = exports.TwitchIntegrationAcceleratorKey || (exports.TwitchIntegrationAcceleratorKey = {}));
/**
 * A list of modifiers for keybinds
 */
var TwitchIntegrationAcceleratorModifier;
(function (TwitchIntegrationAcceleratorModifier) {
    TwitchIntegrationAcceleratorModifier["Command"] = "Command";
    TwitchIntegrationAcceleratorModifier["Control"] = "Control";
    TwitchIntegrationAcceleratorModifier["CommandOrControl"] = "CommandOrControl";
    TwitchIntegrationAcceleratorModifier["Alt"] = "Alt";
    TwitchIntegrationAcceleratorModifier["Option"] = "Option";
    TwitchIntegrationAcceleratorModifier["AltGr"] = "AltGr";
    TwitchIntegrationAcceleratorModifier["Shift"] = "Shift";
    TwitchIntegrationAcceleratorModifier["Super"] = "Super";
})(TwitchIntegrationAcceleratorModifier = exports.TwitchIntegrationAcceleratorModifier || (exports.TwitchIntegrationAcceleratorModifier = {}));
//# sourceMappingURL=accelerator.js.map