"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** User action for Error State */
var UserAction;
(function (UserAction) {
    /** No action required */
    UserAction["NoAction"] = "no-action";
    /** Settings should be updated */
    UserAction["Settings"] = "settings";
    /** Permissions need to be updated */
    UserAction["Permissions"] = "permissions";
})(UserAction = exports.UserAction || (exports.UserAction = {}));
//# sourceMappingURL=error.js.map