"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AdminIntegrationServiceInstallState;
(function (AdminIntegrationServiceInstallState) {
    AdminIntegrationServiceInstallState["NotInstalled"] = "not-installed";
    AdminIntegrationServiceInstallState["Installing"] = "installing";
    AdminIntegrationServiceInstallState["Installed"] = "installed";
    AdminIntegrationServiceInstallState["Failed"] = "failed";
})(AdminIntegrationServiceInstallState = exports.AdminIntegrationServiceInstallState || (exports.AdminIntegrationServiceInstallState = {}));
//# sourceMappingURL=process.js.map