"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Available Integrations key strings that link the implementation and the window global key */
var IntegrationType;
(function (IntegrationType) {
    IntegrationType["AccountConnection"] = "accountConnection";
    IntegrationType["Admin"] = "admin";
    IntegrationType["App"] = "app";
    IntegrationType["AppSettings"] = "appSettings";
    IntegrationType["Broadcast"] = "broadcast";
    IntegrationType["BroadcastPlugin"] = "broadcastPlugin";
    IntegrationType["BugReporting"] = "bugReporting";
    IntegrationType["Demo"] = "demo";
    IntegrationType["Display"] = "display";
    IntegrationType["AudioPlaybackController"] = "audioPlaybackController";
    IntegrationType["AudioPlaybackReceiver"] = "audioPlaybackReceiver";
    IntegrationType["FileSystem"] = "fileSystem";
    IntegrationType["GameLibrary"] = "gameLibrary";
    IntegrationType["Hotkeys"] = "hotkeys";
    IntegrationType["Logging"] = "logging";
    IntegrationType["Menu"] = "menu";
    IntegrationType["Mods"] = "mods";
    IntegrationType["Network"] = "network";
    IntegrationType["Notifications"] = "notifications";
    IntegrationType["Performance"] = "performance";
    IntegrationType["Permissions"] = "permissions";
    IntegrationType["Process"] = "process";
    IntegrationType["Release"] = "release";
    IntegrationType["Session"] = "session";
    IntegrationType["Sidecar"] = "sidecar";
    IntegrationType["Tracking"] = "tracking";
    IntegrationType["VideoPreview"] = "videoPreview";
    IntegrationType["Window"] = "window";
})(IntegrationType = exports.IntegrationType || (exports.IntegrationType = {}));
//# sourceMappingURL=integration.js.map