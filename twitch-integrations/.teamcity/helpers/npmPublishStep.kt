package helpers

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.BuildSteps
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.script

fun npmPublishStep(publishCommand: String): BuildSteps.() -> Unit {
  return {
    // Upload PDBs
    // script {
    //   name = "Upload Symbols"
    //   scriptContent = """
    //     yarn && yarn tc:symbols
    //   """.trimIndent()
    // }

    // Authenticate with npm, publish package
    // Steps on using vcvars64 from Powershell taken from - https://help.appveyor.com/discussions/questions/18777-how-to-use-vcvars64bat-from-powershell
    powerShell {
      name = "Release"
      scriptMode = script {
        content = """
          ${'$'}headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
          ${'$'}headers.Add("Accept", 'application/json')
          ${'$'}data = @{}
          ${'$'}data.Add('name', "%npm.user%")
          ${'$'}data.Add('password', '%npm.password%')
          ${'$'}json = ${'$'}data | ConvertTo-Json
          ${'$'}r = Invoke-RestMethod -Uri https://%npm.url%/-/user/org.couchdb.user:%npm.user% -Headers ${'$'}headers -Method Put -Body ${'$'}json -ContentType 'application/json'
          ${'$'}r.token
          npm set registry "https://%npm.url%"
          npm set //%npm.url%/:_authToken ${'$'}r.token
          cmd.exe /c "call `"%vcvars64.location%`" && set > vcvars.txt"
          Get-Content "vcvars.txt" | Foreach-Object {
            if (${'$'}_ -match "^(.*?)=(.*)${'$'}") {
              Set-Content "env:\${'$'}(${'$'}matches[1])" ${'$'}matches[2]
            }
          }
          yarn
          $publishCommand
        """.trimIndent()
      }
    }
  }
}
