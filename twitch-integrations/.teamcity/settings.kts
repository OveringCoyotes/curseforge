import helpers.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.exec
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.GitVcsRoot
import twitch.instrumentorum.instrum
import twitch.instrumentorum.project.builds.initializeBuildChain
import java.io.File

version = "2018.2"

instrum {
  description = "twitch-integrations"

  // VCS root referencing the associated github repository
  sourceRoot("git@git.xarth.tv:twilight/twitch-integrations.git")

  // Parameters
  params(projectBranchSpecification)
  textParam("branch.sanitized", "", "Placeholder for Sanitized Branch Name", "", ParameterDisplay.HIDDEN)
  textParam("git.commit.short", "", "Placeholder for Short Commit Hash", "", ParameterDisplay.HIDDEN)
  textParam("vcvars64.location", "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\VC\\Auxiliary\\Build\\vcvars64.bat", "vcvars", "Location to load vcvars64 for build env", ParameterDisplay.NORMAL)
  textParam("npm.user", "desktop", "npm", "User to login to npm")
  textParam("npm.url", "npm.internal.justin.tv", "npm", "Endpoint for npm")
  passwordParam("npm.password", "credentialsJSON:f716e2e9-9fad-4dc5-a3eb-ac8c467887ef", "npm")

  // Initialize the build chain and naming scheme for build IDs to prevent duplicate builds
  initializeBuildChain {
    requirements(requireLinux)
  }

  subInstrum("twitch-integrations", "twitch-integrations") {
    buildGroup {
      build("Build") {
        requirements(requireWindows)
        vcsTrigger("+:pull/*")

        windowsNpmPathLengthLimitWorkaround()
        githubStatusPublisher()

        steps(runScript("Build", "yarn tc:build"))
      }

      build("Test") {
        requirements(requireWindows)
        vcsTrigger("+:pull/*")

        windowsNpmPathLengthLimitWorkaround()
        githubStatusPublisher()

        steps(runScript("Build", "yarn tc:test"))
      }

      build("Publish") {
        requirements(requireWindows)
        vcsTrigger("+:tags/v*")

        snapshotDependency("twitch-integrations","Build")
        snapshotDependency("twitch-integrations","Test")

        steps(npmPublishStep("yarn"))
        steps(npmPublishStep("npm publish"))
      }
    }
  }
}
