#!/usr/bin/env bash

if [[ "%teamcity.build.branch%" =~ ^pull/[0-9]+$ ]]; then
  git fetch -q
  IFS=' ' read -ra PR_MERGE_COMMIT_MSG <<< "$(git log --format='%s' -n 1)"
  if [[ ${PR_MERGE_COMMIT_MSG[1]} =~ ^[0-9a-z]{40}$ ]]; then
    PR_COMMIT_ID=${PR_MERGE_COMMIT_MSG[1]}
  else
    PR_COMMIT_ID=$(git rev-parse HEAD)
  fi
  echo $PR_COMMIT_ID
  IFS='/' read -r ORIGIN MY_BRANCH <<< "$(git branch -r --contains $PR_COMMIT_ID | sed -n 1p | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
  SANITIZED_BRANCH=$MY_BRANCH
else
  SANITIZED_BRANCH="%teamcity.build.branch%"
fi

echo "##teamcity[setParameter name='branch.sanitized' value='$SANITIZED_BRANCH']"
