"use strict";
exports.__esModule = true;
function parse(str) {
    // The other end is not guaranteed to send json.
    // Return the original string if we failed to parse it.
    try {
        return JSON.parse(str);
    }
    catch (_) {
        return str;
    }
}
exports.parse = parse;
//# sourceMappingURL=json-helpers.js.map