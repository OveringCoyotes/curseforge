"use strict";
exports.__esModule = true;
var ipc_connection_1 = require("./ipc-connection");
// tslint:disable:no-any
var IPCConnectionProxy = /** @class */ (function () {
    function IPCConnectionProxy(server, connectionHandle) {
        var _this = this;
        this.send = function (value) {
            _this.connection.send(_this.connectionHandle, value);
        };
        this.invoke = function (value) {
            return _this.connection.invoke(_this.connectionHandle, value);
        };
        this.connection = server;
        this.connectionHandle = connectionHandle;
    }
    IPCConnectionProxy.prototype.on = function (event, callback) {
        switch (event) {
            case ipc_connection_1.IPCConnectionEvent.Disconnected:
                this.onDisconnectedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Received:
                this.onReceivedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Invoked:
                this.onInvokedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Log:
                this.onLogCallback = callback;
                break;
            default:
                break;
        }
    };
    return IPCConnectionProxy;
}());
exports.IPCConnectionProxy = IPCConnectionProxy;
//# sourceMappingURL=ipc-connection-proxy.js.map