"use strict";
exports.__esModule = true;
var tslib_1 = require("tslib");
var ipc_connection_1 = require("./ipc-connection");
var json_helpers_1 = require("./json-helpers");
// tslint:disable:no-any
var IPCConnectionType;
(function (IPCConnectionType) {
    IPCConnectionType["Server"] = "server";
    IPCConnectionType["Client"] = "client";
})(IPCConnectionType = exports.IPCConnectionType || (exports.IPCConnectionType = {}));
var IPCClientConnection = /** @class */ (function () {
    function IPCClientConnection(endpoint) {
        var _this = this;
        this.setLogLevel = function (logLevel) {
            _this.connection.setLogLevel(logLevel);
        };
        this.connect = function () {
            _this.connection.on(_this.onHandler);
            _this.connection.connect();
        };
        this.disconnect = function () {
            _this.connection.disconnect();
            _this.connection.on();
        };
        this.send = function (value) {
            _this.connection.send(JSON.stringify(value));
        };
        this.invoke = function (value) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var result, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = json_helpers_1.parse;
                        return [4 /*yield*/, this.connection.invoke(JSON.stringify(value))];
                    case 1:
                        result = _a.apply(void 0, [_b.sent()]);
                        if (result === ipc_connection_1.InvokeFailedError) {
                            throw new Error('No invoke handler found');
                        }
                        return [2 /*return*/, result];
                }
            });
        }); };
        this.onHandler = function (event) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            switch (event) {
                case ipc_connection_1.IPCConnectionEvent.Connected:
                    if (_this.onConnectedCallback) {
                        _this.onConnectedCallback();
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Disconnected:
                    if (_this.onDisconnectedCallback) {
                        _this.onDisconnectedCallback();
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Error:
                    if (_this.onErrorCallback) {
                        _this.onErrorCallback();
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Log:
                    if (_this.onLogCallback) {
                        _this.onLogCallback(args[0], args[1], args[2]);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Received:
                    if (_this.onReceivedCallback) {
                        var value = json_helpers_1.parse(args[0]);
                        _this.onReceivedCallback(value);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Invoked:
                    var resultCallback_1 = args[1];
                    if (_this.onInvokedCallback) {
                        var value_1 = json_helpers_1.parse(args[0]);
                        (function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var result, _err_1;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, , 3]);
                                        return [4 /*yield*/, this.onInvokedCallback(value_1)];
                                    case 1:
                                        result = _a.sent();
                                        resultCallback_1(JSON.stringify(result));
                                        return [3 /*break*/, 3];
                                    case 2:
                                        _err_1 = _a.sent();
                                        resultCallback_1(ipc_connection_1.generateInvokeFailError());
                                        return [3 /*break*/, 3];
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); })();
                    }
                    else {
                        resultCallback_1(ipc_connection_1.generateInvokeFailError());
                    }
                    break;
                default:
                    break;
            }
        };
        var IPCConnectionInternal = require('../build/Release/twitch-desktop-ipc.node').IPCConnection;
        this.connection = new IPCConnectionInternal(IPCConnectionType.Client, endpoint);
    }
    IPCClientConnection.prototype.on = function (event, callback) {
        switch (event) {
            case ipc_connection_1.IPCConnectionEvent.Connected:
                this.onConnectedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Disconnected:
                this.onDisconnectedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Error:
                this.onErrorCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Log:
                this.onLogCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Received:
                this.onReceivedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Invoked:
                this.onInvokedCallback = callback;
                break;
            default:
                break;
        }
    };
    return IPCClientConnection;
}());
exports.IPCClientConnection = IPCClientConnection;
//# sourceMappingURL=ipc-client-connection.js.map