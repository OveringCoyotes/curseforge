"use strict";
exports.__esModule = true;
var ipc_connection_1 = require("./ipc-connection");
var ipc_connection_proxy_1 = require("./ipc-connection-proxy");
var ipc_server_connection_1 = require("./ipc-server-connection");
// tslint:disable:no-any
var IPCConnectionFactory = /** @class */ (function () {
    function IPCConnectionFactory(endpoint, allowMultiuserAccess) {
        var _this = this;
        if (allowMultiuserAccess === void 0) { allowMultiuserAccess = false; }
        this.connections = [];
        this.setLogLevel = function (logLevel) {
            _this.connection.setLogLevel(logLevel);
        };
        this.connect = function () {
            _this.connection.connect();
        };
        this.disconnect = function () {
            _this.connection.disconnect();
        };
        this.broadcast = function (value) {
            _this.connection.broadcast(value);
        };
        this.connection = new ipc_server_connection_1.IPCServerConnection(endpoint, false, allowMultiuserAccess);
        this.connection.on(ipc_connection_1.IPCConnectionEvent.Connected, function (connectionHandle) {
            if (_this.onConnectCallback) {
                var connection = new ipc_connection_proxy_1.IPCConnectionProxy(_this.connection, connectionHandle);
                _this.connections[connectionHandle] = connection;
                _this.onConnectCallback(connection);
            }
        });
        this.connection.on(ipc_connection_1.IPCConnectionEvent.Disconnected, function (connectionHandle) {
            var connection = _this.connections[connectionHandle];
            if (connection && connection.onDisconnectedCallback) {
                connection.onDisconnectedCallback();
            }
        });
        this.connection.on(ipc_connection_1.IPCConnectionEvent.Invoked, function (connectionHandle, value) {
            var connection = _this.connections[connectionHandle];
            if (connection && connection.onInvokedCallback) {
                return connection.onInvokedCallback(value);
            }
            return Promise.reject();
        });
        this.connection.on(ipc_connection_1.IPCConnectionEvent.Received, function (connectionHandle, value) {
            var connection = _this.connections[connectionHandle];
            if (connection && connection.onReceivedCallback) {
                connection.onReceivedCallback(value);
            }
        });
        this.connection.on(ipc_connection_1.IPCConnectionEvent.Log, function (connectionHandle, logLevel, message, category) {
            if (connectionHandle) {
                var connection = _this.connections[connectionHandle];
                if (connection && connection.onLogCallback) {
                    connection.onLogCallback(logLevel, message, category);
                    return;
                }
            }
            if (_this.onLogCallback) {
                _this.onLogCallback(connectionHandle, logLevel, message, category);
            }
        });
    }
    IPCConnectionFactory.prototype.on = function (event, callback) {
        switch (event) {
            case ipc_connection_1.IPCConnectionEvent.Connected:
                this.onConnectCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Log:
                this.onLogCallback = callback;
                break;
            default:
                break;
        }
    };
    return IPCConnectionFactory;
}());
exports.IPCConnectionFactory = IPCConnectionFactory;
//# sourceMappingURL=ipc-connection-factory.js.map