"use strict";
exports.__esModule = true;
var tslib_1 = require("tslib");
var ipc_client_connection_1 = require("./ipc-client-connection");
var ipc_connection_1 = require("./ipc-connection");
var json_helpers_1 = require("./json-helpers");
var IPCServerConnection = /** @class */ (function () {
    function IPCServerConnection(endpoint, onlyAllowOne, allowMultiuserAccess) {
        var _this = this;
        if (onlyAllowOne === void 0) { onlyAllowOne = false; }
        if (allowMultiuserAccess === void 0) { allowMultiuserAccess = false; }
        this.setLogLevel = function (logLevel) {
            if (_this.singleConnection) {
                _this.singleConnection.setLogLevel(logLevel);
            }
            else {
                _this.multiConnection.setLogLevel(logLevel);
            }
        };
        this.connect = function () {
            if (_this.singleConnection) {
                _this.singleConnection.on(_this.onSingleHandler);
                _this.singleConnection.connect();
            }
            else {
                _this.multiConnection.on(_this.onMultiHandler);
                _this.multiConnection.connect();
            }
        };
        this.disconnect = function () {
            if (_this.singleConnection) {
                _this.singleConnection.disconnect();
                _this.singleConnection.on();
            }
            else {
                _this.multiConnection.disconnect();
                _this.multiConnection.on();
            }
        };
        this.broadcast = function (value) {
            if (_this.singleConnection) {
                _this.singleConnection.send(JSON.stringify(value));
            }
            else {
                _this.multiConnection.broadcast(JSON.stringify(value));
            }
        };
        this.send = function (connectionHandle, value) {
            if (_this.singleConnection) {
                _this.singleConnection.send(JSON.stringify(value));
            }
            else {
                _this.multiConnection.send(connectionHandle, JSON.stringify(value));
            }
        };
        this.invoke = function (connectionHandle, value) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var result, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = json_helpers_1.parse;
                        if (!this.singleConnection) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.singleConnection.invoke(JSON.stringify(value))];
                    case 1:
                        _b = _c.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.multiConnection.invoke(connectionHandle, JSON.stringify(value))];
                    case 3:
                        _b = _c.sent();
                        _c.label = 4;
                    case 4:
                        result = _a.apply(void 0, [_b]);
                        if (result === ipc_connection_1.InvokeFailedError) {
                            throw new Error('No invoke handler found');
                        }
                        return [2 /*return*/, result];
                }
            });
        }); };
        this.onMultiHandler = function (connectionHandle, event) {
            var args = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                args[_i - 2] = arguments[_i];
            }
            switch (event) {
                case ipc_connection_1.IPCConnectionEvent.Connected:
                    if (_this.onConnectedCallback) {
                        _this.onConnectedCallback(connectionHandle);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Disconnected:
                    if (_this.onDisconnectedCallback) {
                        _this.onDisconnectedCallback(connectionHandle);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Error:
                    if (_this.onErrorCallback) {
                        _this.onErrorCallback(connectionHandle);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Log:
                    if (_this.onLogCallback) {
                        _this.onLogCallback(connectionHandle, args[0], args[1], args[2]);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Received:
                    if (_this.onReceivedCallback) {
                        var value = json_helpers_1.parse(args[0]);
                        _this.onReceivedCallback(connectionHandle, value);
                    }
                    break;
                case ipc_connection_1.IPCConnectionEvent.Invoked:
                    var resultCallback_1 = args[1];
                    if (_this.onInvokedCallback) {
                        var value_1 = json_helpers_1.parse(args[0]);
                        (function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var result, _err_1;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, , 3]);
                                        return [4 /*yield*/, this.onInvokedCallback(connectionHandle, value_1)];
                                    case 1:
                                        result = _a.sent();
                                        resultCallback_1(JSON.stringify(result));
                                        return [3 /*break*/, 3];
                                    case 2:
                                        _err_1 = _a.sent();
                                        resultCallback_1(ipc_connection_1.generateInvokeFailError());
                                        return [3 /*break*/, 3];
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); })();
                    }
                    else {
                        resultCallback_1(ipc_connection_1.generateInvokeFailError());
                    }
                    break;
                default:
                    break;
            }
        };
        this.onSingleHandler = function (event) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            _this.onMultiHandler.apply(_this, tslib_1.__spreadArrays([1, event], args));
        };
        if (onlyAllowOne) {
            var IPCConnectionInternal = require('../build/Release/twitch-desktop-ipc.node').IPCConnection;
            this.singleConnection = new IPCConnectionInternal(ipc_client_connection_1.IPCConnectionType.Server, endpoint, allowMultiuserAccess);
        }
        else {
            var IPCServerConnectionInternal = require('../build/Release/twitch-desktop-ipc.node').IPCServerConnection;
            this.multiConnection = new IPCServerConnectionInternal(endpoint, allowMultiuserAccess);
        }
    }
    IPCServerConnection.prototype.on = function (event, callback) {
        switch (event) {
            case ipc_connection_1.IPCConnectionEvent.Connected:
                this.onConnectedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Disconnected:
                this.onDisconnectedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Error:
                this.onErrorCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Log:
                this.onLogCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Received:
                this.onReceivedCallback = callback;
                break;
            case ipc_connection_1.IPCConnectionEvent.Invoked:
                this.onInvokedCallback = callback;
                break;
            default:
                break;
        }
    };
    return IPCServerConnection;
}());
exports.IPCServerConnection = IPCServerConnection;
//# sourceMappingURL=ipc-server-connection.js.map