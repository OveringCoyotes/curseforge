"use strict";
// tslint:disable:no-any
exports.__esModule = true;
var IPCConnectionEvent;
(function (IPCConnectionEvent) {
    IPCConnectionEvent["Connected"] = "connected";
    IPCConnectionEvent["Disconnected"] = "disconnected";
    IPCConnectionEvent["Error"] = "error";
    IPCConnectionEvent["Received"] = "received";
    IPCConnectionEvent["Invoked"] = "invoked";
    IPCConnectionEvent["Log"] = "log";
})(IPCConnectionEvent = exports.IPCConnectionEvent || (exports.IPCConnectionEvent = {}));
var IPCConnectionLogLevel;
(function (IPCConnectionLogLevel) {
    IPCConnectionLogLevel["Debug"] = "DEBUG";
    IPCConnectionLogLevel["Info"] = "INFO";
    IPCConnectionLogLevel["Warning"] = "WARNING";
    IPCConnectionLogLevel["Error"] = "ERROR";
    IPCConnectionLogLevel["None"] = "NONE";
})(IPCConnectionLogLevel = exports.IPCConnectionLogLevel || (exports.IPCConnectionLogLevel = {}));
exports.InvokeFailedError = '#*# No Invoke Handler Found *#*';
function generateInvokeFailError() {
    return JSON.stringify(exports.InvokeFailedError);
}
exports.generateInvokeFailError = generateInvokeFailError;
//# sourceMappingURL=ipc-connection.js.map