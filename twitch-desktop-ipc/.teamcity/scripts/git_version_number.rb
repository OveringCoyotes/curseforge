#!/usr/bin/env ruby
# frozen_string_literal: true

tag = `git describe --tags --exact-match --match %teamcity.build.branch% 2> /dev/null`.strip
hash = `git rev-parse HEAD 2> /dev/null`.strip[0..6]

version = tag.empty? ? hash : tag

puts "##teamcity[setParameter name='git.version' value='#{version}']"
puts "##teamcity[setParameter name='git.commit.short' value='#{hash}']"
puts "##teamcity[buildNumber '#{version}']"
