package helpers

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.BuildSteps
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.VcsTrigger
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.GitVcsRoot
import twitch.instrumentorum.instrum
import java.io.File

val projectBranchSpecification: ParametrizedWithType.() -> Unit = {
  text("git.branch.spec", """
    +:refs/heads/*
    +:refs/heads/(master)
    +:refs/(pull/*)/head
    +:refs/(tags/*)
  """.trimIndent(), "Branch Spec", "What branches should we additionally watch or ignore?", ParameterDisplay.HIDDEN)
}

fun BuildType.windowsNpmPathLengthLimitWorkaround() {
    // workaround due to lack of long path support in npm and node-gyp
    vcs {
        checkoutDir = "c:\\tc\\%teamcity.build.default.checkoutDir%"
    }
}

val requireWindows: Requirements.() -> Unit = {
  contains("teamcity.agent.jvm.os.name", "Windows")
}

val requireMacOS: Requirements.() -> Unit = {
  equals("teamcity.agent.jvm.os.name", "Mac OS X")
}

val requireLinux: Requirements.() -> Unit = {
  equals("teamcity.agent.jvm.os.name", "Linux")
}

fun BuildType.useVcs(newVcs: GitVcsRoot) {
  vcs {
    entries.clear()
    root(newVcs)
  }
}

fun runScript(scriptName: String, scriptCode: String): BuildSteps.() -> Unit {
  return {
    script {
      name = scriptName
      scriptContent = scriptCode
    }
  }
}

