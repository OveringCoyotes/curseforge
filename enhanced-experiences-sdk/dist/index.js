'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const deep_diff_1 = require("deep-diff");
const isobject_1 = __importDefault(require("isobject"));
const ws_1 = __importDefault(require("ws"));
const utilities_1 = require("./utilities");
const queue_1 = require("./queue");
const maximumConnectMessageSize = 100000;
const maximumDeltaSize = 20000;
const sendDelay = ((s) => {
    const n = parseInt(s, 10);
    return Number.isFinite(n) && n > 0 ? n : 1000;
})(process.env['TWITCH_TEST_ENHANCED_EXPERIENCES_SEND_DELAY'] || '');
const url = process.env['TWITCH_TEST_ENHANCED_EXPERIENCES_URL'] || 'wss://metadata.twitch.tv/api/ingest';
function createDataSource() {
    let currentData;
    let webSocket;
    let sessionId;
    let token;
    let connectFn;
    let openFn;
    let onTokenExpired;
    let senderTask;
    let lastSendTime;
    let debugFn = console.error;
    const queue = queue_1.createMessageQueue();
    return {
        connect,
        disconnect,
        replaceData,
        removeField,
        updateField,
        updateData,
        appendToArrayField,
    };
    function connect(configuration) {
        return __awaiter(this, void 0, void 0, function* () {
            if (currentData) {
                throw new Error('already connected');
            }
            // Validate the configuration.
            const data = utilities_1.validateData(configuration.initialData);
            onTokenExpired = configuration.onTokenExpired;
            sessionId = configuration.sessionId || utilities_1.generateSessionId();
            token = configuration.token.valueOf().toString();
            // Compose the connect function.
            const broadcasterIds = configuration.broadcasterIds && configuration.broadcasterIds.length ?
                configuration.broadcasterIds.map((value) => value.valueOf().toString()) :
                undefined;
            const gameId = configuration.gameId.valueOf().toString();
            const environment = configuration.environment.valueOf().toString();
            const isDebug = Boolean(configuration.debugFn);
            if (typeof configuration.debugFn === 'function') {
                debugFn = configuration.debugFn;
            }
            const connectMessageSize = JSON.stringify(utilities_1.createConnectMessage(sessionId, token, broadcasterIds, gameId, environment, isDebug, data)).length;
            if (connectMessageSize > maximumConnectMessageSize) {
                throw new Error('initial data object is too large');
            }
            connectFn = (data) => {
                try {
                    // Send the "Connect" message.
                    send(utilities_1.createConnectMessage(sessionId, token, broadcasterIds, gameId, environment, isDebug, data));
                }
                catch (ex) {
                    return ex;
                }
            };
            // Compose the open function.
            const timeoutMs = (configuration.timeoutMs || 9999).valueOf();
            openFn = () => __awaiter(this, void 0, void 0, function* () {
                // Create the socket and await a connection.
                webSocket = yield new Promise((resolve, reject) => {
                    const webSocket = new ws_1.default(url);
                    webSocket.addEventListener('error', onError);
                    webSocket.addEventListener('open', onOpen);
                    const timerId = setTimeout(onTimeout, timeoutMs);
                    function onError(event) {
                        clearHandlers();
                        reject(event.error);
                    }
                    function onOpen(_event) {
                        clearHandlers();
                        resolve(webSocket);
                    }
                    function onTimeout() {
                        clearHandlers();
                        reject(new Error('timeout expired'));
                    }
                    function clearHandlers() {
                        webSocket.removeEventListener('error', onError);
                        webSocket.removeEventListener('open', onOpen);
                        clearTimeout(timerId);
                    }
                });
                webSocket.addEventListener('close', onClose);
                webSocket.addEventListener('message', onMessage);
            });
            // Open the WebSocket.
            yield openFn();
            // Enqueue a "Reauthorize" message without clearing the token.
            queue.enqueue(queue_1.QueueMessage.MakeReauthorize(true));
            // Set the current data.
            currentData = data;
            // Invoke sendMessage to send the intial connect message.  The "Reauthorize"
            // message handler will start the sender task.
            yield sendMessage();
            return sessionId;
        });
    }
    function disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            clearTimeout(senderTask);
            senderTask = undefined;
            if (queue.some()) {
                queue.replaceWith(queue_1.QueueMessage.Refresh);
                const nextSendTime = lastSendTime + sendDelay;
                const finalDelay = nextSendTime - Date.now();
                if (finalDelay > 0) {
                    yield utilities_1.sleep(finalDelay);
                }
                yield sendMessage();
            }
            closeWebSocket();
            currentData = undefined;
        });
    }
    function replaceData(data) {
        validateConnection();
        // Update the current data, first ensuring it is valid.
        currentData = utilities_1.validateData(data);
        // Enqueue the "Refresh" message.
        queue.enqueue(queue_1.QueueMessage.Refresh);
    }
    function removeField(path) {
        // Validate the connection and path.
        validateConnection();
        path = utilities_1.validatePath(path);
        if (path.endsWith(']')) {
            throw new Error(`"${path}" does not specify a field`);
        }
        const segment = utilities_1.getSegment(currentData, path);
        if (!segment || typeof segment.parent[segment.field] === 'undefined') {
            throw new Error(`"${path}" does not specify a known field`);
        }
        // Update the current data.
        const parent = segment.parent;
        const field = segment.field;
        delete parent[field];
        // Enqueue the "Remove Field" message.
        queue.enqueue(new queue_1.QueueMessage(queue_1.MessageType.Remove, path));
    }
    function updateField(path, value) {
        // Validate the connection, path, and value.
        validateConnection();
        path = utilities_1.validatePath(path);
        value = JSON.parse(JSON.stringify(value));
        // If updating the _metadata field, ensure it is valid.
        if (path === '_metadata') {
            utilities_1.validateData({ [path]: value });
        }
        // Update the current data.
        const segment = utilities_1.getSegment(currentData, path);
        if (!segment) {
            throw new Error(`"${path}" does not specify a known field`);
        }
        else if (Array.isArray(segment.parent) && segment.field >= segment.parent.length) {
            throw new Error(`"${path}" is out of bounds`);
        }
        segment.parent[segment.field] = value;
        // Enqueue the "Update" message.
        queue.enqueue(new queue_1.QueueMessage(queue_1.MessageType.Update, path, value));
    }
    function updateData(data) {
        // Validate the connection and data.
        validateConnection();
        data = utilities_1.validateData(data);
        // Produce deltas for this update only if all current messages in the queue
        // are delta messages.
        if (queue.every((m) => m.isDelta())) {
            // Get differences.
            const differences = deep_diff_1.diff(currentData, data);
            if (differences) {
                // Enqueue appropriate modification messages.
                const localQueue = [];
                const result = differences.every((difference) => {
                    const path = utilities_1.constructPath(difference.path || []);
                    switch (difference.kind) {
                        case 'A':
                            if (difference.item.kind === 'N') {
                                localQueue.push(new queue_1.QueueMessage(queue_1.MessageType.Append, path, [difference.item.rhs]));
                            }
                            else {
                                return false;
                            }
                            break;
                        case 'D':
                            localQueue.push(new queue_1.QueueMessage(queue_1.MessageType.Remove, path));
                            break;
                        case 'E':
                        case 'N':
                            localQueue.push(new queue_1.QueueMessage(queue_1.MessageType.Update, path, difference.rhs));
                            break;
                        default:
                            debugFn('DataSource.updateData', 'unexpected difference', difference);
                            return false;
                    }
                    return true;
                });
                if (result) {
                    // Reverse the local queue so array additions are in the correct order.
                    queue.enqueue(...localQueue.reverse());
                    // Enqueue a "Refresh" message if this update and whatever is in the queue
                    // cannot be sent as a single delta message.
                    const deltas = queue.map((m) => m.asDelta());
                    if (JSON.stringify({ delta: deltas }).length > maximumDeltaSize) {
                        debugFn('DataSource.sendMessage', 'delta too large', queue.peek());
                        queue.replaceWith(new queue_1.QueueMessage(queue_1.MessageType.Refresh));
                    }
                    // Update the current data.
                    currentData = data;
                }
                else {
                    replaceData(data);
                }
            }
        }
        else {
            // Update the current data.  There is a message in the queue that will
            // invoke the equivalent of a "Refresh" message.
            currentData = data;
        }
    }
    function appendToArrayField(path, values) {
        // Validate the connection, path, and values.
        validateConnection();
        path = utilities_1.validatePath(path);
        values = JSON.parse(JSON.stringify(values));
        if (!Array.isArray(values)) {
            throw new Error('values is not an array');
        }
        const segment = utilities_1.getSegment(currentData, path);
        if (!segment || typeof segment.parent[segment.field] === 'undefined') {
            throw new Error(`"${path}" does not specify a known field`);
        }
        const array = segment.parent[segment.field];
        if (!Array.isArray(array)) {
            throw new Error(`"${path}" does not specify an array field`);
        }
        else if (values.length === 0) {
            debugFn('DataSource.appendToArrayField', 'array is empty; ignoring', { path, values });
            return;
        }
        // Update the current data.
        array.push(...values);
        // Enqueue the "Append" message.
        queue.enqueue(new queue_1.QueueMessage(queue_1.MessageType.Append, path, values));
    }
    function acquireToken() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new Promise((resolve, _reject) => {
                try {
                    if (!onTokenExpired(resolve)) {
                        // The client wants to shut down.
                        resolve('');
                    }
                }
                catch (ex) {
                    // There is likely a logic error in the client.  Shut down the connection.
                    debugFn('DataSource.acquireToken', 'onTokenExpired threw an exception', ex);
                    resolve('');
                }
            });
        });
    }
    function sendMessage() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                senderTask = undefined;
                const uniqueMessage = queue.findUniqueMessage();
                if (uniqueMessage) {
                    // There is a unique message.  Clear the queue and process the message.
                    queue.clear();
                    switch (uniqueMessage.type) {
                        case queue_1.MessageType.Reauthorize:
                            if (!uniqueMessage.value) {
                                token = '';
                            }
                            yield reauthorize();
                            break;
                        case queue_1.MessageType.Reconnect:
                            // Await the requested delay.
                            const reconnectDelay = uniqueMessage.value - Date.now();
                            if (reconnectDelay > 0) {
                                yield utilities_1.sleep(reconnectDelay);
                            }
                            if (currentData) {
                                // Open a new connection.
                                yield openFn();
                                // Perform the "Reauthorize" action above.
                                if (currentData) {
                                    yield reauthorize();
                                }
                            }
                            break;
                        case queue_1.MessageType.Refresh:
                            send({ refresh: { data: currentData } });
                            break;
                        default:
                            throw new Error(`unexpected message type ${uniqueMessage.type}`);
                    }
                }
                else {
                    // Trim the queue based on repeated modifications.
                    const trimmedQueue = [];
                    while (queue.some()) {
                        const message = queue.dequeue();
                        switch (message.type) {
                            case queue_1.MessageType.Append:
                                const currentMessage = trimmedQueue.find((m) => m.type === message.type && m.path === message.path);
                                if (currentMessage) {
                                    currentMessage.value.push(...message.value);
                                }
                                else {
                                    trimmedQueue.push(message);
                                }
                                break;
                            case queue_1.MessageType.Remove:
                            case queue_1.MessageType.Update:
                                trimmedQueue.splice(0, trimmedQueue.length, ...trimmedQueue.filter((m) => m.path !== message.path));
                                trimmedQueue.push(message);
                                break;
                            default:
                                throw new Error(`unexpected message type ${message.type}`);
                        }
                    }
                    queue.replaceWith(...trimmedQueue);
                    // Take messages off of the queue until reaching the maximum payload size.
                    if (queue.some()) {
                        const deltas = [];
                        while (queue.some()) {
                            let delta;
                            const message = queue.peek();
                            delta = message.asDelta();
                            if (JSON.stringify({ delta: [...deltas, delta] }).length > maximumDeltaSize) {
                                break;
                            }
                            deltas.push(delta);
                            queue.dequeue();
                        }
                        if (deltas.length) {
                            send({ delta: deltas });
                        }
                        else {
                            // The first message is larger than the maximum payload size.  Enqueue a
                            // "Refresh" message and recurse.
                            debugFn('DataSource.sendMessage', 'delta too large', queue.peek());
                            queue.replaceWith(queue_1.QueueMessage.Refresh);
                            yield sendMessage();
                            // Prevent two time-outs due to recursion.
                            clearTimeout(senderTask);
                        }
                    }
                }
            }
            catch (ex) {
                debugFn('DataSource.sendMessage', 'unexpected exception', ex);
            }
            finally {
                // Set another time-out to send the next message.
                if (currentData) {
                    senderTask = setTimeout(sendMessage, sendDelay);
                }
            }
        });
    }
    function reauthorize() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                token = yield acquireToken();
            }
            if (token) {
                const error = connectFn(currentData);
                if (!error) {
                    return true;
                }
                debugFn('DataSource.reauthorize', 'connection error', error);
            }
            yield disconnect();
            return false;
        });
    }
    function validateConnection() {
        // Ensure connect has already been invoked.
        if (!currentData) {
            throw new Error('connection not established');
        }
    }
    function send(message) {
        webSocket.send(JSON.stringify(message));
        lastSendTime = Date.now();
    }
    function closeWebSocket() {
        webSocket.removeEventListener('close', onClose);
        webSocket.removeEventListener('message', onMessage);
        webSocket.close();
    }
    function onClose(_event) {
        // Clear the handlers and enqueue a "Reconnect" message.
        webSocket.removeEventListener('close', onClose);
        webSocket.removeEventListener('message', onMessage);
        queue.enqueue(queue_1.QueueMessage.MakeReconnect());
    }
    function onMessage(event) {
        try {
            const data = event.data.toString();
            const response = JSON.parse(data);
            const keys = Object.keys(response);
            if (keys.length === 1) {
                switch (keys[0]) {
                    case 'connected':
                        if (response['connected']) {
                            return;
                        }
                        break;
                    case 'error':
                        const error = response['error'];
                        if (isobject_1.default(error)) {
                            const code = error['code'];
                            if (code === 'invalid_connect_token') {
                                // The server has rejected the authorization token.
                                queue.enqueue(queue_1.QueueMessage.MakeReauthorize());
                                return;
                            }
                            else if (code === 'connection_not_authed') {
                                // Ignore this since it means we're in the middle of getting connected.
                                return;
                            }
                            else if (code === 'waiting_on_refresh_message') {
                                // Ignore this since it means we're in the middle of refreshing.
                                return;
                            }
                        }
                        break;
                    case 'reconnect':
                        const reconnectDelay = response['reconnect'];
                        if (typeof reconnectDelay === 'number') {
                            // The server requested a reconnection.  Close the socket.  The reconnection
                            // process will open a new one.
                            closeWebSocket();
                            queue.enqueue(queue_1.QueueMessage.MakeReconnect(reconnectDelay));
                            return;
                        }
                        break;
                }
            }
            throw new Error(`unexpected response from server:  ${data}`);
        }
        catch (ex) {
            debugFn('DataSource.onMessage', 'unexpected exception', ex);
        }
    }
}
exports.default = createDataSource;
//# sourceMappingURL=index.js.map