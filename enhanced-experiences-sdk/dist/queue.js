'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType;
(function (MessageType) {
    MessageType[MessageType["Append"] = 0] = "Append";
    MessageType[MessageType["Reauthorize"] = 1] = "Reauthorize";
    MessageType[MessageType["Reconnect"] = 2] = "Reconnect";
    MessageType[MessageType["Refresh"] = 3] = "Refresh";
    MessageType[MessageType["Remove"] = 4] = "Remove";
    MessageType[MessageType["Update"] = 5] = "Update";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
class QueueMessage {
    constructor(type, path, value) {
        this.type = type;
        this.path = path;
        this.value = value;
    }
    static MakeReauthorize(usingToken) {
        return new QueueMessage(MessageType.Reauthorize, undefined, usingToken);
    }
    static MakeReconnect(reconnectDelay) {
        const reconnectTime = (reconnectDelay || 0) + Date.now();
        return new QueueMessage(MessageType.Reconnect, undefined, reconnectTime);
    }
    asDelta() {
        switch (this.type) {
            case MessageType.Append:
                return [this.path, 'a', this.value];
            case MessageType.Remove:
                return [this.path];
            case MessageType.Update:
                return [this.path, this.value];
            default:
                throw new Error(`unexpected message type ${this.type}`);
        }
    }
    isDelta() {
        return [MessageType.Append, MessageType.Remove, MessageType.Update].some((m) => m === this.type);
    }
}
exports.QueueMessage = QueueMessage;
QueueMessage.Refresh = new QueueMessage(MessageType.Refresh);
// These are in priority order.
const uniqueMessageTypes = [MessageType.Reconnect, MessageType.Reauthorize, MessageType.Refresh];
function createMessageQueue() {
    const queue = [];
    return {
        clear,
        dequeue,
        findUniqueMessage,
        peek,
        enqueue,
        replaceWith,
        some,
        every,
        map,
    };
    function clear() {
        queue.splice(0, queue.length);
    }
    function dequeue() {
        return queue.shift();
    }
    function findUniqueMessage() {
        for (const uniqueMessageType of uniqueMessageTypes) {
            const uniqueMessage = queue.find((message) => message.type === uniqueMessageType);
            if (uniqueMessage) {
                return uniqueMessage;
            }
        }
    }
    function peek() {
        return queue[0];
    }
    function enqueue(...message) {
        queue.push(...message);
    }
    function replaceWith(...message) {
        queue.splice(0, queue.length, ...message);
    }
    function some() {
        return queue.length > 0;
    }
    function every(fn) {
        return queue.every(fn);
    }
    function map(fn) {
        return queue.map(fn);
    }
}
exports.createMessageQueue = createMessageQueue;
//# sourceMappingURL=queue.js.map