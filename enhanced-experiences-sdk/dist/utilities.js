'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const isobject_1 = __importDefault(require("isobject"));
function constructPath(path) {
    return path.reduce((path, segment) => {
        return path + (typeof segment === 'number' ? `[${segment}]` : path ? '.' + segment : segment);
    }, '');
}
exports.constructPath = constructPath;
function createConnectMessage(sessionId, token, broadcasterIds, gameId, environment, isDebug, data) {
    const message = {
        connect: {
            session_id: sessionId,
            token,
            game_id: gameId,
            env: environment,
            data,
        },
    };
    if (broadcasterIds) {
        message.connect.broadcaster_ids = broadcasterIds;
    }
    if (isDebug) {
        message.connect.debug = true;
    }
    return message;
}
exports.createConnectMessage = createConnectMessage;
function getSegment(data, path) {
    const parts = path.split(/[.[]/);
    const field = parts.pop();
    if (field) {
        const parent = parts.reduce(checkSegment, data);
        if (parent) {
            if (field.endsWith(']')) {
                if (Array.isArray(parent)) {
                    const index = parseInt(field, 10);
                    return { parent, field: index };
                }
            }
            else if (isobject_1.default(parent)) {
                return { parent, field };
            }
        }
    }
    function checkSegment(data, segment) {
        if (data) {
            if (segment.endsWith(']')) {
                if (Array.isArray(data)) {
                    const index = parseInt(segment, 10);
                    return data[index];
                }
            }
            else if (isobject_1.default(data)) {
                return data[segment];
            }
        }
    }
}
exports.getSegment = getSegment;
function sleep(milliseconds) {
    return __awaiter(this, void 0, void 0, function* () {
        yield new Promise((resolve, _reject) => setTimeout(resolve, milliseconds));
    });
}
exports.sleep = sleep;
function validateData(data) {
    // Validate the data.
    if (!isobject_1.default(data)) {
        throw new Error('data is not an object');
    }
    // If there is a "_metadata" field, ensure it is an object.
    const metadata = data['_metadata'];
    if (metadata !== undefined && !isobject_1.default(metadata)) {
        throw new Error('_metadata field is not an object');
    }
    return JSON.parse(JSON.stringify(data));
}
exports.validateData = validateData;
const pathRx = /^\w+(\.\w+|\[[0-9]\])*$/;
function validatePath(path) {
    path = path.valueOf();
    if (!path) {
        throw new Error('path is empty');
    }
    else if (typeof path !== 'string') {
        throw new Error('path is not a string');
    }
    else if (!pathRx.test(path)) {
        throw new Error(`"${path}" is not a valid field specifier`);
    }
    return path;
}
exports.validatePath = validatePath;
const idCharacters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
function generateSessionId() {
    return Array(32).fill(0).map(_ => idCharacters.charAt(Math.floor(idCharacters.length * Math.random()))).join('');
}
exports.generateSessionId = generateSessionId;
//# sourceMappingURL=utilities.js.map